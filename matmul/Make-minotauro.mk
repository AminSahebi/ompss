
CFLAGS=-I /opt/compilers/intel/mkl/include
LDFLAGS=-L /opt/compilers/intel/mkl/lib/intel64/ -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lpthread -lm -Wl,-rpath,/opt/compilers/intel/mkl/lib/intel64/,-rpath,/opt/compilers/intel/lib/intel64/ -lm -L /opt/compilers/intel/lib/intel64/ -lirc -L /opt/mpi/bullxmpi/1.1.11.1/lib -lmpi
CUDA_LDFLAGS=-L /opt/compilers/intel/mkl/lib/intel64/ -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lpthread -lm -Xlinker "-rpath /opt/compilers/intel/mkl/lib/intel64/" -Xlinker "-rpath /opt/compilers/intel/lib/intel64/" -lm -L /opt/compilers/intel/lib/intel64/ -lirc -L /opt/mpi/bullxmpi/1.1.11.1/lib -lmpi

#Extra targets for cuda versions

PERF_CUDA_BINARIES=dgemm_cuda_kij_multilevel_nomanualblocking.perf \
                   dgemm_cuda_kij_multilevel.perf                  \
                   dgemm_cuda_kij_onelevel_nomanualblocking.perf   \
                   dgemm_cuda_multilevel_nomanualblocking.perf     \
                   dgemm_cuda_onelevel_nomanualblocking.perf       \
                   dgemm_cuda_multilevel.perf                      \
                   dgemm_cuda_onelevel.perf

PRV_CUDA_BINARIES=dgemm_cuda_kij_multilevel_nomanualblocking.instr \
                  dgemm_cuda_kij_multilevel.instr                  \
                  dgemm_cuda_kij_onelevel_nomanualblocking.instr   \
                  dgemm_cuda_multilevel_nomanualblocking.instr     \
                  dgemm_cuda_onelevel_nomanualblocking.instr       \
                  dgemm_cuda_multilevel.instr                      \
                  dgemm_cuda_onelevel.instr

DBG_CUDA_BINARIES=dgemm_cuda_kij_multilevel_nomanualblocking.verif \
                  dgemm_cuda_kij_multilevel.verif                  \
                  dgemm_cuda_kij_onelevel_nomanualblocking.verif   \
                  dgemm_cuda_multilevel_nomanualblocking.verif     \
                  dgemm_cuda_onelevel_nomanualblocking.verif       \
                  dgemm_cuda_multilevel.verif                      \
                  dgemm_cuda_onelevel.verif


perf_cuda: $(PERF_CUDA_BINARIES)

dgemm_cuda_multilevel.perf: $(all_files)
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DMANUALBLOCKING -o $@ $(common_build_files) src/matmul_regions_multilevel.c

dgemm_cuda_onelevel.perf: $(all_files)
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DMANUALBLOCKING -o $@ $(common_build_files) src/matmul_regions_onelevel.c

dgemm_cuda_multilevel_nomanualblocking.perf: $(all_files) src/matmul_regions_multilevel_nomanualblocking.c
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DNOMANUALBLOCKING -o $@ $(common_build_files) src/matmul_regions_multilevel_nomanualblocking.c

dgemm_cuda_onelevel_nomanualblocking.perf: $(all_files)
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DNOMANUALBLOCKING -o $@ $(common_build_files) src/matmul_regions_onelevel_nomanualblocking.c

dgemm_cuda_kij_multilevel_nomanualblocking.perf: $(all_files)
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DNOMANUALBLOCKING -o $@ $(common_build_files) src/matmul_kij_regions_multilevel_nomanualblocking.c

dgemm_cuda_kij_onelevel_nomanualblocking.perf: $(all_files)
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DNOMANUALBLOCKING -o $@ $(common_build_files) src/matmul_kij_regions_onelevel_nomanualblocking.c

dgemm_cuda_kij_multilevel.perf: $(all_files)
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DMANUALBLOCKING -o $@ $(common_build_files) src/matmul_kij_regions_multilevel.c

prv_cuda: $(PRV_CUDA_BINARIES)

dgemm_cuda_multilevel.instr: $(all_files)
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DMANUALBLOCKING --instrument -o $@ $(common_build_files) src/matmul_regions_multilevel.c

dgemm_cuda_onelevel.instr: $(all_files)
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DMANUALBLOCKING --instrument -o $@ $(common_build_files) src/matmul_regions_onelevel.c

dgemm_cuda_multilevel_nomanualblocking.instr: $(all_files)
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DNOMANUALBLOCKING --instrument -o $@ $(common_build_files) src/matmul_regions_multilevel_nomanualblocking.c

dgemm_cuda_onelevel_nomanualblocking.instr: $(all_files)
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DNOMANUALBLOCKING --instrument -o $@ $(common_build_files) src/matmul_regions_onelevel_nomanualblocking.c

dgemm_cuda_kij_multilevel_nomanualblocking.instr: $(all_files)
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DNOMANUALBLOCKING --instrument -o $@ $(common_build_files) src/matmul_kij_regions_multilevel_nomanualblocking.c

dgemm_cuda_kij_onelevel_nomanualblocking.instr: $(all_files)
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DNOMANUALBLOCKING --instrument -o $@ $(common_build_files) src/matmul_kij_regions_onelevel_nomanualblocking.c

dgemm_cuda_kij_multilevel.instr: $(all_files)
	$(CUDACC) $(CFLAGS) $(CUDA_LDFLAGS) -DMANUALBLOCKING --instrument -o $@ $(common_build_files) src/matmul_kij_regions_multilevel.c


dbg_cuda: $(DBG_CUDA_BINARIES)

dgemm_cuda_kij_multilevel_nomanualblocking.verif: $(all_files)
	$(CUDACC_DBG) $(CFLAGS) $(CUDA_LDFLAGS) -DNOMANUALBLOCKING -DVALIDATE -o $@ $(common_build_files) src/matmul_kij_regions_multilevel_nomanualblocking.c

dgemm_cuda_kij_onelevel_nomanualblocking.verif: $(all_files)
	$(CUDACC_DBG) $(CFLAGS) $(CUDA_LDFLAGS) -DNOMANUALBLOCKING -DVALIDATE -o $@ $(common_build_files) src/matmul_kij_regions_onelevel_nomanualblocking.c

dgemm_cuda_multilevel_nomanualblocking.verif: $(all_files)
	$(CUDACC_DBG) $(CFLAGS) $(CUDA_LDFLAGS) -DNOMANUALBLOCKING -DVALIDATE -o $@ $(common_build_files) src/matmul_regions_multilevel_nomanualblocking.c

dgemm_cuda_onelevel_nomanualblocking.verif: $(all_files)
	$(CUDACC_DBG) $(CFLAGS) $(CUDA_LDFLAGS) -DNOMANUALBLOCKING -DVALIDATE -o $@ $(common_build_files) src/matmul_regions_onelevel_nomanualblocking.c

dgemm_cuda_multilevel.verif: $(all_files)
	$(CUDACC_DBG) $(CFLAGS) $(CUDA_LDFLAGS) -DMANUALBLOCKING -DVALIDATE -o $@ $(common_build_files) src/matmul_regions_multilevel.c

dgemm_cuda_onelevel.verif: $(all_files)
	$(CUDACC_DBG) $(CFLAGS) $(CUDA_LDFLAGS) -DMANUALBLOCKING -DVALIDATE -o $@ $(common_build_files) src/matmul_regions_onelevel.c

dgemm_cuda_kij_multilevel.verif: $(all_files)
	$(CUDACC_DBG) $(CFLAGS) $(CUDA_LDFLAGS) -DMANUALBLOCKING -DVALIDATE -o $@ $(common_build_files) src/matmul_kij_regions_multilevel.c

dgemm_cuda_kij_onelevel.verif: $(all_files)
	$(CUDACC_DBG) $(CFLAGS) $(CUDA_LDFLAGS) -DMANUALBLOCKING -DVALIDATE -o $@ $(common_build_files) src/matmul_kij_regions_onelevel.c
