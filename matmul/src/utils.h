#include <stdio.h>

extern void print_all_manualblocking( size_t nblocks, size_t bsize, elem_t (*C)[nblocks][nblocks][bsize*bsize] );
extern void print_summary_manualblocking(  size_t nblocks, size_t bsize, elem_t (*C)[nblocks][nblocks][bsize*bsize] );
extern int verify_manualblocking( int colmajor, size_t nblocks, size_t bsize, int niter, elem_t (*C)[nblocks][nblocks][bsize*bsize]);
extern void print_addr_nomanualblocking( size_t nblocks, size_t bsize, elem_t (*A)[nblocks][nblocks][bsize*bsize] );

extern int verify_nomanualblocking(int colmajor, size_t dim, int niter, elem_t (*C)[dim][dim]);
extern void print_summary_nomanualblocking( size_t dim, size_t bsize, elem_t (*C)[dim][dim] );
extern void print_addresses_nomanualblocking( size_t dim, size_t bsize, elem_t (*A)[dim][dim] );

#include "alloc.h"
