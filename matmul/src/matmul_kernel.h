#ifdef SMP_KERNEL
#ifdef INTEL_MKL
#include <mkl_cblas.h>
#else
#include <cblas.h>
#endif
#endif

#ifdef MANUALBLOCKING
 #ifdef CUDA_KERNEL
  #include "kernels/matmul_kernel_cuda_manualblocking.h"
  #define matmul_kernel matmul_kernel_cuda_manualblocking
 #else
  #ifdef SMP_KERNEL
   #include "kernels/matmul_kernel_smp_manualblocking.h"
  #define matmul_kernel matmul_kernel_smp_manualblocking_mkl
  #else
   #error "Must define CUDA_KERNEL or SMP_KERNEL"
  #endif
 #endif
#else
 #ifdef NOMANUALBLOCKING
  #ifdef CUDA_KERNEL
   #include "kernels/matmul_kernel_cuda_nomanualblocking.h"
   #define matmul_kernel matmul_kernel_cuda_nomanualblocking
  #else
   #ifdef SMP_KERNEL
    #include "kernels/matmul_kernel_smp_nomanualblocking.h"
    #define matmul_kernel matmul_kernel_smp_nomanualblocking_mkl
   #else
    #error "Must define CUDA_KERNEL or SMP_KERNEL"
   #endif
  #endif
 #endif
#endif
