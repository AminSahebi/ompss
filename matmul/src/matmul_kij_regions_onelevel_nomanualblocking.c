/*************************************************************************************/
/*      Copyright 2009 Barcelona Supercomputing Center                               */
/*                                                                                   */
/*      This file is part of the NANOS++ library.                                    */
/*                                                                                   */
/*      NANOS++ is free software: you can redistribute it and/or modify              */
/*      it under the terms of the GNU Lesser General Public License as published by  */
/*      the Free Software Foundation, either version 3 of the License, or            */
/*      (at your option) any later version.                                          */
/*                                                                                   */
/*      NANOS++ is distributed in the hope that it will be useful,                   */
/*      but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*      GNU Lesser General Public License for more details.                          */
/*                                                                                   */
/*      You should have received a copy of the GNU Lesser General Public License     */
/*      along with NANOS++.  If not, see <http://www.gnu.org/licenses/>.             */
/*************************************************************************************/
#include <sys/time.h>
#include <stdio.h>
struct timeval timev1, timev2;
double time_seconds;

#include "matmul_data_size.h"
#include "matmul_defs.h"
#include "matmul_init.h"
#include "matmul_kernel.h"
#include "utils.h"

//#define USE_1_1_0_INIT

const unsigned int bsize = BSIZE;
const unsigned int dim = DIM;

int main ( int argc, char **argv )
{
   (void) argc;
   (void) argv;
   elem_t (*A)[dim][dim];
   elem_t (*B)[dim][dim];
   elem_t (*C)[dim][dim];
   unsigned int i;
   unsigned int k;

   if ( allocate_data(dim, bsize, &A, &B, &C) != 0) {
      return -1;
   }

   init_matrix_kij( COLMAJOR, dim, bsize, A, B, C );
#pragma omp taskwait noflush
   fprintf(stderr, "Matmul started # block size: %d / dimension: %d\n", bsize, dim);
   gettimeofday(&timev1, NULL);

   for (k = 0; k < dim; k += bsize)
   {
      for (i = 0; i < dim; i += bsize)
      {
         unsigned int j=0;
         for (j = 0; j < dim; j += bsize)
         {
            matmul_kernel( i, j, k, bsize, dim, A, B, C );
         }
      }
   }

#ifdef VALIDATE 
#pragma omp taskwait 
#else
#pragma omp taskwait noflush
#endif

   gettimeofday(&timev2, NULL);
   time_seconds = timev2.tv_sec - timev1.tv_sec;
   time_seconds += 0.000001 * (timev2.tv_usec - timev1.tv_usec);

   double gflops =  ( 2.0e-9 * dim * dim ) * dim / time_seconds;
#ifdef VALIDATE 
   //write_matrix( (elem_t* (*)[NUM_BLOCKS])C);
   int res = verify_nomanualblocking( COLMAJOR, dim, 1, C );
   if (res == 0) 
      fprintf(stderr,"Verification ok.\n");
   else
      fprintf(stderr,"Verification failed.\n");
   //print_summary( C );
#endif
   fprintf(stderr, "Execution time: %f seconds %f GFLOPS\n", time_seconds, gflops );
   return 0;
}
