#include <math.h>
#include <stdio.h>
#include "matmul_defs.h"
#include <stdio.h>
void print_all_manualblocking( size_t nblocks, size_t bsize, elem_t (*C)[nblocks][nblocks][bsize*bsize] ) {
   unsigned int ii, iii, jj, jjj;
   fprintf(stderr, "------\n");
   for (ii=0; ii < nblocks; ii+=1) {
         for (iii=0; iii < nblocks; iii+=1) {
         fprintf(stderr, " %d, %d\t", ii, iii);
      for ( jj=0; jj<bsize; jj+=1) {
            for ( jjj=0; jjj<bsize; jjj+=1)
               fprintf(stderr, " %2.9f",  (*C)[ii][iii][jj*bsize+jjj]); 
         fprintf(stderr, "\n\t");
         }
         fprintf(stderr, "\n");
      }
   }
   fprintf(stderr, "------\n");
}
void print_summary_manualblocking(  size_t nblocks, size_t bsize, elem_t (*C)[nblocks][nblocks][bsize*bsize] ) {
   unsigned int ii, iii;
   fprintf(stderr, "------\n");
   for (ii=0; ii < nblocks; ii+=1) {
      for (iii=0; iii < nblocks; iii+=1) {
         fprintf(stderr, " %4.0f", (*C)[ii][iii][0]); 
      }
      fprintf(stderr, "\n");
   }
   fprintf(stderr, "------\n");
}



static int verify_manualblocking_colmajor( size_t nblocks, size_t bsize, int niter, elem_t (*C)[nblocks][nblocks][bsize*bsize])
{
   unsigned int i,j,ii,jj;
   int res = 0;
   for (ii = 0; ii < nblocks; ii++) {
      for (jj = 0; jj < nblocks; jj++) {
         for (i = 0; i < bsize; i++) {
            for (j = 0; j < bsize; j++) {
               if((ii*bsize+j)<2) continue;
#ifdef USE_1_1_0_INIT
               if ( (*C)[ii][jj][i * bsize + j] == nblocks * bsize ) continue;
               else { res = 1; fprintf(stderr, "error at C[%d][%d][%d]=%f\n", ii, jj, i * bsize + j, (*C)[ii][jj][i * bsize + j] ); return res; }
#else
               elem_t i1 = 1.0/((elem_t)(ii*bsize)+j+1);
               elem_t shb = niter * (pow (i1, (elem_t)((jj*bsize)+i+1))-1) / (i1-1);
               elem_t diff = ((*C)[ii][jj][i * bsize + j]-shb)/shb;
               if (diff < 0) diff = -diff;

               if (diff > 1e-4)
               {
                  res = 22;
                  fprintf(stderr, "error at %d, %d, %d, %d\n", ii, jj, i, j);
                  //fprintf(stderr,"Verification FAILED\n");
                  return res;
               }
#endif
            }
         }
      }
   }
   return res;
}

static int verify_manualblocking_rowmajor( size_t nblocks, size_t bsize, int niter, elem_t (*C)[nblocks][nblocks][bsize*bsize])
{
   unsigned int i,j,ii,jj;
   int res = 0;
   for (ii = 0; ii < nblocks; ii++) {
      for (jj = 0; jj < nblocks; jj++) {
         for (i = 0; i < bsize; i++) {
            for (j = 0; j < bsize; j++) {
               if((ii*bsize+i)<2) continue;
#ifdef USE_1_1_0_INIT
               if ( (*C)[ii][jj][i * bsize + j] == nblocks * bsize ) continue;
               else { res = 1; fprintf(stderr, "error at C[%d][%d][%d]=%f\n", ii, jj, i * bsize + j, (*C)[ii][jj][i * bsize + j] ); return res; }
#else
               elem_t i1 = 1.0/((elem_t)(ii*bsize)+i+1);
               elem_t shb = niter * (pow (i1, (elem_t)((jj*bsize)+j+1))-1) / (i1-1);
               elem_t diff = ((*C)[ii][jj][i * bsize + j]-shb)/shb;
               if (diff < 0) diff = -diff;

               if (diff > 1e-4)
               {
                  res = 22;
                  fprintf(stderr, "error at %d, %d, %d, %d\n", ii, jj, i, j);
                  //fprintf(stderr,"Verification FAILED\n");
                  return res;
               }
#endif
            }
         }
      }
   }
   return res;
}

int verify_manualblocking( int colmajor, size_t nblocks, size_t bsize, int niter, elem_t (*C)[nblocks][nblocks][bsize*bsize])
{
   int result;
   if ( colmajor ) {
      result = verify_manualblocking_colmajor( nblocks, bsize, niter, C);
   } else {
      result = verify_manualblocking_rowmajor( nblocks, bsize, niter, C);
   }
   return result;
}

void print_addr_manualblocking( size_t nblocks, size_t bsize, elem_t (*A)[nblocks][nblocks][bsize*bsize] )
{
   unsigned int i, j;
   for ( i = 0; i < nblocks; i += 1 ) {
      for ( j = 0; j < nblocks; j += 1 ) {
         fprintf( stderr, "%p ", &((*A)[i][j][0]) );
      }
      fprintf( stderr, "\n");
   }
   fprintf( stderr, "\n");
}



int verify_nomanualblocking(size_t dim, int niter, elem_t (*C)[dim][dim])
{
   unsigned int i,j;
   int res = 0;
   (void) niter;
   for (i = 0; i < dim; i++) {
      for (j = 0; j < dim; j++) {
         if((i)<2) continue;
#ifdef USE_1_1_0_INIT
         if ( (*C)[i][j] == dim ) continue;
         else { res = 1; fprintf(stderr, "error at C[%d][%d]=%f\n", i, j, (*C)[i][j] ); return res; }
#else
         elem_t i1 = 1.0/((elem_t)i+1);
         elem_t shb = niter * (pow (i1, (elem_t)(j+1))-1) / (i1-1);
         elem_t diff = ((*C)[i][j]-shb)/shb;
         if (diff < 0) diff = -diff;

         if (diff > 1e-4)
         {
            res = 22;
            fprintf(stderr, "error at %d, %d\n", i, j);
            //fprintf(stderr,"Verification FAILED\n");
            return res;
         }
#endif
      }
   }
   return res;
}

void print_summary_nomanualblocking( size_t dim, size_t bsize, elem_t (*C)[dim][dim] ) {
   unsigned int ii, iii;
   fprintf(stderr, "------\n");
   for (ii=0; ii < dim; ii+= bsize ) {
      for (iii=0; iii < dim; iii+= bsize) {
         fprintf(stderr, " %2.9f", (*C)[ii][iii]); 
      }
      fprintf(stderr, "\n");
   }
   fprintf(stderr, "------\n");
}

void print_all_nomanualblocking( size_t dim, elem_t (*C)[dim][dim] ) {
   unsigned int ii, iii;
   fprintf(stderr, "------\n");
   for (ii=0; ii < dim; ii+= 1 ) {
      for (iii=0; iii < dim; iii+= 1) {
         fprintf(stderr, " %2.9f", (*C)[ii][iii]); 
      }
      fprintf(stderr, "\n");
   }
   fprintf(stderr, "------\n");
}

void print_addresses_nomanualblocking( size_t dim, size_t bsize, elem_t (*A)[dim][dim] )
{
   unsigned int ii;
   unsigned int jj;
   for ( ii= 0; ii < dim; ii+=bsize ) {
      fprintf(stderr, "\t");
      for ( jj= 0; jj < dim; jj+=bsize ) {
         fprintf(stderr, " %p", &(*A)[ ii ][ jj ] );
      }
      fprintf(stderr, "\n");
   }
}
