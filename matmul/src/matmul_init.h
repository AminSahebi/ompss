#ifdef MANUALBLOCKING
 #include "matmul_init_manualblocking.h"
 #define init_matrix init_matrix_manualblocking
 #define init_matrix_kij init_matrix_kij_manualblocking
#else
 #ifdef NOMANUALBLOCKING
  #include "matmul_init_nomanualblocking.h"
  #define init_matrix init_matrix_nomanualblocking
  #define init_matrix_kij init_matrix_kij_nomanualblocking
 #else
  #error "Must define MANUALBLOCKING or NOMANUALBLOCKING"
 #endif
#endif
