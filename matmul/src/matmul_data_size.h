#ifndef MATMUL_DATA_SIZE_H
#define MATMUL_DATA_SIZE_H

#define NBLOCKS             (8)
#define BSIZE              (256)
#define DIM      (NBLOCKS*BSIZE)

#endif
