#ifndef MATMUL_INIT_NOMANUALBLOCKING
#define MATMUL_INIT_NOMANUALBLOCKING
#include "matmul_defs.h"
void init_matrix_nomanualblocking( int colmajor, size_t dim, size_t bsize,
                                   elem_t (*A)[dim][dim],
                                   elem_t (*B)[dim][dim],
                                   elem_t (*C)[dim][dim]);
#endif
