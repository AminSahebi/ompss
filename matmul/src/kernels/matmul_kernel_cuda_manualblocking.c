#include "../matmul_defs.h"
#include "../matmul_data_size.h" //fix since cudacc does not support VLAs
void matmul_kernel_cuda_manualblocking( unsigned int i,
                                        unsigned int j,
                                        unsigned int k,
                                        size_t nblocks,
                                        size_t bsize,
                                        elem_t (*A)[nblocks][nblocks][bsize*bsize],
                                        elem_t (*B)[nblocks][nblocks][bsize*bsize],
                                        elem_t (*C)[nblocks][nblocks][bsize*bsize] )
{
   elem_t (*A_ptr)[NBLOCKS][NBLOCKS][BSIZE*BSIZE] = A;
   elem_t (*B_ptr)[NBLOCKS][NBLOCKS][BSIZE*BSIZE] = B;
   elem_t (*C_ptr)[NBLOCKS][NBLOCKS][BSIZE*BSIZE] = C;
#pragma omp target device (cuda) copy_deps
#pragma omp task in( (*A_ptr)[i:i][k:k][0:(bsize*bsize)-1],  \
                     (*B_ptr)[k:k][j:j][0:(bsize*bsize)-1] ) \
                 inout( (*C_ptr)[i:i][j:j][0:(bsize*bsize)-1] ) \
   firstprivate( i, j, k, nblocks, bsize ) label(matmul_kernel)
   {
      //#ifdef USE_1_1_0_INIT
      //unsigned int thisNode = -1; nanos_get_node_num( &thisNode );
      //if ( (*A)[i][k][0] == 0 || (*B)[k][j][0] == 0 ) printf("#####################################################################[%p @ %d] do matmul block: A[%d][%d]{%p}=%f B[%d][%d]{%p}=%f C[%d][%d]{%p}=%f\n",nanos_current_wd(), thisNode, i, k, &(*A)[i][k][0], (*A)[i][k][0], k, j, &(*B)[k][j][0], (*B)[k][j][0], i, j, &(*C)[i][j][0], (*C)[i][j][0] );
      //#endif
      cudaStream_t stream = nanos_get_kernel_execution_stream();
      cublasStatus_t cberr = CUBLAS_STATUS_SUCCESS;
      if (stream) {
         cublasSetKernelStream(stream);
      }
      cublasHandle_t handle = nanos_get_cublas_handle();
      elem_t alpha = 1.0 , beta = 1.0;
      cberr = _cublasgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, bsize, bsize, bsize, &alpha, &((*A_ptr)[i][k][0]),
            bsize, &((*B_ptr)[k][j][0]), bsize, &beta, &((*C_ptr)[i][j][0]), bsize);
      if (cberr != CUBLAS_STATUS_SUCCESS) {
         printf("CUBLAS error!\n");
      }
   }
}
