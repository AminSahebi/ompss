#include "../matmul_defs.h"

void matmul_kernel_smp_nomanualblocking_mkl( unsigned int i,
                                             unsigned int j,
                                             unsigned int k,
                                             size_t bsize,
                                             size_t dim,
                                             elem_t (*A)[dim][dim],
                                             elem_t (*B)[dim][dim],
                                             elem_t (*C)[dim][dim] )
{
#ifdef PLAIN_DEPS
#pragma omp target device (smp) no_copy_deps          \
                 copy_in(    (*A)[i;bsize][k;bsize],  \
                             (*B)[k;bsize][j;bsize] ) \
                 copy_inout( (*C)[i;bsize][j;bsize] )
#pragma omp task in(    (*A)[i][k],  \
                        (*B)[k][j] ) \
                 inout( (*C)[i][j] ) \
                 firstprivate( i, j, k, bsize, dim )
#else
#pragma omp target device (smp) copy_deps
#pragma omp task in(    (*A)[i;bsize][k;bsize],   \
                        (*B)[k;bsize][j;bsize] )  \
                 inout( (*C)[i;bsize][j; bsize] ) \
                 firstprivate( i, j, k, bsize, dim )
#endif
   {
#ifdef USE_1_1_0_INIT
              int thisNode = -1; nanos_get_node_num( &thisNode );
              if ( (*A)[i][k] == 0 || (*B)[k][j] == 0 ) { printf("#####################################################################[%p : %d @ %d] do matmul block: A[%d][%d]{%p}=%f B[%d][%d]{%p}=%f C[%d][%d]{%p}=%f\n", nanos_current_wd(), nanos_get_wd_id(nanos_current_wd()), thisNode, i, k, &((*A)[i][k]), (*A)[i][k], k, j, &((*B)[k][j]), (*B)[k][j], i, j, &((*C)[i][j]), (*C)[i][j] ); }
              //if ( 1 ) { printf("#####################################################################[%p : %d @ %d] do matmul block: A[%d][%d]{%p}=%f B[%d][%d]{%p}=%f C[%d][%d]{%p}=%f\n", nanos_current_wd(), nanos_get_wd_id(nanos_current_wd()), thisNode, i, k, &((*A)[i][k]), (*A)[i][k], k, j, &((*B)[k][j]), (*B)[k][j], i, j, &((*C)[i][j]), (*C)[i][j] ); }
#endif
      cblas_dgemm( CblasRowMajor, CblasNoTrans, CblasNoTrans, bsize, bsize, bsize, 1, &((*A)[i][k]), dim, &((*B)[k][j]), dim, 1, &((*C)[i][j]), dim );
   }
}



