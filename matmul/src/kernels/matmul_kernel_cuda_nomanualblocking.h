#ifndef MATMUL_KERNEL_CUDA_NOMANUALBLOCKING_H
#define MATMUL_KERNEL_CUDA_NOMANUALBLOCKING_H
void matmul_kernel_cuda_nomanualblocking( unsigned int i,
                                          unsigned int j,
                                          unsigned int k,
                                          size_t bsize,
                                          size_t dim,
                                          elem_t *A_ptr,
                                          elem_t *B_ptr,
                                          elem_t *C_ptr );
#endif
