#include "../matmul_defs.h"

void matmul_kernel_smp_manualblocking_mkl( unsigned int i,
                                           unsigned int j,
                                           unsigned int k,
                                           size_t nblocks,
                                           size_t bsize,
                                           elem_t (* __restrict__ A)[nblocks][nblocks][bsize*bsize],
                                           elem_t (* __restrict__ B)[nblocks][nblocks][bsize*bsize],
                                           elem_t (* __restrict__ C)[nblocks][nblocks][bsize*bsize] )
{
#ifdef PLAIN_DEPS
#pragma omp target device (smp) no_copy_deps \
      copy_in( (*A)[i:i][k:k][0:(bsize*bsize)-1], \
               (*B)[k:k][j:j][0:(bsize*bsize)-1] ) \
      copy_inout( (*C)[i:i][j:j][0:(bsize*bsize)-1] )
#pragma omp task label(matmul_kernel) \
      in( (*A)[i][k][0], \
          (*B)[k][j][0] ) \
      inout( (*C)[i][j][0] ) \
      firstprivate( i, j, k, bsize )
#else
#pragma omp target device (smp) copy_deps
#pragma omp task label(matmul_kernel) \
      in( (*A)[i:i][k:k][0:(bsize*bsize)-1], \
          (*B)[k:k][j:j][0:(bsize*bsize)-1] ) \
      inout( (*C)[i:i][j:j][0:(bsize*bsize)-1] ) \
      firstprivate( i, j, k, bsize )
#endif
   {
//#ifdef USE_1_1_0_INIT
//              unsigned int thisNode = -1; nanos_get_node_num( &thisNode );
//              if ( (*A)[ii][kk][0] == 0 || (*B)[kk][jj][0] == 0 ) printf("#####################################################################[%p @ %d] do matmul block: A[%d][%d]{%p}=%f B[%d][%d]{%p}=%f C[%d][%d]{%p}=%f\n",nanos_current_wd(), thisNode, ii, kk, &(*A)[ii][kk][0], (*A)[ii][kk][0], kk, jj, &(*B)[kk][jj][0], (*B)[kk][jj][0], ii, jj, &(*C)[ii][jj][0], (*C)[ii][jj][0] );
//#endif
   
         //printf("A[%d][0][0] is %p\n", i, &((*A)[i][0][0]) );
         //printf("B[%d][0][0] is %p\n", i, &((*B)[i][0][0]) );
         //printf("C[%d][0][0] is %p\n", i, &((*C)[i][0][0]) );
      //cblas_dgemm( CblasRowMajor, CblasNoTrans, CblasNoTrans, bsize, bsize, bsize, 1, (*A)[i][k], bsize, (*B)[k][j], bsize, 1, (*C)[i][j], bsize );
      for ( unsigned int ii = 0; ii < bsize; ii += 1)
      {
          for ( unsigned int kk = 0; kk < bsize; kk += 1)
          {
              elem_t tmp = (*A)[i][k][ii*bsize+kk];
              for ( unsigned int jj = 0; jj < bsize; jj += 1)
              {
                  (*C)[i][j][ii*bsize+jj] += tmp * (*B)[k][j][kk*bsize+jj];
              }
          }
      }
   }
}
