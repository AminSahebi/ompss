#include "../matmul_defs.h"

void matmul_kernel_smp_manualblocking_mkl( unsigned int i,
                                           unsigned int j,
                                           unsigned int k,
                                           size_t nblocks,
                                           size_t bsize,
                                           elem_t (*A)[nblocks][nblocks][bsize*bsize],
                                           elem_t (*B)[nblocks][nblocks][bsize*bsize],
                                           elem_t (*C)[nblocks][nblocks][bsize*bsize] )
{
#pragma omp target device (smp) copy_deps
#pragma omp task label(matmul_kernel) \
      in( (*A)[i:i][k:k][0:(bsize*bsize)-1], \
          (*B)[k:k][j:j][0:(bsize*bsize)-1] ) \
      inout( (*C)[i:i][j:j][0:(bsize*bsize)-1] ) \
      firstprivate( i, j, k, bsize )
   {
      for ( unsigned int ii = 0; ii < bsize; ii += 1)
      {
          for ( unsigned int jj = 0; jj < bsize; jj += 1)
          {
              for ( unsigned int kk = 0; kk < bsize; kk += 1)
              {
                  (*C)[i][j][ii*bsize+jj] += (*A)[i][k][ii*bsize+kk] * (*B)[k][j][kk*bsize+jj];
              }
          }
      }
   }
}
