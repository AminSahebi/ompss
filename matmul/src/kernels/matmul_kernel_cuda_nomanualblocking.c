#include "../matmul_defs.h"
#include "../matmul_data_size.h" //fix since cudacc does not support VLAs

void matmul_kernel_cuda_nomanualblocking( unsigned int i,
                                          unsigned int j,
                                          unsigned int k,
                                          size_t bsize,
                                          size_t dim,
                                          elem_t (*A)[dim][dim],
                                          elem_t (*B)[dim][dim],
                                          elem_t (*C)[dim][dim] )
{
   elem_t (*A_ptr)[DIM][DIM] = A;
   elem_t (*B_ptr)[DIM][DIM] = B;
   elem_t (*C_ptr)[DIM][DIM] = C;
#pragma omp target device (cuda) copy_deps
#pragma omp task in( (*A_ptr)[ k ; bsize ][ i ; bsize ],  \
                     (*B_ptr)[ j ; bsize ][ k ; bsize ] ) \
                 inout( (*C_ptr)[ j ; bsize ][ i ; bsize ] ) \
                 firstprivate( i, j, k, bsize, dim )
   {
      //#ifdef USE_1_1_0_INIT
      //              unsigned int thisNode = -1; nanos_get_node_num( &thisNode );
      //              if ( (*A)[ii][kk][0] == 0 || (*B)[kk][jj][0] == 0 ) printf("#####################################################################[%p @ %d] do matmul block: A[%d][%d]{%p}=%f B[%d][%d]{%p}=%f C[%d][%d]{%p}=%f\n",nanos_current_wd(), thisNode, ii, kk, &(*A)[ii][kk][0], (*A)[ii][kk][0], kk, jj, &(*B)[kk][jj][0], (*B)[kk][jj][0], ii, jj, &(*C)[ii][jj][0], (*C)[ii][jj][0] );
      //#endif
      cudaStream_t stream = nanos_get_kernel_execution_stream();
      cublasStatus_t cberr = CUBLAS_STATUS_SUCCESS;
      if (stream) {
         cublasSetKernelStream(stream);
      }
      cublasHandle_t handle = nanos_get_cublas_handle();
      elem_t alpha = 1.0 , beta = 1.0;
      cberr = _cublasgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, bsize, bsize, bsize, &alpha, &((*A_ptr)[k][i]),
            dim, &((*B_ptr)[j][k]), dim, &beta, &((*C_ptr)[j][i]), dim);
      if (cberr != CUBLAS_STATUS_SUCCESS) {
         printf("CUBLAS error!\n");
      }
   }
}
