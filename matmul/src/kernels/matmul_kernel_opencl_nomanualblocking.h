#ifndef MATMUL_KERNEL_OPENCL_NOMANUALBLOCKING_H
#define MATMUL_KERNEL_OPENCL_NOMANUALBLOCKING_H
extern void matmul_kernel_opencl_nomanualblocking( unsigned int i,
                                                   unsigned int j,
                                                   unsigned int k,
                                                   size_t bsize,
                                                   size_t dim,
                                                   elem_t (*A)[dim][dim],
                                                   elem_t (*B)[dim][dim],
                                                   elem_t (*C)[dim][dim] );
#endif
