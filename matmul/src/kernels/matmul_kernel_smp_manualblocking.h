#ifndef MATMUL_KERNEL_SMP_MANUALBLOCKING_H
#define MATMUL_KERNEL_SMP_MANUALBLOCKING_H

#define _matmul_kernel matmul_kernel_smp_manualblocking_mkl
extern void matmul_kernel_smp_manualblocking_mkl( unsigned int i,
                                                  unsigned int j,
                                                  unsigned int k,
                                                  size_t nblocks,
                                                  size_t bsize,
                                                  elem_t (*A)[nblocks][nblocks][bsize*bsize],
                                                  elem_t (*B)[nblocks][nblocks][bsize*bsize],
                                                  elem_t (*C)[nblocks][nblocks][bsize*bsize] );
#endif
