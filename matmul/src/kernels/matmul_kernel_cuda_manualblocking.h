#ifndef MATMUL_KERNEL_CUDA_MANUALBLOCKING_H
#define MATMUL_KERNEL_CUDA_MANUALBLOCKING_H
void matmul_kernel_cuda_manualblocking( unsigned int i,
                                        unsigned int j,
                                        unsigned int k,
                                        size_t nblocks,
                                        size_t bsize,
                                        elem_t (*A)[nblocks][nblocks][bsize*bsize],
                                        elem_t (*B)[nblocks][nblocks][bsize*bsize],
                                        elem_t (*C)[nblocks][nblocks][bsize*bsize] );
#endif
