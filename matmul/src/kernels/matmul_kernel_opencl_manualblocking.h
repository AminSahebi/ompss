#ifndef MATMUL_KERNEL_OPENCL_MANUALBLOCKING_H
#define MATMUL_KERNEL_OPENCL_MANUALBLOCKING_H

#define _matmul_kernel matmul_kernel_opencl_manualblocking
extern void matmul_kernel_opencl_manualblocking( unsigned int i,
                                                 unsigned int j,
                                                 unsigned int k,
                                                 size_t nblocks,
                                                 size_t bsize,
                                                 elem_t (*A)[nblocks][nblocks][bsize*bsize],
                                                 elem_t (*B)[nblocks][nblocks][bsize*bsize],
                                                 elem_t (*C)[nblocks][nblocks][bsize*bsize] );
#endif
