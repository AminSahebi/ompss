#include "matmul_defs.h"
#include "matmul_kernel.h"

#ifdef MANUALBLOCKING
 #ifdef CUDA_KERNEL
  #include "kernels/matmul_kernel_cuda_manualblocking.c"
 #else
  #ifdef SMP_KERNEL
   #include "kernels/matmul_kernel_smp_manualblocking.c"
  #else
   #error "Must define CUDA_KERNEL or SMP_KERNEL"
  #endif
 #endif
#else
 #ifdef NOMANUALBLOCKING
  #ifdef CUDA_KERNEL
   #include "kernels/matmul_kernel_cuda_nomanualblocking.c"
  #else
   #ifdef SMP_KERNEL
    #include "kernels/matmul_kernel_smp_nomanualblocking.c"
   #else
    #error "Must define CUDA_KERNEL or SMP_KERNEL"
   #endif
  #endif
 #endif
#endif
