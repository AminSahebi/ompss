#include <math.h>
#include "matmul_defs.h"
static void init_matrix_manualblocking_rowmajor(size_t nblocks, size_t bsize,
                                                elem_t (*A)[nblocks][nblocks][bsize*bsize],
                                                elem_t (*B)[nblocks][nblocks][bsize*bsize],
                                                elem_t (*C)[nblocks][nblocks][bsize*bsize])
{
   unsigned int i;
   for (i = 0; i < nblocks; i++) {
#ifdef PLAIN_DEPS
#pragma omp target device (smp) no_copy_deps \
            copy_out( (*A)[i:i][0;nblocks][0;bsize*bsize], \
                      (*B)[0;nblocks][i:i][0;bsize*bsize], \
                      (*C)[0;nblocks][i:i][0;bsize*bsize])
#pragma omp task label(init) \
                 firstprivate( i, nblocks, bsize ) \
                 out( (*A)[i][0][0], \
                      (*B)[0][i][0], \
                      (*C)[0][i][0])
#else
#pragma omp target device (smp) copy_deps
#pragma omp task label(init) \
                 firstprivate( i, nblocks, bsize ) \
                 out( (*A)[i:i][0;nblocks][0;bsize*bsize], \
                      (*B)[0;nblocks][i:i][0;bsize*bsize], \
                      (*C)[0;nblocks][i:i][0;bsize*bsize])
#endif
      {
         unsigned int j, ii, jj;
         // printf("A[%d][0][0] is %p\n", i, &((*A)[i][0][0]) );
         // printf("B[%d][0][0] is %p\n", i, &((*B)[i][0][0]) );
         // printf("C[%d][0][0] is %p\n", i, &((*C)[i][0][0]) );
         for (j = 0; j < nblocks; j++) {
            for (ii = 0; ii < bsize; ii++) {
               for (jj = 0; jj < bsize; jj++) {
#ifdef USE_1_1_0_INIT
                  ((*A)[i][j])[ii*bsize+jj] = 1;
                  ((*B)[j][i])[ii*bsize+jj] = 1;
#else
                  ((*A)[i][j])[ii*bsize+jj] = pow (1.0/(elem_t)((i*bsize)+ii+1), (elem_t)(j*bsize)+jj);
                  ((*B)[j][i])[ii*bsize+jj] = ((j*bsize)+ii)<=((i*bsize)+jj);
#endif
                  ((*C)[j][i])[ii*bsize+jj] = 0;
               }
            }
         }
      }
   }
}

static void init_matrix_manualblocking_colmajor(size_t nblocks, size_t bsize,
                                                elem_t (*A)[nblocks][nblocks][bsize*bsize],
                                                elem_t (*B)[nblocks][nblocks][bsize*bsize],
                                                elem_t (*C)[nblocks][nblocks][bsize*bsize])
{
   unsigned int i;
   for (i = 0; i < nblocks; i++) {
         //printf("A[%d][0][0] is %p\n", i, &((*A)[i][0][0]) );
         //printf("B[%d][0][0] is %p\n", i, &((*B)[i][0][0]) );
         //printf("C[%d][0][0] is %p\n", i, &((*C)[i][0][0]) );
#ifdef PLAIN_DEPS
#pragma omp target device (smp) no_copy_deps \
            copy_out( (*A)[i:i][0;nblocks][0;bsize*bsize], \
                      (*B)[0;nblocks][i:i][0;bsize*bsize], \
                      (*C)[0;nblocks][i:i][0;bsize*bsize])
#pragma omp task label(init) \
                 firstprivate( i, nblocks, bsize ) \
                 out( (*A)[i][0][0], \
                      (*B)[0][i][0], \
                      (*C)[0][i][0])
#else
#pragma omp target device (smp) copy_deps
#pragma omp task label(init) \
                 firstprivate( i, nblocks, bsize ) \
                 out( (*A)[i:i][0;nblocks][0;bsize*bsize], \
                      (*B)[0;nblocks][i:i][0;bsize*bsize], \
                      (*C)[0;nblocks][i:i][0;bsize*bsize])
#endif
      {
         unsigned int j, ii, jj;
         for (j = 0; j < nblocks; j++) {
            for (ii = 0; ii < bsize; ii++) {
               for (jj = 0; jj < bsize; jj++) {
#ifdef USE_1_1_0_INIT
                  ((*A)[i][j])[ii*bsize+jj] = 1;
                  ((*B)[j][i])[ii*bsize+jj] = 1;
#else
                  ((*A)[i][j])[ii*bsize+jj] = pow (1.0/(elem_t)((i*bsize)+jj+1), (elem_t)(j*bsize)+ii);
                  ((*B)[j][i])[ii*bsize+jj] = ((j*bsize)+jj)<=((i*bsize)+ii);
#endif
                  ((*C)[j][i])[ii*bsize+jj] = 0;
               }
            }
         }
      }
   }
}

static void init_matrix_kij_manualblocking_colmajor(size_t nblocks, size_t bsize,
                                                    elem_t (*A)[nblocks][nblocks][bsize*bsize],
                                                    elem_t (*B)[nblocks][nblocks][bsize*bsize],
                                                    elem_t (*C)[nblocks][nblocks][bsize*bsize])
{
   unsigned int i;
   for (i = 0; i < nblocks; i++) {
         //printf("A[%d][0][0] is %p\n", i, &((*A)[i][0][0]) );
         //printf("B[%d][0][0] is %p\n", i, &((*B)[i][0][0]) );
         //printf("C[%d][0][0] is %p\n", i, &((*C)[i][0][0]) );
#ifdef PLAIN_DEPS
#pragma omp target device (smp) no_copy_deps \
            copy_out( (*A)[i:i][0;nblocks][0;bsize*bsize], \
                      (*B)[i:i][0;nblocks][0;bsize*bsize], \
                      (*C)[i:i][0;nblocks][0;bsize*bsize])
#pragma omp task label(init_kij_manualblocking_colmajor) \
                 firstprivate( i, nblocks, bsize ) \
                 out( (*A)[i][0][0], \
                      (*B)[i][0][0], \
                      (*C)[i][0][0])
#else
#pragma omp target device (smp) copy_deps
#pragma omp task label(init_kij_manualblocking_colmajor) \
                 firstprivate( i, nblocks, bsize ) \
                 out( (*A)[i:i][0;nblocks][0;bsize*bsize], \
                      (*B)[i:i][0;nblocks][0;bsize*bsize], \
                      (*C)[i:i][0;nblocks][0;bsize*bsize])
#endif
      {
         unsigned int j, ii, jj;
         for (j = 0; j < nblocks; j++) {
            for (ii = 0; ii < bsize; ii++) {
               for (jj = 0; jj < bsize; jj++) {
#ifdef USE_1_1_0_INIT
                  ((*A)[i][j])[ii*bsize+jj] = 1;
                  ((*B)[i][j])[ii*bsize+jj] = 1;
#else
                  ((*A)[i][j])[ii*bsize+jj] = pow (1.0/(elem_t)((i*bsize)+jj+1), (elem_t)(j*bsize)+ii);
                  ((*B)[i][j])[ii*bsize+jj] = ((i*bsize)+jj)<=((j*bsize)+ii);
#endif
                  ((*C)[i][j])[ii*bsize+jj] = 0;
               }
            }
         }
      }
   }
}

void init_matrix_manualblocking(int colmajor, size_t nblocks, size_t bsize,
                                elem_t (*A)[nblocks][nblocks][bsize*bsize],
                                elem_t (*B)[nblocks][nblocks][bsize*bsize],
                                elem_t (*C)[nblocks][nblocks][bsize*bsize])
{
   if ( colmajor ) {
      init_matrix_manualblocking_colmajor(nblocks, bsize, A, B, C);
   } else {
      init_matrix_manualblocking_rowmajor(nblocks, bsize, A, B, C);
   }
}

void init_matrix_kij_manualblocking(int colmajor, size_t nblocks, size_t bsize,
                                elem_t (*A)[nblocks][nblocks][bsize*bsize],
                                elem_t (*B)[nblocks][nblocks][bsize*bsize],
                                elem_t (*C)[nblocks][nblocks][bsize*bsize])
{
   if ( colmajor ) {
      init_matrix_kij_manualblocking_colmajor(nblocks, bsize, A, B, C);
   } else {
      fprintf(stderr, "unimplemented\n");
      //init_matrix_kij_manualblocking_rowmajor(nblocks, bsize, A, B, C);
   }
}
