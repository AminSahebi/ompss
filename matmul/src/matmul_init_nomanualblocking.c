#include <math.h>
#include "matmul_defs.h"
void init_matrix_nomanualblocking_rowmajor( size_t dim, size_t bsize,
                                            elem_t (*A)[dim][dim],
                                            elem_t (*B)[dim][dim],
                                            elem_t (*C)[dim][dim])
{
   unsigned int i;
   for (i = 0; i < dim; i += bsize ) {
#ifdef PLAIN_DEPS
#pragma omp target device (smp) no_copy_deps \
            copy_out( (*A)[i;bsize][0;dim], \
                      (*B)[i;bsize][0;dim], \
                      (*C)[i;bsize][0;dim])
#pragma omp task label(init) \
                 firstprivate( bsize, dim, i ) \
                 out( (*A)[i][0], \
                      (*B)[i][0], \
                      (*C)[i][0])
#else
#pragma omp target device (smp) copy_deps
#pragma omp task label(init) \
                 firstprivate( bsize, dim, i ) \
                 out( (*A)[i;bsize][0;dim], \
                      (*B)[i;bsize][0;dim], \
                      (*C)[i;bsize][0;dim])
#endif
   {
      unsigned int j, ii, jj;
      unsigned int thisNode = -1; nanos_get_node_num( &thisNode );
      //printf("%d A=%p {%p - %p} B=%p {%p - %p} C=%p {%p - %p} bsize= %d dim=%d i=%d\n", thisNode, A, &((*A)[i][0]), &((*A)[i+bsize][0]), B, &((*B)[i][0]), &((*B)[i+bsize][0]), C, &((*C)[i][0]), &((*C)[i+bsize][0]), bsize, dim, i);
      for (ii = i; ii < i + bsize; ii++) {
        for (j = 0; j < dim; j++) {
#ifdef USE_1_1_0_INIT
           ((*A)[ii][j]) = 1;
           ((*B)[ii][j]) = 1;
#else
           ((*A)[ii][j]) = pow (1.0/(elem_t)(ii+1), (elem_t)j);
           ((*B)[ii][j]) = (ii)<=(j);
#endif
           ((*C)[ii][j]) = 0;
        }
     }
   }
   }
}

void init_matrix_nomanualblocking_colmajor( size_t dim, size_t bsize,
                                            elem_t (*A)[dim][dim],
                                            elem_t (*B)[dim][dim],
                                            elem_t (*C)[dim][dim])
{
   unsigned int i;
   for (i = 0; i < dim; i += bsize ) {
#ifdef PLAIN_DEPS
#pragma omp target device (smp) no_copy_deps \
            copy_out( (*A)[i;bsize][0;dim], \
                      (*B)[i;bsize][0;dim], \
                      (*C)[i;bsize][0;dim])
#pragma omp task firstprivate( bsize, dim, i )\
                 out( (*A)[i][0], \
                      (*B)[i][0], \
                      (*C)[i][0])
#else
#pragma omp target device (smp) copy_deps
#pragma omp task firstprivate( bsize, dim, i )\
                 out( (*A)[i;bsize][0;dim], \
                      (*B)[i;bsize][0;dim], \
                      (*C)[i;bsize][0;dim])
#endif
   {
      unsigned int j, ii, jj;
      unsigned int thisNode = -1; nanos_get_node_num( &thisNode );
      //printf("%d A=%p {%p - %p} B=%p {%p - %p} C=%p {%p - %p} bsize= %d dim=%d i=%d\n", thisNode, A, &((*A)[i][0]), &((*A)[i+bsize][0]), B, &((*B)[i][0]), &((*B)[i+bsize][0]), C, &((*C)[i][0]), &((*C)[i+bsize][0]), bsize, dim, i);
      for (ii = i; ii < i + bsize; ii++) {
        for (j = 0; j < dim; j++) {
#ifdef USE_1_1_0_INIT
           ((*A)[ii][j]) = 1;
           ((*B)[ii][j]) = 1;
#else
           ((*A)[ii][j]) = pow (1.0/(elem_t)(j+1), (elem_t)i);
           ((*B)[ii][j]) = (j)<=(ii);
#endif
           ((*C)[ii][j]) = 0;
        }
     }
   }
   }
}

void init_matrix_nomanualblocking( int colmajor, size_t dim, size_t bsize,
                                   elem_t (*A)[dim][dim],
                                   elem_t (*B)[dim][dim],
                                   elem_t (*C)[dim][dim])
{
   if ( colmajor ) {
      init_matrix_nomanualblocking_colmajor(dim, bsize, A, B, C);
   } else {
      init_matrix_nomanualblocking_rowmajor(dim, bsize, A, B, C);
   }
}

void init_matrix_kij_nomanualblocking( int colmajor, size_t dim, size_t bsize,
                                   elem_t (*A)[dim][dim],
                                   elem_t (*B)[dim][dim],
                                   elem_t (*C)[dim][dim])
{
   fprintf(stderr, "unimplemented\n");
   //if ( colmajor ) {
   //   init_matrix_nomanualblocking_colmajor(dim, bsize, A, B, C);
   //} else {
   //   init_matrix_nomanualblocking_rowmajor(dim, bsize, A, B, C);
   //}
}
