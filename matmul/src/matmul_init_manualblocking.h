#ifndef MATMUL_INIT_NOMANUALBLOCKING
#define MATMUL_INIT_NOMANUALBLOCKING
#include "matmul_defs.h"
void init_matrix_manualblocking(int colmajor, size_t nblocks, size_t bsize,
                                elem_t (*A)[nblocks][nblocks][bsize*bsize],
                                elem_t (*B)[nblocks][nblocks][bsize*bsize],
                                elem_t (*C)[nblocks][nblocks][bsize*bsize]);
#endif
