/*************************************************************************************/
/*      Copyright 2009 Barcelona Supercomputing Center                               */
/*                                                                                   */
/*      This file is part of the NANOS++ library.                                    */
/*                                                                                   */
/*      NANOS++ is free software: you can redistribute it and/or modify              */
/*      it under the terms of the GNU Lesser General Public License as published by  */
/*      the Free Software Foundation, either version 3 of the License, or            */
/*      (at your option) any later version.                                          */
/*                                                                                   */
/*      NANOS++ is distributed in the hope that it will be useful,                   */
/*      but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*      GNU Lesser General Public License for more details.                          */
/*                                                                                   */
/*      You should have received a copy of the GNU Lesser General Public License     */
/*      along with NANOS++.  If not, see <http://www.gnu.org/licenses/>.             */
/*************************************************************************************/
#include <sys/time.h>
#include <stdio.h>
#include "matmul_data_size.h"
#include "matmul_defs.h"
#include "matmul_kernel.h"
#include "matmul_init.h"
#include "utils.h"

struct timeval timev1, timev2;
double time_seconds;

//#define USE_1_1_0_INIT

const unsigned int bsize = BSIZE;
const unsigned int dim = DIM;

int main( int argc, char **argv )
{
   (void) argc;
   (void) argv;
   elem_t (*A)[dim][dim];
   elem_t (*B)[dim][dim];
   elem_t (*C)[dim][dim];
   unsigned int i;
   unsigned int k;

   allocate_data(dim, bsize, &A, &B, &C);
   init_matrix_kij( COLMAJOR, dim, bsize, A, B, C );
#pragma omp taskwait noflush
   fprintf(stderr, "Matmul started # block size: %d / dimension: %d\n", bsize, dim);

   gettimeofday(&timev1, NULL);

   for (k = 0; k < dim; k += bsize)
   {
      for (i = 0; i < dim; i += bsize)
      {
#ifdef PLAIN_DEPS
#pragma omp target device (smp) no_copy_deps \
            copy_in( (*A)[i;bsize][k;bsize],\
                     (*B)[k;bsize][0;dim] ) \
         copy_inout( (*C)[i;bsize][0;dim] )
#pragma omp task in( (*A)[i][k],\
                     (*B)[k][0] ) \
              inout( (*C)[i][0] ) \
         firstprivate( i, k, bsize, dim )
#else
#pragma omp target device (smp) copy_deps
#pragma omp task in( (*A)[i;bsize][k;bsize],\
                     (*B)[k;bsize][0;dim] ) \
              inout( (*C)[i;bsize][0;dim] ) \
         firstprivate( i, k, bsize, dim )
#endif
         {
            unsigned int j=0;
#ifdef USE_1_1_0_INIT
            unsigned int thisNode = -1; nanos_get_node_num( &thisNode );
            if ( (*A)[i][k] == 0 || (*B)[k][j] == 0 ) { printf("#####################################################################[%p : %d @ %d] do matmul block: A[%d][%d]{%p}=%f B[%d][%d]{%p}=%f C[%d][%d]{%p}=%f\n",nanos_current_wd(), nanos_get_wd_id(nanos_current_wd()), thisNode, i, k, &((*A)[i][k]), (*A)[i][k], k, j, &((*B)[k][j]), (*B)[k][j], i, j, &((*C)[i][j]), (*C)[i][j] ); }
#endif
            for (j = 0; j < dim; j += bsize)
            {
               matmul_kernel( i, j, k, bsize, dim, A, B, C );
            }
#pragma omp taskwait noflush
         }
      }
   }

#ifdef VALIDATE 
#pragma omp taskwait 
#else
#pragma omp taskwait noflush
#endif

   gettimeofday(&timev2, NULL);
   time_seconds = timev2.tv_sec - timev1.tv_sec;
   time_seconds += 0.000001 * (timev2.tv_usec - timev1.tv_usec);

   double gflops =  ( 2.0e-9 * dim * dim ) * dim / time_seconds;
   //write_matrix( (elem_t* (*)[NUM_BLOCKS])C);
#ifdef VALIDATE 
   int res = verify_nomanualblocking( COLMAJOR, dim, 1, C );
   if (res == 0) 
      fprintf(stderr,"Verification ok.\n");
   else
      fprintf(stderr,"Verification failed.\n");
   //print_summary( C );
#endif
   fprintf(stderr, "Execution time: %f seconds %f GFLOPS\n", time_seconds, gflops );
   return 0;
}
