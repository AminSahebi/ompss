#ifndef MATMUL_DEFS_H
#define MATMUL_DEFS_H

#include<stdio.h>

#define USE_DOUBLE

#ifdef USE_DOUBLE

typedef double elem_t;
#define _cublasgemm cublasDgemm_v2

#else

typedef float elem_t;
#define _cublasgemm cublasSgemm_v2

#endif

#endif
