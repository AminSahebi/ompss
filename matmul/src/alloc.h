
static int _alloc(void **ptr, size_t len) {
#ifdef PLAIN_DEPS
   void *tmp = NULL;
   tmp = malloc(len);
   if ( tmp != NULL ) {
      *ptr = tmp;
   }
   return (tmp == NULL) ? -1 : 0;
#else
   return nanos_memalign( ptr, len, NULL, 0 );
#endif
}

#ifdef MANUALBLOCKING
int allocate_data(size_t nblocks, size_t bsize, elem_t (**A)[nblocks][nblocks][bsize*bsize], elem_t (**B)[nblocks][nblocks][bsize*bsize], elem_t (**C)[nblocks][nblocks][bsize*bsize] ) {
   if ( _alloc( (void **) A, sizeof(elem_t)*nblocks*nblocks*bsize*bsize ) != 0 ) {
      fprintf(stderr, "C(void **) ould not allocate matrix A.\n");
      return -1;
   }
   if ( _alloc( (void **) B, sizeof(elem_t)*nblocks*nblocks*bsize*bsize ) != 0 ) {
      fprintf(stderr, "C(void **) ould not allocate matrix B.\n");
      return -1;
   }
   if ( _alloc( (void **) C, sizeof(elem_t)*nblocks*nblocks*bsize*bsize ) != 0 ) {
      fprintf(stderr, "Could not allocate matrix C.\n");
      return -1;
   }
   return 0;
}
#else
 #ifdef NOMANUALBLOCKING
int allocate_data(size_t dim, size_t bsize, elem_t (**A)[dim][dim], elem_t (**B)[dim][dim], elem_t (**C)[dim][dim] ) {
   (void) bsize;
   if ( _alloc( (void **) A, sizeof(elem_t)*dim*dim ) != 0 ) {
      fprintf(stderr, "C(void **) ould not allocate matrix A.\n");
      return -1;
   }
   if ( _alloc( (void **) B, sizeof(elem_t)*dim*dim ) != 0 ) {
      fprintf(stderr, "C(void **) ould not allocate matrix B.\n");
      return -1;
   }
   if ( _alloc( (void **) C, sizeof(elem_t)*dim*dim ) != 0 ) {
      fprintf(stderr, "Could not allocate matrix C.\n");
      return -1;
   }
   return 0;
}
 #endif
#endif
