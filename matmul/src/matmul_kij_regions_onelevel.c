/*************************************************************************************/
/*      Copyright 2009 Barcelona Supercomputing Center                               */
/*                                                                                   */
/*      This file is part of the NANOS++ library.                                    */
/*                                                                                   */
/*      NANOS++ is free software: you can redistribute it and/or modify              */
/*      it under the terms of the GNU Lesser General Public License as published by  */
/*      the Free Software Foundation, either version 3 of the License, or            */
/*      (at your option) any later version.                                          */
/*                                                                                   */
/*      NANOS++ is distributed in the hope that it will be useful,                   */
/*      but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*      GNU Lesser General Public License for more details.                          */
/*                                                                                   */
/*      You should have received a copy of the GNU Lesser General Public License     */
/*      along with NANOS++.  If not, see <http://www.gnu.org/licenses/>.             */
/*************************************************************************************/
#include <sys/time.h>
#include <stdio.h>
#include "matmul_data_size.h"
#include "matmul_defs.h"
#include "matmul_kernel.h"
#include "matmul_init.h"
#include "utils.h"

struct timeval timev1, timev2;
double time_seconds;

const unsigned int nblocks = NBLOCKS;
const unsigned int bsize = BSIZE;

int main ( int argc, char **argv )
{
   (void) argc;
   (void) argv;
   elem_t (*A)[NBLOCKS][NBLOCKS][BSIZE*BSIZE];
   elem_t (*B)[NBLOCKS][NBLOCKS][BSIZE*BSIZE];
   elem_t (*C)[NBLOCKS][NBLOCKS][BSIZE*BSIZE];
   unsigned int ii;
   unsigned int kk;

   if ( allocate_data( nblocks, bsize, &A, &B, &C) != 0 ) {
      return -1;
   }
   init_matrix_kij( COLMAJOR, nblocks, bsize, A, B, C );
#pragma omp taskwait noflush
   fprintf(stderr, "Matmul started # blocks: %d / block size: %d\n", nblocks, bsize);

   gettimeofday(&timev1, NULL);

   for (kk = 0; kk < nblocks; kk++)
   {
      for (ii = 0; ii < nblocks; ii++)
      {
         unsigned int jj;
         for (jj = 0; jj < nblocks; jj++)
         {
            matmul_kernel( ii, jj, kk, nblocks, bsize, A, B, C);
         }
      }
   }

#ifdef VALIDATE
#pragma omp taskwait
#else
#pragma omp taskwait noflush
#endif

   gettimeofday(&timev2, NULL);
   time_seconds = timev2.tv_sec - timev1.tv_sec;
   time_seconds += 0.000001 * (timev2.tv_usec - timev1.tv_usec);

   double gflops =  ( 2.0e-9 * nblocks*bsize * nblocks*bsize ) * nblocks*bsize / time_seconds;
#ifdef VALIDATE
   int res = verify_manualblocking( COLMAJOR, nblocks, bsize, 1, C );
   if (res == 0) 
      fprintf(stderr,"Verification ok.\n");
   else
      fprintf(stderr,"Verification failed.\n");
   print_summary_manualblocking( nblocks, bsize, C );
#endif
   fprintf(stderr, "Execution time: %f seconds %f GFLOPS\n", time_seconds, gflops );
   return 0;
}
