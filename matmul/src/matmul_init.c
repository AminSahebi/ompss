#ifdef MANUALBLOCKING
 #include "matmul_init_manualblocking.c"
#else
 #ifdef NOMANUALBLOCKING
  #include "matmul_init_nomanualblocking.c"
 #else
  #error "Must define MANUALBLOCKING or NOMANUALBLOCKING"
 #endif
#endif
