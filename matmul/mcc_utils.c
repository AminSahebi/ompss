extern int signgam;
enum mcc_enum_anon_13
{
  _IEEE_ =  -1,
  _SVID_ = 0,
  _XOPEN_ = 1,
  _POSIX_ = 2,
  _ISOC_ = 3
};
typedef enum mcc_enum_anon_13 _LIB_VERSION_TYPE;
extern _LIB_VERSION_TYPE _LIB_VERSION;
struct _IO_FILE_plus;
extern struct _IO_FILE_plus _IO_2_1_stdin_;
extern struct _IO_FILE_plus _IO_2_1_stdout_;
extern struct _IO_FILE_plus _IO_2_1_stderr_;
struct _IO_FILE;
extern struct _IO_FILE *stdin;
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;
extern int sys_nerr;
extern const char *const sys_errlist[];
typedef struct _IO_FILE _IO_FILE;
extern int _IO_getc(_IO_FILE *__fp);
extern struct _IO_FILE *stdin;
extern __inline __attribute__((__gnu_inline__)) int getchar(void)
{
  return _IO_getc(stdin);
}
typedef struct _IO_FILE FILE;
struct _IO_marker;
__extension__ typedef long int __off_t;
typedef void _IO_lock_t;
__extension__ typedef long long int __quad_t;
__extension__ typedef __quad_t __off64_t;
typedef unsigned int size_t;
struct  _IO_FILE
{
  int _flags;
  char *_IO_read_ptr;
  char *_IO_read_end;
  char *_IO_read_base;
  char *_IO_write_base;
  char *_IO_write_ptr;
  char *_IO_write_end;
  char *_IO_buf_base;
  char *_IO_buf_end;
  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;
  struct _IO_marker *_markers;
  struct _IO_FILE *_chain;
  int _fileno;
  int _flags2;
  __off_t _old_offset;
  unsigned short int _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1];
  _IO_lock_t *_lock;
  __off64_t _offset;
  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;
  size_t __pad5;
  int _mode;
  char _unused2[40];
};
extern int __uflow(_IO_FILE *);
extern __inline __attribute__((__gnu_inline__)) int fgetc_unlocked(FILE *__fp)
{
  return __builtin_expect((*__fp)._IO_read_ptr >= (*__fp)._IO_read_end, 0) ? __uflow(__fp) : *((unsigned char *)(*__fp)._IO_read_ptr++);
}
extern __inline __attribute__((__gnu_inline__)) int getc_unlocked(FILE *__fp)
{
  return __builtin_expect((*__fp)._IO_read_ptr >= (*__fp)._IO_read_end, 0) ? __uflow(__fp) : *((unsigned char *)(*__fp)._IO_read_ptr++);
}
extern __inline __attribute__((__gnu_inline__)) int getchar_unlocked(void)
{
  return __builtin_expect((*stdin)._IO_read_ptr >= (*stdin)._IO_read_end, 0) ? __uflow(stdin) : *((unsigned char *)(*stdin)._IO_read_ptr++);
}
extern int _IO_putc(int __c, _IO_FILE *__fp);
extern struct _IO_FILE *stdout;
extern __inline __attribute__((__gnu_inline__)) int putchar(int __c)
{
  return _IO_putc(__c, stdout);
}
extern int __overflow(_IO_FILE *, int);
extern __inline __attribute__((__gnu_inline__)) int fputc_unlocked(int __c, FILE *__stream)
{
  return __builtin_expect((*__stream)._IO_write_ptr >= (*__stream)._IO_write_end, 0) ? __overflow(__stream, (unsigned char)__c) : (unsigned char)(*(*__stream)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__gnu_inline__)) int putc_unlocked(int __c, FILE *__stream)
{
  return __builtin_expect((*__stream)._IO_write_ptr >= (*__stream)._IO_write_end, 0) ? __overflow(__stream, (unsigned char)__c) : (unsigned char)(*(*__stream)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__gnu_inline__)) int putchar_unlocked(int __c)
{
  return __builtin_expect((*stdout)._IO_write_ptr >= (*stdout)._IO_write_end, 0) ? __overflow(stdout, (unsigned char)__c) : (unsigned char)(*(*stdout)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__)) __attribute__((__gnu_inline__)) int feof_unlocked(FILE *__stream)
{
  return ((*__stream)._flags & 16) != 0;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__)) __attribute__((__gnu_inline__)) int ferror_unlocked(FILE *__stream)
{
  return ((*__stream)._flags & 32) != 0;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int sprintf(char *__restrict __s, const char *__restrict __fmt, ...)
{
  return __builtin___sprintf_chk(__s, 2 - 1, __builtin_object_size(__s, 2 > 1), __fmt, __builtin_va_arg_pack());
}
typedef __builtin_va_list __gnuc_va_list;
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int vsprintf(char *__restrict __s, const char *__restrict __fmt, __gnuc_va_list __ap)
{
  return __builtin___vsprintf_chk(__s, 2 - 1, __builtin_object_size(__s, 2 > 1), __fmt, __ap);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__format__(__printf__, 3, 4))) __attribute__((__leaf__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int snprintf(char *__restrict __s, size_t __n, const char *__restrict __fmt, ...)
{
  return __builtin___snprintf_chk(__s, __n, 2 - 1, __builtin_object_size(__s, 2 > 1), __fmt, __builtin_va_arg_pack());
}
extern __inline __attribute__((__nothrow__)) __attribute__((__format__(__printf__, 3, 0))) __attribute__((__leaf__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int vsnprintf(char *__restrict __s, size_t __n, const char *__restrict __fmt, __gnuc_va_list __ap)
{
  return __builtin___vsnprintf_chk(__s, __n, 2 - 1, __builtin_object_size(__s, 2 > 1), __fmt, __ap);
}
extern int __fprintf_chk(FILE *__restrict __stream, int __flag, const char *__restrict __format, ...);
extern __inline __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int fprintf(FILE *__restrict __stream, const char *__restrict __fmt, ...)
{
  return __fprintf_chk(__stream, 2 - 1, __fmt, __builtin_va_arg_pack());
}
extern int __printf_chk(int __flag, const char *__restrict __format, ...);
extern __inline __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int printf(const char *__restrict __fmt, ...)
{
  return __printf_chk(2 - 1, __fmt, __builtin_va_arg_pack());
}
extern int __vfprintf_chk(FILE *__restrict __stream, int __flag, const char *__restrict __format, __gnuc_va_list __ap);
extern __inline __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int vprintf(const char *__restrict __fmt, __gnuc_va_list __ap)
{
  return __vfprintf_chk(stdout, 2 - 1, __fmt, __ap);
}
extern __inline __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int vfprintf(FILE *__restrict __stream, const char *__restrict __fmt, __gnuc_va_list __ap)
{
  return __vfprintf_chk(__stream, 2 - 1, __fmt, __ap);
}
extern int __dprintf_chk(int __fd, int __flag, const char *__restrict __fmt, ...) __attribute__((__format__(__printf__, 3, 4)));
extern __inline __attribute__((__format__(__printf__, 2, 3))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int dprintf(int __fd, const char *__restrict __fmt, ...)
{
  return __dprintf_chk(__fd, 2 - 1, __fmt, __builtin_va_arg_pack());
}
extern int __vdprintf_chk(int __fd, int __flag, const char *__restrict __fmt, __gnuc_va_list __arg) __attribute__((__format__(__printf__, 3, 0)));
extern __inline __attribute__((__format__(__printf__, 2, 0))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int vdprintf(int __fd, const char *__restrict __fmt, __gnuc_va_list __ap)
{
  return __vdprintf_chk(__fd, 2 - 1, __fmt, __ap);
}
extern char *__gets_chk(char *__str, size_t) __attribute__((__warn_unused_result__));
extern char *__gets_warn(char *__str) __asm("""gets") __attribute__((__warn_unused_result__)) __attribute__((__warning__("please use fgets or getline instead, gets can't ""specify buffer size")));
extern __inline __attribute__((__warn_unused_result__)) __attribute__((__deprecated__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) char *gets(char *__str)
{
  if (__builtin_object_size(__str, 2 > 1) != (size_t) -1)
    {
      return __gets_chk(__str, __builtin_object_size(__str, 2 > 1));
    }
  return __gets_warn(__str);
}
extern char *__fgets_chk(char *__restrict __s, size_t __size, int __n, FILE *__restrict __stream) __attribute__((__warn_unused_result__));
extern char *__fgets_chk_warn(char *__restrict __s, size_t __size, int __n, FILE *__restrict __stream) __asm("""__fgets_chk") __attribute__((__warn_unused_result__)) __attribute__((__warning__("fgets called with bigger size than length ""of destination buffer")));
extern char *__fgets_alias(char *__restrict __s, int __n, FILE *__restrict __stream) __asm("""fgets") __attribute__((__warn_unused_result__));
extern __inline __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) char *fgets(char *__restrict __s, int __n, FILE *__restrict __stream)
{
  if (__builtin_object_size(__s, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__n) || __n <= 0)
        {
          return __fgets_chk(__s, __builtin_object_size(__s, 2 > 1), __n, __stream);
        }
      if ((size_t)__n > __builtin_object_size(__s, 2 > 1))
        {
          return __fgets_chk_warn(__s, __builtin_object_size(__s, 2 > 1), __n, __stream);
        }
    }
  return __fgets_alias(__s, __n, __stream);
}
extern size_t __fread_chk(void *__restrict __ptr, size_t __ptrlen, size_t __size, size_t __n, FILE *__restrict __stream) __attribute__((__warn_unused_result__));
extern size_t __fread_chk_warn(void *__restrict __ptr, size_t __ptrlen, size_t __size, size_t __n, FILE *__restrict __stream) __asm("""__fread_chk") __attribute__((__warn_unused_result__)) __attribute__((__warning__("fread called with bigger size * nmemb than length ""of destination buffer")));
extern size_t __fread_alias(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream) __asm("""fread") __attribute__((__warn_unused_result__));
extern __inline __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) size_t fread(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream)
{
  if (__builtin_object_size(__ptr, 0) != (size_t) -1)
    {
      if ((!__builtin_constant_p(__size) || !__builtin_constant_p(__n)) || (__size | __n) >= (size_t)1 << 8 * sizeof(size_t) / 2)
        {
          return __fread_chk(__ptr, __builtin_object_size(__ptr, 0), __size, __n, __stream);
        }
      if (__size * __n > __builtin_object_size(__ptr, 0))
        {
          return __fread_chk_warn(__ptr, __builtin_object_size(__ptr, 0), __size, __n, __stream);
        }
    }
  return __fread_alias(__ptr, __size, __n, __stream);
}
extern size_t __fread_unlocked_chk(void *__restrict __ptr, size_t __ptrlen, size_t __size, size_t __n, FILE *__restrict __stream) __attribute__((__warn_unused_result__));
extern size_t __fread_unlocked_chk_warn(void *__restrict __ptr, size_t __ptrlen, size_t __size, size_t __n, FILE *__restrict __stream) __asm("""__fread_unlocked_chk") __attribute__((__warn_unused_result__)) __attribute__((__warning__("fread_unlocked called with bigger size * nmemb than ""length of destination buffer")));
extern size_t __fread_unlocked_alias(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream) __asm("""fread_unlocked") __attribute__((__warn_unused_result__));
extern __inline __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) size_t fread_unlocked(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream)
{
  if (__builtin_object_size(__ptr, 0) != (size_t) -1)
    {
      if ((!__builtin_constant_p(__size) || !__builtin_constant_p(__n)) || (__size | __n) >= (size_t)1 << 8 * sizeof(size_t) / 2)
        {
          return __fread_unlocked_chk(__ptr, __builtin_object_size(__ptr, 0), __size, __n, __stream);
        }
      if (__size * __n > __builtin_object_size(__ptr, 0))
        {
          return __fread_unlocked_chk_warn(__ptr, __builtin_object_size(__ptr, 0), __size, __n, __stream);
        }
    }
  if (((__builtin_constant_p(__size) && __builtin_constant_p(__n)) && (__size | __n) < (size_t)1 << 8 * sizeof(size_t) / 2) && __size * __n <= 8)
    {
      size_t __cnt = __size * __n;
      char *__cptr = (char *)__ptr;
      if (__cnt == 0)
        {
          return 0;
        }
      for (; __cnt > 0;  --__cnt)
        {
          int __c = __builtin_expect((*__stream)._IO_read_ptr >= (*__stream)._IO_read_end, 0) ? __uflow(__stream) : *((unsigned char *)(*__stream)._IO_read_ptr++);
          if (__c ==  -1)
            {
              break;
            }
          *__cptr++ = __c;
        }
      return (__cptr - (char *)__ptr) / __size;
    }
  return __fread_unlocked_alias(__ptr, __size, __n, __stream);
}
typedef double elem_t;
extern struct _IO_FILE *stderr;
void print_all_manualblocking(size_t nblocks, size_t bsize, elem_t (*C)[nblocks][nblocks][bsize * bsize])
{
  unsigned int ii;
  unsigned int iii;
  unsigned int jj;
  unsigned int jjj;
  fprintf(stderr, "------\n");
  for (ii = 0; ii < nblocks; ii += 1)
    {
      for (iii = 0; iii < nblocks; iii += 1)
        {
          fprintf(stderr, " %d, %d\t", ii, iii);
          for (jj = 0; jj < bsize; jj += 1)
            {
              for (jjj = 0; jjj < bsize; jjj += 1)
                {
                  fprintf(stderr, " %2.9f", (*C)[ii][iii][jj * bsize + jjj]);
                }
              fprintf(stderr, "\n\t");
            }
          fprintf(stderr, "\n");
        }
    }
  fprintf(stderr, "------\n");
}
void print_summary_manualblocking(size_t nblocks, size_t bsize, elem_t (*C)[nblocks][nblocks][bsize * bsize])
{
  unsigned int ii;
  unsigned int iii;
  fprintf(stderr, "------\n");
  for (ii = 0; ii < nblocks; ii += 1)
    {
      for (iii = 0; iii < nblocks; iii += 1)
        {
          fprintf(stderr, " %4.0f", (*C)[ii][iii][0]);
        }
      fprintf(stderr, "\n");
    }
  fprintf(stderr, "------\n");
}
extern double pow(double __x, double __y) __attribute__((__nothrow__)) __attribute__((__leaf__));
static int verify_manualblocking_colmajor(size_t nblocks, size_t bsize, int niter, elem_t (*C)[nblocks][nblocks][bsize * bsize])
{
  unsigned int ii;
  unsigned int jj;
  unsigned int i;
  unsigned int j;
  int res = 0;
  for (ii = 0; ii < nblocks; ii++)
    {
      for (jj = 0; jj < nblocks; jj++)
        {
          for (i = 0; i < bsize; i++)
            {
              for (j = 0; j < bsize; j++)
                {
                  if (ii * bsize + j < 2)
                    {
                      continue;
                    }
                  elem_t i1 = 1.00000000000000000000000000000000000000000000000000000e+00 / ((elem_t)(ii * bsize) + j + 1);
                  elem_t shb = niter * (pow(i1, (elem_t)(jj * bsize + i + 1)) - 1) / (i1 - 1);
                  elem_t diff = ((*C)[ii][jj][i * bsize + j] - shb) / shb;
                  if (diff < 0)
                    {
                      diff =  -diff;
                    }
                  if (diff > 1.00000000000000004792173602385929598312941379845142365e-04)
                    {
                      res = 22;
                      fprintf(stderr, "error at %d, %d, %d, %d\n", ii, jj, i, j);
                      return res;
                    }
                }
            }
        }
    }
  return res;
}
static int verify_manualblocking_rowmajor(size_t nblocks, size_t bsize, int niter, elem_t (*C)[nblocks][nblocks][bsize * bsize])
{
  unsigned int ii;
  unsigned int jj;
  unsigned int i;
  unsigned int j;
  int res = 0;
  for (ii = 0; ii < nblocks; ii++)
    {
      for (jj = 0; jj < nblocks; jj++)
        {
          for (i = 0; i < bsize; i++)
            {
              for (j = 0; j < bsize; j++)
                {
                  if (ii * bsize + i < 2)
                    {
                      continue;
                    }
                  elem_t i1 = 1.00000000000000000000000000000000000000000000000000000e+00 / ((elem_t)(ii * bsize) + i + 1);
                  elem_t shb = niter * (pow(i1, (elem_t)(jj * bsize + j + 1)) - 1) / (i1 - 1);
                  elem_t diff = ((*C)[ii][jj][i * bsize + j] - shb) / shb;
                  if (diff < 0)
                    {
                      diff =  -diff;
                    }
                  if (diff > 1.00000000000000004792173602385929598312941379845142365e-04)
                    {
                      res = 22;
                      fprintf(stderr, "error at %d, %d, %d, %d\n", ii, jj, i, j);
                      return res;
                    }
                }
            }
        }
    }
  return res;
}
int verify_manualblocking(int colmajor, size_t nblocks, size_t bsize, int niter, elem_t (*C)[nblocks][nblocks][bsize * bsize])
{
  int result;
  if (colmajor)
    {
      result = verify_manualblocking_colmajor(nblocks, bsize, niter, C);
    }
  else
    {
      result = verify_manualblocking_rowmajor(nblocks, bsize, niter, C);
    }
  return result;
}
void print_addr_manualblocking(size_t nblocks, size_t bsize, elem_t (*A)[nblocks][nblocks][bsize * bsize])
{
  unsigned int i;
  unsigned int j;
  for (i = 0; i < nblocks; i += 1)
    {
      for (j = 0; j < nblocks; j += 1)
        {
          fprintf(stderr, "%p ", &(*A)[i][j][0]);
        }
      fprintf(stderr, "\n");
    }
  fprintf(stderr, "\n");
}
int verify_nomanualblocking(size_t dim, int niter, elem_t (*C)[dim][dim])
{
  unsigned int i;
  unsigned int j;
  int res = 0;
  (void)niter;
  for (i = 0; i < dim; i++)
    {
      for (j = 0; j < dim; j++)
        {
          if (i < 2)
            {
              continue;
            }
          elem_t i1 = 1.00000000000000000000000000000000000000000000000000000e+00 / ((elem_t)i + 1);
          elem_t shb = niter * (pow(i1, (elem_t)(j + 1)) - 1) / (i1 - 1);
          elem_t diff = ((*C)[i][j] - shb) / shb;
          if (diff < 0)
            {
              diff =  -diff;
            }
          if (diff > 1.00000000000000004792173602385929598312941379845142365e-04)
            {
              res = 22;
              fprintf(stderr, "error at %d, %d\n", i, j);
              return res;
            }
        }
    }
  return res;
}
void print_summary_nomanualblocking(size_t dim, size_t bsize, elem_t (*C)[dim][dim])
{
  unsigned int ii;
  unsigned int iii;
  fprintf(stderr, "------\n");
  for (ii = 0; ii < dim; ii += bsize)
    {
      for (iii = 0; iii < dim; iii += bsize)
        {
          fprintf(stderr, " %2.9f", (*C)[ii][iii]);
        }
      fprintf(stderr, "\n");
    }
  fprintf(stderr, "------\n");
}
void print_all_nomanualblocking(size_t dim, elem_t (*C)[dim][dim])
{
  unsigned int ii;
  unsigned int iii;
  fprintf(stderr, "------\n");
  for (ii = 0; ii < dim; ii += 1)
    {
      for (iii = 0; iii < dim; iii += 1)
        {
          fprintf(stderr, " %2.9f", (*C)[ii][iii]);
        }
      fprintf(stderr, "\n");
    }
  fprintf(stderr, "------\n");
}
void print_addresses_nomanualblocking(size_t dim, size_t bsize, elem_t (*A)[dim][dim])
{
  unsigned int ii;
  unsigned int jj;
  for (ii = 0; ii < dim; ii += bsize)
    {
      fprintf(stderr, "\t");
      for (jj = 0; jj < dim; jj += bsize)
        {
          fprintf(stderr, " %p", &(*A)[ii][jj]);
        }
      fprintf(stderr, "\n");
    }
}
