#!/bin/bash

base_nx_args="--deps regions --affinity-no-support --cluster-smp-presend 1020 --schedule affinity --affinity-no-steal"
nanoxrun=nanoxrun-mn3.sh

#for nodes in 1 2 4 8 16 32 ; do
#   for thds in 15 ; do
#      for totalmem_factor in 1 5/4 3/2 2 ; do
#         total_mem=$( printf "%.0f" $(echo "$totalmem_factor * 6 * 1024 * 1024 * 1024 / $nodes" | bc -q -l) )
#         echo $total_mem $nodes $( printf "%.0f" $(echo "$totalmem_factor * 6 * 1024 * 1024 * 1024 " | bc -q -l) )
#         $nanoxrun $base_nx_args --cluster-node-memory $total_mem --job-time 00:20 --queue --np $(($nodes + 1)) --affinity-no-master --threads $thds -- dgemm_kij_multilevel_nomanualblocking.perf
#         #$nanoxrun $base_nx_args --cluster-node-memory $total_mem --job-time 00:20 --queue --np $(($nodes + 1)) --affinity-no-master --threads $thds -- dgemm_kij_multilevel_nomanualblocking.verif
#         $nanoxrun $base_nx_args --cluster-node-memory $total_mem --job-time 00:20 --queue --np $(($nodes + 1)) --affinity-no-master --threads $thds -- dgemm_kij_onelevel_nomanualblocking.perf
#         #$nanoxrun $base_nx_args --cluster-node-memory $total_mem --job-time 00:20 --queue --np $(($nodes + 1)) --affinity-no-master --threads $thds -- dgemm_kij_onelevel_nomanualblocking.verif
#         #nanoxrun.sh $base_nx_args --job-time 00:20:00 --queue --np $nodes --threads $thds -- dgemm_multilevel_nomanualblocking
#         #nanoxrun.sh $base_nx_args --job-time 00:20:00 --queue --np $nodes --threads $thds -- dgemm_onelevel_nomanualblocking
#      done
#   done
#done

#for nodes in 1 2 4 8 16 32 ; do
for nodes in 4 ; do
   for thds in  1 ; do
      #$nanoxrun $base_nx_args --job-time 00:20 --queue --np $(($nodes + 1)) --affinity-no-master --threads $thds -- dgemm_kij_multilevel_nomanualblocking.perf
      $nanoxrun $base_nx_args --job-time 00:20 --queue --np $(($nodes + 1)) --affinity-no-master --threads $thds -- dgemm_kij_multilevel_nomanualblocking.verif
      #$nanoxrun $base_nx_args --cluster-alloc-wide --job-time 00:20 --queue --np $(($nodes + 1)) --affinity-no-master --threads $thds -- dgemm_multilevel_nomanualblocking.perf
      $nanoxrun $base_nx_args --job-time 00:20 --queue --np $(($nodes + 1)) --affinity-no-master --threads $thds -- dgemm_multilevel_nomanualblocking.verif
   done
done
