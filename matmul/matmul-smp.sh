echo "Executing matrix multiplication on 1 core..."
export NX_ARGS="--deps regions --smp-workers 1"
./matmul/dgemm_onelevel.perf

echo "Executing matrix multiplication on 4 cores..."
export NX_ARGS="--deps regions --smp-workers 4"
./matmul/dgemm_onelevel.perf
