#!/bin/bash

nanoxrun=/home/bsc15/bsc15105/usr/bin/nanoxrun.sh
waitjobs=/home/bsc15/bsc15105/usr/bin/waitjobs.sh

job_id_file=this-session-jobs.txt
base_nx_args="--job-time 00:20:00 --queue --deps regions --cluster-smp-presend 1020 --append-job-id $job_id_file --schedule affinity --affinity-no-steal --affinity-no-inval-aware --affinity-no-support --affinity-use-immediate-successor"

if [ -f "$job_id_file" ] ; then
   rm $job_id_file
fi

#for app in dgemm_kij_onelevel_nomanualblocking dgemm_kij_multilevel_nomanualblocking dgemm_onelevel_nomanualblocking dgemm_multilevel_nomanualblocking dgemm_onelevel dgemm_multilevel ; do
for app in dgemm_multilevel ; do
   for nodes in 1 2 4 8 16 32 ; do
      for cores in 1 2 4 8 ; do 
         $nanoxrun $base_nx_args --smp-workers $cores --np $nodes -- $app.verif
         for count in 1 2 3 4 ; do
            $nanoxrun $base_nx_args --smp-workers $cores --np $nodes -- $app.perf
         done # count
      done # cores

      #unlimited cores
      $nanoxrun $base_nx_args --np $nodes -- $app.verif
      for count in 1 2 3 4 ; do
         $nanoxrun $base_nx_args --np $nodes -- $app.perf
      done # count
   done # nodes
done # apps

$waitjobs $job_id_file

./print_results.sh

ok_verif_tests=$(grep "Verification ok" otpt.*.verif.*.err | wc -l)
verif_tests=$(ls otpt.*.verif.*.err | wc -l)

echo "Correct tests: " $ok_verif_tests " of " $verif_tests
