#!/bin/bash
#export NX_ARGS="--cluster --cluster-network=mpi --smp-workers 3 --disable-binding --cluster-smp-presend 1020 --deps regions --cluster-node-memory $((1024*1024*(256))) --schedule affinity --thd-output"
export NX_ARGS="--cluster --cluster-network=mpi --cluster-smp-presend=3" \
	NX_CLUSTER_NODE_MEMORY=268435456 \
	NX_GASNET_SEGMENT_SIZE=268435456 \

~/matmul/dgemm_onelevel.perf
