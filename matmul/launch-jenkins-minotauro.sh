#!/bin/bash

nanoxrun=/home/bsc15/bsc15105/usr/bin/nanoxrun.sh
waitjobs=/home/bsc15/bsc15105/usr/bin/waitjobs.sh

job_id_file=this-session-jobs.txt
base_nx_args="--deps regions --cluster-smp-presend 1020 --append-job-id $job_id_file --schedule affinity --affinity-no-steal --affinity-no-support"
#base_nx_args="--deps regions --cluster-smp-presend 1020 --append-job-id $job_id_file"

if [ -f "$job_id_file" ] ; then
   rm $job_id_file
fi

#for nodes in 1 2 4 8 16 32 ; do
# for nodes in 2 4 8 16 32 ; do
for nodes in 4 ; do
   # $nanoxrun $base_nx_args --job-time 00:20:00 --queue --np $nodes -- dgemm_kij_onelevel_nomanualblocking.verif
   # $nanoxrun $base_nx_args --job-time 00:20:00 --queue --np $nodes -- dgemm_kij_multilevel_nomanualblocking.verif
   # $nanoxrun $base_nx_args --job-time 00:20:00 --queue --np $nodes -- dgemm_onelevel_nomanualblocking.verif
   # $nanoxrun $base_nx_args --job-time 00:20:00 --queue --np $nodes -- dgemm_multilevel_nomanualblocking.verif
   #$nanoxrun $base_nx_args --job-time 00:20:00 --queue --np $nodes -- dgemm_onelevel.perf
   $nanoxrun $base_nx_args --job-time 00:20:00 --queue --np $nodes -- dgemm_multilevel.instr

   # for count in 1 2 3 4 ; do
   # $nanoxrun $base_nx_args --job-time 00:20:00 --queue --np $nodes -- dgemm_kij_onelevel_nomanualblocking.perf
   # $nanoxrun $base_nx_args --job-time 00:20:00 --queue --np $nodes -- dgemm_kij_multilevel_nomanualblocking.perf
   # $nanoxrun $base_nx_args --job-time 00:20:00 --queue --np $nodes -- dgemm_onelevel_nomanualblocking.perf
   # $nanoxrun $base_nx_args --job-time 00:20:00 --queue --np $nodes -- dgemm_multilevel_nomanualblocking.perf
   # $nanoxrun $base_nx_args --job-time 00:20:00 --queue --np $nodes -- dgemm_onelevel.perf
   # $nanoxrun $base_nx_args --job-time 00:20:00 --queue --np $nodes -- dgemm_multilevel.perf
   # done
done

$waitjobs $job_id_file

./print_results.sh

ok_verif_tests=$(grep "Verification ok" otpt.*.verif.*.err | wc -l)
verif_tests=$(ls otpt.*.verif.*.err | wc -l)

echo "Correct tests: " $ok_verif_tests " of " $verif_tests
