#!/bin/bash

#base_mem=2*96
#base_mem=3*96
#base_mem=5*48

function printResultsForFiles
{
   local _num_files=$1
   local _nodes=$2
   local _total_mem=$3
   local _files=$4
   local _cpus=$5
   
   local _etime=0;
   for a in ` grep "GFLOPS" $_files | cut -d " " -f 3` ; do 
      _etime=$(echo "$_etime + $a" | bc -l -q ) ; 
   done
   local _total_sinv=0
   local _total_hinv=0
   local _total_fail=0
   local _total_balance=0
   local _gflops=0
   local _gflops_array=""
   local _num_results=0
   for _item in ` grep "GFLOPS" $_files | cut -d " " -f 5` ; do 
      _num_results=$(($_num_results + 1));
      _gflops=$(echo "$_gflops + $_item" | bc -l -q ) ; 
      _gflops_array="$_item $_gflops_array"
   done
   if [ $_num_results -gt 0 ] ; then
      for _file in $_files ; do
         local _this_sinv=$(grep "Cluster Soft invalidations:" $_file | cut -d " " -f 6)
         if [ x$_this_sinv != x ] ; then
            _total_sinv=$(( $_total_sinv + $_this_sinv ))
         fi
      
         local _this_hinv=$(grep "Cluster Hard invalidations:" $_file | cut -d " " -f 6)
         if [ x$_this_hinv != x ] ; then
            _total_hinv=$(( $_total_hinv + $_this_hinv ))
         fi
      
         local _thisfail=$(grep "Failed" $_file | cut -d " " -f 8)
         if [ x$_thisfail != x ] ; then 
            _total_fail=$(( $_total_fail + $_thisfail ))
         fi
      
         local _this_balance=$(grep "Balance: " $_file | cut -d " " -f 5)
         if [ x$_this_balance != x ] ; then 
            _total_balance=$( echo "$_total_balance + $_this_balance" | bc -l -q )
         fi
      done
      _total_sinv=$(echo "$_total_sinv / $_num_results" | bc -l -q )
      _total_hinv=$(echo "$_total_hinv / $_num_results" | bc -l -q )
      _total_fail=$(echo "$_total_fail / $_num_results" | bc -l -q )
      _total_balance=$(echo "$_total_balance / $_num_results" | bc -l -q )
      local _gflops_avg=$(echo "$_gflops / $_num_results" | bc -q -l)
      local _etime_avg=$(echo "$_etime / $_num_results" | bc -q -l)
      local _gflops_variance=0;
      for _item in $_gflops_array ; do 
         _gflops_variance=$( echo " ( ($_item - $_gflops_avg) ^ 2 ) + $_gflops_variance" | bc -q -l)
      done
      local _gflops_stdev=$(echo "sqrt( $_gflops_variance / $_num_results )" | bc -q -l)
      local _sinv_avg=$(echo "$_total_sinv / $_nodes" | bc -q -l )
      local _hinv_avg=$(echo "$_total_hinv / $_nodes" | bc -q -l )
      echo "$_nodes $_cpus $_gflops_avg $_etime_avg $_total_sinv $_total_hinv $_total_mem $_num_results $_num_files $_gflops_stdev $_total_fail $_total_balance"
   else
      echo "# no results in $files "
   fi
}

#for app in dgemm_kij_onelevel_nomanualblocking.perf dgemm_kij_multilevel_nomanualblocking.perf ; do 
# for app in dgemm_kij_onelevel_nomanualblocking.perf ; do 
#    #for totalmem_factor in 1 5/4 3/2 2 ; do
#    for totalmem_factor in 1/2 2/3 3/4 1 ; do
#       echo "# $app $( printf "%.0f" $(echo "$totalmem_factor * 6 * 1024 * 1024 * 1024" | bc -q -l) ) Mb total mem"
#       for nodes in 1 2 4 8 16 32 ; do 
#          total_mem=$( printf "%.0f" $(echo "$totalmem_factor * 6 * 1024 * 1024 * 1024 / $nodes" | bc -q -l) )
#          real_nodes=$(printf "%04d" $((nodes + 1))) ; 
#          files=$( ls otpt.$app.$real_nodes*cluster-node-memory_$total_mem*err 2> /dev/null )
#          numfiles=$( ls otpt.$app.$real_nodes*cluster-node-memory_$total_mem*err 2> /dev/null | wc -l )
#          if [ $numfiles -gt 0 ] ; then
#              printResultsForFiles $numfiles $nodes $(echo "$totalmem_factor * 6 * 1024 * 1024 * 1024" | bc -q -l) "$files"
#          else
#             echo "#no files"
#          fi
#       done
#    done
# done

for app in dgemm_kij_onelevel_nomanualblocking.perf dgemm_kij_multilevel_nomanualblocking.perf dgemm_onelevel_nomanualblocking.perf dgemm_multilevel_nomanualblocking.perf dgemm_onelevel.perf dgemm_multilevel.perf ; do 
   echo "# $app full mem"
   for nodes in 1 2 4 8 16 32 ; do 
      for workers in 1 2 4 8 15 ; do 
         real_node=$(printf "%04d" $nodes) ; 
         if [ $workers = 0 ] ; then
            files=$( ls otpt.$app.$real_node.*err 2>/dev/null | grep -v cluster-node-memory | grep -v smp-workers )
            numfiles=$( ls otpt.$app.$real_node.*err 2>/dev/null | grep -v cluster-node-memory | grep -v smp-workers | wc -l )
         else
            files=$( ls otpt.$app.$real_node*smp-workers_${workers}_*err 2>/dev/null | grep -v cluster-node-memory )
            numfiles=$( ls otpt.$app.$real_node*smp-workers_${workers}_*err 2>/dev/null | grep -v cluster-node-memory | wc -l )
         fi

         if [ $numfiles -gt 0 ] ; then
            printResultsForFiles $numfiles $nodes inf "$files" $workers
         else
            echo "#no files"
         fi
      done  
   done  
done
