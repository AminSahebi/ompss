#!/bin/bash

nanox_opts="--smp-workers 1 --cluster-smp-presend 1020 --deps regions --cluster-node-memory $((1024*1024*(512+128))) --cluster-unaligned-node-memory --schedule affinity"

for nodes in 1 2 4 8 16 32 ; do
	nanoxrun.sh --queue --np $nodes $nanox_opts -- dgemm_multilevel.perf 
done
