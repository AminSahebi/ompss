# matmul-float

# edit size
#read
#echo "vi matmul-float/src/matmul_data_size.h"
#vi matmul-float/src/matmul_data_size.h
#echo "Done"

export PATH=$PATH:/home/amin/mercurium/bin/
# compile
#read
echo "(cd matmul/; make clean dgemm_onelevel.perf)"
#(cd matmul-float/; make clean dgemm_onelevel.perf)
(cd matmul/; make dgemm_onelevel.perf)
echo "Done"

# copy to udoo2
#read
#echo "./copy-matmul-float.sh"
#./copy-matmul-float.sh
#echo "Done"

# execute
#read
#echo "./matmul-float-cluster.sh"
#./matmul-float-cluster.sh
#echo "Done"
