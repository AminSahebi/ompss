#
sudo apt -y install build-essential git libssl-dev libncurses5-dev gawk bison wget
sudo apt -y install flex bc pkg-config libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev
sudo apt -y install linux-tools-generic linux-headers-generic autoconf automake gfortran libxml2-dev
sudo apt -y install libtool-bin libsqlite3-dev gperf debhelper fakeroot fakechroot qemu-user-static
sudo apt -y install Python2.7 automake autoconf libtool

#echo "installing GASNet-1.32.0"

cd GASNet-1.32.0
./configure --prefix=/usr
make
sudo make install

#
#echo "installing OpenMPI"
#

cd openmpi-1.10.2
./configure --prefix="/home/$USER/.openmpi"
make
sudo make install
export PATH="$PATH:/home/$USER/.openmpi/bin"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/home/$USER/.openmpi/lib/"


#
#echo "installing nanox"
# 
cd ompss/ompss-19.06/nanox-0.15
mkdir ~/nanox
export NANOX=/home/$USER/nanox
./configure --prefix=$NANOX
make
sudo make install


#
#echo "installing mcxx"
#
cd ompss/ompss-19.06/mcxx-2.3.0
mkdir ~/mercurium
export MERCURIUM=/home/$USER/mercurium
./configure --prefix=$MERCURIUM --enable-ompss --with-nanox=/home/$USER/nanox
make
sudo make install


#echo "Done"

