**OmpSs installation and Matrix Multiplication experiment**
---
## Platform
* Processor: Intel, and AMD x86_64
* OS: Ubuntu 18.04, 16.04 and 14.04 running the experiment successfully with the following procedures,
## Preparation
The following items are necessary to install and run an experiment with OmpSs programming model 
by Barcelona SuperComputing Center [here](https://www.bsc.es/).

Please keep the order of the installation procedure: 

1. openmpi-1.10.7 (openmpi ubuntu package would be sufficient)
2. GASNet-1.32.0
3. ompss-19.06.tar.gz
    * nanox-0.15 
    * mcxx-2.3.0

You should have them when clone this repository or please download the latest packages 
from Barcelona Super Computing website. https://pm.bsc.es/

## Clone the repository
All the softwares listed above can be found here:   
```
git clone https://AminSahebi@bitbucket.org/AminSahebi/ompss.git
```

## Install the packages
Next, make sure these packages are installled successfully:

* sudo apt -y install build-essential git libssl-dev libncurses5-dev gawk bison wget
* sudo apt -y install flex bc pkg-config libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev
* sudo apt -y install linux-tools-generic linux-headers-generic autoconf automake gfortran libxml2-dev
* sudo apt -y install libtool-bin libsqlite3-dev gperf debhelper fakeroot fakechroot qemu-user-static
* sudo apt -y install Python2.7 automake autoconf libtool

---
NOTE: at some machines, after this step is better to logout or restart the machine, Then continue.
##  
## Install softwares one by one  
Installing the cloned packages one by one:
##  
NOTE: Assumed that the cloned the repository has been placed in the $HOME directory. 

1- Installing OpenMPI packages

```
cd ~/ompss
tar -xvf openmpi-1.10.7.tar.gz -C .
cd openmpi-1.10.7

./configure 
make
make install
```
or simply
```
sudo apt install openmpi-bin openmpi-common libopenmpi-dev
```

2- Installing GASNet
```
cd ~/ompss
tar -xvf GASNet-1.32.0.tar.gz -C .
cd GASNet-1.32.0

./configure --prefix=/home/$USER/gasnet-install --disable-aligned-segments --disable-pshm --disable-seq --disable-parsync --with-mpi-cc="mpicc -fPIC -DPIC" --with-mpi-cxx="mpicxx -fPIC -DPIC" CC="gcc -fPIC -DPIC" CFLAGS="-fPIC -DPIC" CXX="g++ -fPIC -DPIC" CXXFLAGS=" -fPIC -DPIC" CPPFLAGS="-DPIC" LDFLAGS="-fPIC" --enable-smp --enable-udp --enable-mpi --disable-ibv

The output would be sth like below: (check if it satisfies your requirements)
    
     SNet configuration: 
 
     Portable conduits:
     -----------------
      portable UDP network conduit (udp)                 ON
      portable MPI-1.1/2.x network conduit (mpi)         ON
      portable SMP-loopback network conduit (smp)        ON
 
     Native, high-performance conduits:
     ---------------------------------
      Mellanox MXM conduit (mxm)                         OFF
      Intel PSM conduit (psm)                            OFF
      Portals4 network conduit (portals4)                OFF
      OpenIB/OpenFabrics IB Verbs network conduit (ibv)  OFF
      Gemini network conduit (gemini)                    OFF
      Aries network conduit (aries)                      OFF
      Shmem network conduit (shmem)                      OFF
      IBM PAMI network conduit (pami)                    OFF
      Libfabric network conduit (ofi)                    OFF

     Some conduits require --enable-XXX configure flags and/or additional
     variables providing the install location of vendor drivers.
     See the GASNet documentation for details.

     Misc Settings
     -------------
      MPI compatibility:      yes
      Pthreads support:       yes
      Segment config:         fast
      PSHM support:           no
      FCA support:            no


make -j4
make install
```

3- Installing nanox
```
cd ~/ompss
tar -xvf ompss-19.06.tar.gz -C .
cd ompss-19.06/nanox-0.15

./configure --prefix=/home/$USER/nanox-install --disable-debug --disable-instrumentation --with-gasnet=/home/$USER/gasnet-install --with-mpi-include=/usr/include/mpi --with-mpi-lib=/usr/lib MPICXX=mpicxx

Output would be sth like below: (check if it satisfies your requirements)
 
 Configuration summary of Nanos++
    ================================

    Host arch:                x86_64
    User level threading:     yes
    Configured architectures:  smp cluster
    Configured versions:      performance instrumentation 
    Extra options:            
    GCC atomics:              legacy gcc __sync builtins
    Memory tracker:           
    Memory allocator:         
    Task resiliency:          
    Cluster/GASNet conduits:   udp mpi


make -j4
make install
```
4- Installing Mercurium Compiler

IMPORTANT NOTE: Installing MCXX on x86 and aarch64 platforms has different repository with same procedure
if you want to install on x86 machine use ompss-19.06/mcxx-2.3.0-aarch64 directory! and add two flags to distinguish the target platform:
cd ~/ompss
cd ompss-19.06/mcxx-2.3.0-aarch64

./configure --prefix=/home/$USER/mcxx-install --enable-ompss --enable-tl-openmp-nanox --with-nanox=/home/$USER/nanox-install --target=aarch64-linux-gnu target_alias=aarch64-linux-gnu

```
cd ~/ompss
cd ompss-19.06/mcxx-2.3.0

./configure --prefix=/home/$USER/mcxx-install --enable-ompss --enable-tl-openmp-nanox --with-nanox=/home/$USER/nanox-install

Pay attention the output of the configuration must shows the addresses of the installed nanos properly:

 * C++ dialect used to build Mercurium : C++2011

 * Tools configured:

   Flex : flex
   GNU bison : bison
   GNU gperf : (not found)
   git content tracker: (not found)
   SQLite 3 cflags: -I/usr/local/include
   SQLite 3 libs: -L/usr/local/lib -lsqlite3

 * Mercurium backend compilers:

   GNU C compiler: gcc
   GNU C++ compiler: g++
   GNU Fortran compiler: gfortran
   nVidia CUDA compiler : (not found)
   Intel C compiler: (not found)
   Intel C++ compiler: (not found)
   Intel Fortran compiler: (not found)
   IBM XL C compiler : (not found)
   IBM XL C++ compiler : (not found)
   IBM XL Fortran compiler : (not found)
   Cray C compiler : (not found)
   Cray C++ compiler : (not found)
   Cray Fortran compiler : (not found)

 * Default type environment :  linux-x86_64

   Host supports __float128 and quadmath.h :  yes
   Host supports __int128 :  yes
   SIMD Support:  SSE4.2 (SVML not enabled)
   Intel Xeon Phi compilation support:  no
   Intel Xeon Phi remote device used for testing:  no

   Fortran tests enabled:  yes

 * TL Examples will be built : no

 * Nanos++ OmpSs / OpenMP : yes

   Nanos++ includes : /home/sahebi/nanox-install/include/nanox
   Nanos++ libraries: /home/sahebi/nanox-install/lib
   CUDA support: yes
   OpenCL support: no
   MPI Offload support: no

 * Nanos6 OmpSs-2 : no

 * Intel OpenMP RTL : no

 * GNU GOMP : no

 * OpenMP static profile mode:  no

 * Optional phases :

   Analysis phase enabled: yes
   Vectorization phase enabled: no

make
make install
```

## Put the OmpSs directories in your PATH
After a successfull installation you have to modify the PATH in your machine and 
address properly the ompss directories in the PATH as follows:

    export PATH=/home/$USER/nanox-install/bin:/home/$USER/gasnet-install/bin:/home/$USER/mcxx-install/bin:$PATH
    and 
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/$USER/nanox-install/lib/performance
    for performance libraries, you can add other paths like "instrumentaion" etc. in your PATH if needed.

it's recommended to update your .bashrc or bash_profile and keep these paths there:

    echo 'export PATH=/home/$USER/nanox-install/bin:/home/$USER/gasnet-install/bin:/home/$USER/mcxx-install/bin:$PATH' >> ~/.bashrc

## Common known probelms
NOTE:

    Please DO NOT use any parallelization like make -j4 during the installation 
    for MCXX compiler, in some cases we recieved unknown errors just just skip the parallelization.
    It won't take that much, roughly 10 minutes. 
 
    Please be aware that MATLAB compiler is also called MCC, 
    so mercurium compiler also is named MCC. So these two softwares can be
    on the PATH of your system and will make conflict.

    Sometimes during the installation of the Mercurium compiler, 
    system can not find the gfortran compiler, even though it is installed, 
    to solve this issue, just 
    sudo apt remove gfortran, log out, then sudo apt install it again. It will work.

    If there was a unkown error during "make" on mercurrium compiler, just reconfigure with 
    "autoreconf -fiv", then configure again with the flags provided above. 
    if didn't help either, try following: 
    autoheader \
    && aclocal \
    && libtoolize --ltdl --copy --force \
    && automake --add-missing --copy \
    && autoconf 
    then ./configure ...
    
    If using the cluster configuration consisted of multiple nodes: 
    There is a bug for managing and scheduling the resources while using MPI + GASNet,
    the description of the bug is that MPI can not retrieve the resources provided properly, 
    Nodes, Slots, Sockets and etc, you should:
    
    * make sure you put the proper specification of the cores for each node in the hostfile,
    e.g,. 192.168.5.73 slots=4 max-slots=4
    
    * make sure you use this command to run the application with the MPI, 
    mpirun --bind-to none --map-by ppr:1:node -x NX_ARGS
    
    * Pass the proper GasNet variables by "export" them properly, for example: 
    export NX_CLUSTER_NODE_MEMORY=1073741824
    export NX_GASNET_SEGMENT_SIZE=1073741824
    
    If some packages face ubuntu knowe error such as :
    
    E: Unable to locate package linux-tools-4.15.0-58-generic
    E: Couldn't find any package by glob 'linux-tools-4.15.0-58-generic'
    E: Couldn't find any package by regex 'linux-tools-4.15.0-58-generic'

    add the multiverse and universe repositories may help.
   
    sudo add-apt-repository universe
    sudo add-apt-repository multiverse
    sudo apt update

for upgrading GCC compiler:
```
sudo apt-get install -y software-properties-common
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt update
sudo apt install g++-7 -y
Set it up so the symbolic links gcc, g++ point to the newer version:

sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 60 \
                         --slave /usr/bin/g++ g++ /usr/bin/g++-7 
sudo update-alternatives --config gcc
gcc --version
g++ --version
```