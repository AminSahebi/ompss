struct _IO_FILE_plus;
extern struct _IO_FILE_plus _IO_2_1_stdin_;
extern struct _IO_FILE_plus _IO_2_1_stdout_;
extern struct _IO_FILE_plus _IO_2_1_stderr_;
struct _IO_FILE;
extern struct _IO_FILE *stdin;
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;
extern int sys_nerr;
extern const char *const sys_errlist[];
typedef struct _IO_FILE _IO_FILE;
extern int _IO_getc(_IO_FILE *__fp);
extern struct _IO_FILE *stdin;
extern __inline __attribute__((__gnu_inline__)) int getchar(void)
{
  return _IO_getc(stdin);
}
typedef struct _IO_FILE FILE;
struct _IO_marker;
typedef long int __off_t;
typedef void _IO_lock_t;
typedef long int __off64_t;
typedef unsigned long int size_t;
struct  _IO_FILE
{
  int _flags;
  char *_IO_read_ptr;
  char *_IO_read_end;
  char *_IO_read_base;
  char *_IO_write_base;
  char *_IO_write_ptr;
  char *_IO_write_end;
  char *_IO_buf_base;
  char *_IO_buf_end;
  char *_IO_save_base;
  char *_IO_backup_base;
  char *_IO_save_end;
  struct _IO_marker *_markers;
  struct _IO_FILE *_chain;
  int _fileno;
  int _flags2;
  __off_t _old_offset;
  unsigned short int _cur_column;
  signed char _vtable_offset;
  char _shortbuf[1L];
  _IO_lock_t *_lock;
  __off64_t _offset;
  void *__pad1;
  void *__pad2;
  void *__pad3;
  void *__pad4;
  size_t __pad5;
  int _mode;
  char _unused2[20L];
};
extern int __uflow(_IO_FILE *);
extern __inline __attribute__((__gnu_inline__)) int fgetc_unlocked(FILE *__fp)
{
  return __builtin_expect((*__fp)._IO_read_ptr >= (*__fp)._IO_read_end, 0) ? __uflow(__fp) : *((unsigned char *)(*__fp)._IO_read_ptr++);
}
extern __inline __attribute__((__gnu_inline__)) int getc_unlocked(FILE *__fp)
{
  return __builtin_expect((*__fp)._IO_read_ptr >= (*__fp)._IO_read_end, 0) ? __uflow(__fp) : *((unsigned char *)(*__fp)._IO_read_ptr++);
}
extern __inline __attribute__((__gnu_inline__)) int getchar_unlocked(void)
{
  return __builtin_expect((*stdin)._IO_read_ptr >= (*stdin)._IO_read_end, 0) ? __uflow(stdin) : *((unsigned char *)(*stdin)._IO_read_ptr++);
}
extern int _IO_putc(int __c, _IO_FILE *__fp);
extern struct _IO_FILE *stdout;
extern __inline __attribute__((__gnu_inline__)) int putchar(int __c)
{
  return _IO_putc(__c, stdout);
}
extern int __overflow(_IO_FILE *, int);
extern __inline __attribute__((__gnu_inline__)) int fputc_unlocked(int __c, FILE *__stream)
{
  return __builtin_expect((*__stream)._IO_write_ptr >= (*__stream)._IO_write_end, 0) ? __overflow(__stream, (unsigned char)__c) : (unsigned char)(*(*__stream)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__gnu_inline__)) int putc_unlocked(int __c, FILE *__stream)
{
  return __builtin_expect((*__stream)._IO_write_ptr >= (*__stream)._IO_write_end, 0) ? __overflow(__stream, (unsigned char)__c) : (unsigned char)(*(*__stream)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__gnu_inline__)) int putchar_unlocked(int __c)
{
  return __builtin_expect((*stdout)._IO_write_ptr >= (*stdout)._IO_write_end, 0) ? __overflow(stdout, (unsigned char)__c) : (unsigned char)(*(*stdout)._IO_write_ptr++ = __c);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__)) __attribute__((__gnu_inline__)) int feof_unlocked(FILE *__stream)
{
  return ((*__stream)._flags & 16) != 0;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__)) __attribute__((__gnu_inline__)) int ferror_unlocked(FILE *__stream)
{
  return ((*__stream)._flags & 32) != 0;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int sprintf(char *__restrict __s, const char *__restrict __fmt, ...)
{
  return __builtin___sprintf_chk(__s, 2 - 1, __builtin_object_size(__s, 2 > 1), __fmt, __builtin_va_arg_pack());
}
typedef __builtin_va_list __gnuc_va_list;
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int vsprintf(char *__restrict __s, const char *__restrict __fmt, __gnuc_va_list __ap)
{
  return __builtin___vsprintf_chk(__s, 2 - 1, __builtin_object_size(__s, 2 > 1), __fmt, __ap);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__format__(__printf__, 3, 4))) __attribute__((__leaf__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int snprintf(char *__restrict __s, size_t __n, const char *__restrict __fmt, ...)
{
  return __builtin___snprintf_chk(__s, __n, 2 - 1, __builtin_object_size(__s, 2 > 1), __fmt, __builtin_va_arg_pack());
}
extern __inline __attribute__((__nothrow__)) __attribute__((__format__(__printf__, 3, 0))) __attribute__((__leaf__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int vsnprintf(char *__restrict __s, size_t __n, const char *__restrict __fmt, __gnuc_va_list __ap)
{
  return __builtin___vsnprintf_chk(__s, __n, 2 - 1, __builtin_object_size(__s, 2 > 1), __fmt, __ap);
}
extern int __fprintf_chk(FILE *__restrict __stream, int __flag, const char *__restrict __format, ...);
extern __inline __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int fprintf(FILE *__restrict __stream, const char *__restrict __fmt, ...)
{
  return __fprintf_chk(__stream, 2 - 1, __fmt, __builtin_va_arg_pack());
}
extern int __printf_chk(int __flag, const char *__restrict __format, ...);
extern __inline __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int printf(const char *__restrict __fmt, ...)
{
  return __printf_chk(2 - 1, __fmt, __builtin_va_arg_pack());
}
extern int __vfprintf_chk(FILE *__restrict __stream, int __flag, const char *__restrict __format, __gnuc_va_list __ap);
extern __inline __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int vprintf(const char *__restrict __fmt, __gnuc_va_list __ap)
{
  return __vfprintf_chk(stdout, 2 - 1, __fmt, __ap);
}
extern __inline __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int vfprintf(FILE *__restrict __stream, const char *__restrict __fmt, __gnuc_va_list __ap)
{
  return __vfprintf_chk(__stream, 2 - 1, __fmt, __ap);
}
extern int __dprintf_chk(int __fd, int __flag, const char *__restrict __fmt, ...) __attribute__((__format__(__printf__, 3, 4)));
extern __inline __attribute__((__format__(__printf__, 2, 3))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int dprintf(int __fd, const char *__restrict __fmt, ...)
{
  return __dprintf_chk(__fd, 2 - 1, __fmt, __builtin_va_arg_pack());
}
extern int __vdprintf_chk(int __fd, int __flag, const char *__restrict __fmt, __gnuc_va_list __arg) __attribute__((__format__(__printf__, 3, 0)));
extern __inline __attribute__((__format__(__printf__, 2, 0))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int vdprintf(int __fd, const char *__restrict __fmt, __gnuc_va_list __ap)
{
  return __vdprintf_chk(__fd, 2 - 1, __fmt, __ap);
}
extern char *__gets_chk(char *__str, size_t) __attribute__((__warn_unused_result__));
extern char *__gets_warn(char *__str) __asm("""gets") __attribute__((__warn_unused_result__)) __attribute__((__warning__("please use fgets or getline instead, gets can't ""specify buffer size")));
extern __inline __attribute__((__warn_unused_result__)) __attribute__((__deprecated__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) char *gets(char *__str)
{
  if (__builtin_object_size(__str, 2 > 1) != (size_t) -1)
    {
      return __gets_chk(__str, __builtin_object_size(__str, 2 > 1));
    }
  return __gets_warn(__str);
}
extern char *__fgets_chk(char *__restrict __s, size_t __size, int __n, FILE *__restrict __stream) __attribute__((__warn_unused_result__));
extern char *__fgets_chk_warn(char *__restrict __s, size_t __size, int __n, FILE *__restrict __stream) __asm("""__fgets_chk") __attribute__((__warn_unused_result__)) __attribute__((__warning__("fgets called with bigger size than length ""of destination buffer")));
extern char *__fgets_alias(char *__restrict __s, int __n, FILE *__restrict __stream) __asm("""fgets") __attribute__((__warn_unused_result__));
extern __inline __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) char *fgets(char *__restrict __s, int __n, FILE *__restrict __stream)
{
  if (__builtin_object_size(__s, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__n) || __n <= 0)
        {
          return __fgets_chk(__s, __builtin_object_size(__s, 2 > 1), __n, __stream);
        }
      if ((size_t)__n > __builtin_object_size(__s, 2 > 1))
        {
          return __fgets_chk_warn(__s, __builtin_object_size(__s, 2 > 1), __n, __stream);
        }
    }
  return __fgets_alias(__s, __n, __stream);
}
extern size_t __fread_chk(void *__restrict __ptr, size_t __ptrlen, size_t __size, size_t __n, FILE *__restrict __stream) __attribute__((__warn_unused_result__));
extern size_t __fread_chk_warn(void *__restrict __ptr, size_t __ptrlen, size_t __size, size_t __n, FILE *__restrict __stream) __asm("""__fread_chk") __attribute__((__warn_unused_result__)) __attribute__((__warning__("fread called with bigger size * nmemb than length ""of destination buffer")));
extern size_t __fread_alias(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream) __asm("""fread") __attribute__((__warn_unused_result__));
extern __inline __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) size_t fread(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream)
{
  if (__builtin_object_size(__ptr, 0) != (size_t) -1)
    {
      if ((!__builtin_constant_p(__size) || !__builtin_constant_p(__n)) || (__size | __n) >= (size_t)1 << 8 * sizeof(size_t) / 2)
        {
          return __fread_chk(__ptr, __builtin_object_size(__ptr, 0), __size, __n, __stream);
        }
      if (__size * __n > __builtin_object_size(__ptr, 0))
        {
          return __fread_chk_warn(__ptr, __builtin_object_size(__ptr, 0), __size, __n, __stream);
        }
    }
  return __fread_alias(__ptr, __size, __n, __stream);
}
extern size_t __fread_unlocked_chk(void *__restrict __ptr, size_t __ptrlen, size_t __size, size_t __n, FILE *__restrict __stream) __attribute__((__warn_unused_result__));
extern size_t __fread_unlocked_chk_warn(void *__restrict __ptr, size_t __ptrlen, size_t __size, size_t __n, FILE *__restrict __stream) __asm("""__fread_unlocked_chk") __attribute__((__warn_unused_result__)) __attribute__((__warning__("fread_unlocked called with bigger size * nmemb than ""length of destination buffer")));
extern size_t __fread_unlocked_alias(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream) __asm("""fread_unlocked") __attribute__((__warn_unused_result__));
extern __inline __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) size_t fread_unlocked(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream)
{
  if (__builtin_object_size(__ptr, 0) != (size_t) -1)
    {
      if ((!__builtin_constant_p(__size) || !__builtin_constant_p(__n)) || (__size | __n) >= (size_t)1 << 8 * sizeof(size_t) / 2)
        {
          return __fread_unlocked_chk(__ptr, __builtin_object_size(__ptr, 0), __size, __n, __stream);
        }
      if (__size * __n > __builtin_object_size(__ptr, 0))
        {
          return __fread_unlocked_chk_warn(__ptr, __builtin_object_size(__ptr, 0), __size, __n, __stream);
        }
    }
  if (((__builtin_constant_p(__size) && __builtin_constant_p(__n)) && (__size | __n) < (size_t)1 << 8 * sizeof(size_t) / 2) && __size * __n <= 8)
    {
      size_t __cnt = __size * __n;
      char *__cptr = (char *)__ptr;
      if (__cnt == 0)
        {
          return 0;
        }
      for (; __cnt > 0;  --__cnt)
        {
          int __c = __builtin_expect((*__stream)._IO_read_ptr >= (*__stream)._IO_read_end, 0) ? __uflow(__stream) : *((unsigned char *)(*__stream)._IO_read_ptr++);
          if (__c ==  -1)
            {
              break;
            }
          *__cptr++ = __c;
        }
      return (__cptr - (char *)__ptr) / __size;
    }
  return __fread_unlocked_alias(__ptr, __size, __n, __stream);
}
static __inline unsigned int __bswap_32(unsigned int __bsx)
{
  return __builtin_bswap32(__bsx);
}
typedef unsigned long int __uint64_t;
static __inline __uint64_t __bswap_64(__uint64_t __bsx)
{
  return __builtin_bswap64(__bsx);
}
extern long int strtol(const char *__restrict __nptr, char **__restrict __endptr, int __base) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__warn_unused_result__)) __attribute__((__gnu_inline__)) int atoi(const char *__nptr)
{
  return (int)strtol(__nptr, (char **)(void *)0, 10);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__warn_unused_result__)) __attribute__((__gnu_inline__)) long int atol(const char *__nptr)
{
  return strtol(__nptr, (char **)(void *)0, 10);
}
extern long long int strtoll(const char *__restrict __nptr, char **__restrict __endptr, int __base) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
__extension__ extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__warn_unused_result__)) __attribute__((__gnu_inline__)) long long int atoll(const char *__nptr)
{
  return strtoll(__nptr, (char **)(void *)0, 10);
}
__extension__ extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) unsigned int gnu_dev_major(unsigned long long int __dev)
{
  return (__dev >> 8 & 4095) | ((unsigned int)(__dev >> 32) & ~4095);
}
__extension__ extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) unsigned int gnu_dev_minor(unsigned long long int __dev)
{
  return (__dev & 255) | ((unsigned int)(__dev >> 12) & ~255);
}
__extension__ extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__const__)) __attribute__((__gnu_inline__)) unsigned long long int gnu_dev_makedev(unsigned int __major, unsigned int __minor)
{
  return (((__minor & 255) | (__major & 4095) << 8) | (unsigned long long int)(__minor & ~255) << 12) | (unsigned long long int)(__major & ~4095) << 32;
}
typedef int (*__compar_fn_t)(const void *, const void *);
extern __inline __attribute__((__nonnull__(1, 2, 5))) __attribute__((__warn_unused_result__)) __attribute__((__gnu_inline__)) void *bsearch(const void *__key, const void *__base, size_t __nmemb, size_t __size, __compar_fn_t __compar)
{
  size_t __l;
  size_t __u;
  size_t __idx;
  const void *__p;
  int __comparison;
  __l = 0;
  __u = __nmemb;
  while (__l < __u)
    {
      __idx = (__l + __u) / 2;
      __p = (void *)((const char *)__base + __idx * __size);
      __comparison = (*__compar)(__key, __p);
      if (__comparison < 0)
        {
          __u = __idx;
        }
      else
        {
          if (__comparison > 0)
            {
              __l = __idx + 1;
            }
          else
            {
              return (void *)__p;
            }
        }
    }
  return (void *)0;
}
extern double strtod(const char *__restrict __nptr, char **__restrict __endptr) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1))) __attribute__((__warn_unused_result__)) __attribute__((__gnu_inline__)) double atof(const char *__nptr)
{
  return strtod(__nptr, (char **)(void *)0);
}
extern char *__realpath_chk(const char *__restrict __name, char *__restrict __resolved, size_t __resolvedlen) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__));
extern char *__realpath_alias(const char *__restrict __name, char *__restrict __resolved) __asm("""realpath") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) char *__attribute__((__leaf__)) realpath(const char *__restrict __name, char *__restrict __resolved)
{
  if (__builtin_object_size(__resolved, 2 > 1) != (size_t) -1)
    {
      return __realpath_chk(__name, __resolved, __builtin_object_size(__resolved, 2 > 1));
    }
  return __realpath_alias(__name, __resolved);
}
extern int __ptsname_r_chk(int __fd, char *__buf, size_t __buflen, size_t __nreal) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(2)));
extern int __ptsname_r_chk_warn(int __fd, char *__buf, size_t __buflen, size_t __nreal) __asm("""__ptsname_r_chk") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(2))) __attribute__((__warning__("ptsname_r called with buflen bigger than ""size of buf")));
extern int __ptsname_r_alias(int __fd, char *__buf, size_t __buflen) __asm("""ptsname_r") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(2)));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int ptsname_r(int __fd, char *__buf, size_t __buflen)
{
  if (__builtin_object_size(__buf, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__buflen))
        {
          return __ptsname_r_chk(__fd, __buf, __buflen, __builtin_object_size(__buf, 2 > 1));
        }
      if (__buflen > __builtin_object_size(__buf, 2 > 1))
        {
          return __ptsname_r_chk_warn(__fd, __buf, __buflen, __builtin_object_size(__buf, 2 > 1));
        }
    }
  return __ptsname_r_alias(__fd, __buf, __buflen);
}
typedef int wchar_t;
extern int __wctomb_chk(char *__s, wchar_t __wchar, size_t __buflen) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__));
extern int __wctomb_alias(char *__s, wchar_t __wchar) __asm("""wctomb") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) __attribute__((__warn_unused_result__)) int wctomb(char *__s, wchar_t __wchar)
{
  if (__builtin_object_size(__s, 2 > 1) != (size_t) -1 && 16 > __builtin_object_size(__s, 2 > 1))
    {
      return __wctomb_chk(__s, __wchar, __builtin_object_size(__s, 2 > 1));
    }
  return __wctomb_alias(__s, __wchar);
}
extern size_t __mbstowcs_chk(wchar_t *__restrict __dst, const char *__restrict __src, size_t __len, size_t __dstlen) __attribute__((__nothrow__)) __attribute__((__leaf__));
extern size_t __mbstowcs_chk_warn(wchar_t *__restrict __dst, const char *__restrict __src, size_t __len, size_t __dstlen) __asm("""__mbstowcs_chk") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warning__("mbstowcs called with dst buffer smaller than len ""* sizeof (wchar_t)")));
extern size_t __mbstowcs_alias(wchar_t *__restrict __dst, const char *__restrict __src, size_t __len) __asm("""mbstowcs") __attribute__((__nothrow__)) __attribute__((__leaf__));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) size_t mbstowcs(wchar_t *__restrict __dst, const char *__restrict __src, size_t __len)
{
  if (__builtin_object_size(__dst, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__len))
        {
          return __mbstowcs_chk(__dst, __src, __len, __builtin_object_size(__dst, 2 > 1) / sizeof(wchar_t));
        }
      if (__len > __builtin_object_size(__dst, 2 > 1) / sizeof(wchar_t))
        {
          return __mbstowcs_chk_warn(__dst, __src, __len, __builtin_object_size(__dst, 2 > 1) / sizeof(wchar_t));
        }
    }
  return __mbstowcs_alias(__dst, __src, __len);
}
extern size_t __wcstombs_chk(char *__restrict __dst, const wchar_t *__restrict __src, size_t __len, size_t __dstlen) __attribute__((__nothrow__)) __attribute__((__leaf__));
extern size_t __wcstombs_chk_warn(char *__restrict __dst, const wchar_t *__restrict __src, size_t __len, size_t __dstlen) __asm("""__wcstombs_chk") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warning__("wcstombs called with dst buffer smaller than len")));
extern size_t __wcstombs_alias(char *__restrict __dst, const wchar_t *__restrict __src, size_t __len) __asm("""wcstombs") __attribute__((__nothrow__)) __attribute__((__leaf__));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) size_t wcstombs(char *__restrict __dst, const wchar_t *__restrict __src, size_t __len)
{
  if (__builtin_object_size(__dst, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__len))
        {
          return __wcstombs_chk(__dst, __src, __len, __builtin_object_size(__dst, 2 > 1));
        }
      if (__len > __builtin_object_size(__dst, 2 > 1))
        {
          return __wcstombs_chk_warn(__dst, __src, __len, __builtin_object_size(__dst, 2 > 1));
        }
    }
  return __wcstombs_alias(__dst, __src, __len);
}
extern char **__environ;
extern char *optarg;
extern int optind;
extern int opterr;
extern int optopt;
typedef long int __ssize_t;
typedef __ssize_t ssize_t;
extern ssize_t __read_chk(int __fd, void *__buf, size_t __nbytes, size_t __buflen) __attribute__((__warn_unused_result__));
extern ssize_t __read_chk_warn(int __fd, void *__buf, size_t __nbytes, size_t __buflen) __asm("""__read_chk") __attribute__((__warn_unused_result__)) __attribute__((__warning__("read called with bigger length than size of ""the destination buffer")));
extern ssize_t __read_alias(int __fd, void *__buf, size_t __nbytes) __asm("""read") __attribute__((__warn_unused_result__));
extern __inline __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) ssize_t read(int __fd, void *__buf, size_t __nbytes)
{
  if (__builtin_object_size(__buf, 0) != (size_t) -1)
    {
      if (!__builtin_constant_p(__nbytes))
        {
          return __read_chk(__fd, __buf, __nbytes, __builtin_object_size(__buf, 0));
        }
      if (__nbytes > __builtin_object_size(__buf, 0))
        {
          return __read_chk_warn(__fd, __buf, __nbytes, __builtin_object_size(__buf, 0));
        }
    }
  return __read_alias(__fd, __buf, __nbytes);
}
extern ssize_t __readlink_chk(const char *__restrict __path, char *__restrict __buf, size_t __len, size_t __buflen) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2))) __attribute__((__warn_unused_result__));
extern ssize_t __readlink_chk_warn(const char *__restrict __path, char *__restrict __buf, size_t __len, size_t __buflen) __asm("""__readlink_chk") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2))) __attribute__((__warn_unused_result__)) __attribute__((__warning__("readlink called with bigger length ""than size of destination buffer")));
extern ssize_t __readlink_alias(const char *__restrict __path, char *__restrict __buf, size_t __len) __asm("""readlink") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2))) __attribute__((__warn_unused_result__));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2))) __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) ssize_t readlink(const char *__restrict __path, char *__restrict __buf, size_t __len)
{
  if (__builtin_object_size(__buf, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__len))
        {
          return __readlink_chk(__path, __buf, __len, __builtin_object_size(__buf, 2 > 1));
        }
      if (__len > __builtin_object_size(__buf, 2 > 1))
        {
          return __readlink_chk_warn(__path, __buf, __len, __builtin_object_size(__buf, 2 > 1));
        }
    }
  return __readlink_alias(__path, __buf, __len);
}
extern ssize_t __readlinkat_chk(int __fd, const char *__restrict __path, char *__restrict __buf, size_t __len, size_t __buflen) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(2, 3))) __attribute__((__warn_unused_result__));
extern ssize_t __readlinkat_chk_warn(int __fd, const char *__restrict __path, char *__restrict __buf, size_t __len, size_t __buflen) __asm("""__readlinkat_chk") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(2, 3))) __attribute__((__warn_unused_result__)) __attribute__((__warning__("readlinkat called with bigger ""length than size of destination ""buffer")));
extern ssize_t __readlinkat_alias(int __fd, const char *__restrict __path, char *__restrict __buf, size_t __len) __asm("""readlinkat") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(2, 3))) __attribute__((__warn_unused_result__));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(2, 3))) __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) ssize_t readlinkat(int __fd, const char *__restrict __path, char *__restrict __buf, size_t __len)
{
  if (__builtin_object_size(__buf, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__len))
        {
          return __readlinkat_chk(__fd, __path, __buf, __len, __builtin_object_size(__buf, 2 > 1));
        }
      if (__len > __builtin_object_size(__buf, 2 > 1))
        {
          return __readlinkat_chk_warn(__fd, __path, __buf, __len, __builtin_object_size(__buf, 2 > 1));
        }
    }
  return __readlinkat_alias(__fd, __path, __buf, __len);
}
extern char *__getcwd_chk(char *__buf, size_t __size, size_t __buflen) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__));
extern char *__getcwd_chk_warn(char *__buf, size_t __size, size_t __buflen) __asm("""__getcwd_chk") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__)) __attribute__((__warning__("getcwd caller with bigger length than size of ""destination buffer")));
extern char *__getcwd_alias(char *__buf, size_t __size) __asm("""getcwd") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) char *__attribute__((__leaf__)) getcwd(char *__buf, size_t __size)
{
  if (__builtin_object_size(__buf, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__size))
        {
          return __getcwd_chk(__buf, __size, __builtin_object_size(__buf, 2 > 1));
        }
      if (__size > __builtin_object_size(__buf, 2 > 1))
        {
          return __getcwd_chk_warn(__buf, __size, __builtin_object_size(__buf, 2 > 1));
        }
    }
  return __getcwd_alias(__buf, __size);
}
extern char *__getwd_chk(char *__buf, size_t buflen) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1))) __attribute__((__warn_unused_result__));
extern char *__getwd_warn(char *__buf) __asm("""getwd") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1))) __attribute__((__warn_unused_result__)) __attribute__((__warning__("please use getcwd instead, as getwd ""doesn't specify buffer size")));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1))) __attribute__((__deprecated__)) __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) char *__attribute__((__leaf__)) getwd(char *__buf)
{
  if (__builtin_object_size(__buf, 2 > 1) != (size_t) -1)
    {
      return __getwd_chk(__buf, __builtin_object_size(__buf, 2 > 1));
    }
  return __getwd_warn(__buf);
}
extern size_t __confstr_chk(int __name, char *__buf, size_t __len, size_t __buflen) __attribute__((__nothrow__)) __attribute__((__leaf__));
extern size_t __confstr_chk_warn(int __name, char *__buf, size_t __len, size_t __buflen) __asm("""__confstr_chk") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warning__("confstr called with bigger length than size of destination ""buffer")));
extern size_t __confstr_alias(int __name, char *__buf, size_t __len) __asm("""confstr") __attribute__((__nothrow__)) __attribute__((__leaf__));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) size_t confstr(int __name, char *__buf, size_t __len)
{
  if (__builtin_object_size(__buf, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__len))
        {
          return __confstr_chk(__name, __buf, __len, __builtin_object_size(__buf, 2 > 1));
        }
      if (__builtin_object_size(__buf, 2 > 1) < __len)
        {
          return __confstr_chk_warn(__name, __buf, __len, __builtin_object_size(__buf, 2 > 1));
        }
    }
  return __confstr_alias(__name, __buf, __len);
}
typedef unsigned int __gid_t;
extern int __getgroups_chk(int __size, __gid_t __list[], size_t __listlen) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__));
extern int __getgroups_chk_warn(int __size, __gid_t __list[], size_t __listlen) __asm("""__getgroups_chk") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__)) __attribute__((__warning__("getgroups called with bigger group count than what ""can fit into destination buffer")));
extern int __getgroups_alias(int __size, __gid_t __list[]) __asm("""getgroups") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int getgroups(int __size, __gid_t *__list)
{
  if (__builtin_object_size(__list, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__size) || __size < 0)
        {
          return __getgroups_chk(__size, __list, __builtin_object_size(__list, 2 > 1));
        }
      if (__size * sizeof(__gid_t) > __builtin_object_size(__list, 2 > 1))
        {
          return __getgroups_chk_warn(__size, __list, __builtin_object_size(__list, 2 > 1));
        }
    }
  return __getgroups_alias(__size, __list);
}
extern int __ttyname_r_chk(int __fd, char *__buf, size_t __buflen, size_t __nreal) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(2)));
extern int __ttyname_r_chk_warn(int __fd, char *__buf, size_t __buflen, size_t __nreal) __asm("""__ttyname_r_chk") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(2))) __attribute__((__warning__("ttyname_r called with bigger buflen than ""size of destination buffer")));
extern int __ttyname_r_alias(int __fd, char *__buf, size_t __buflen) __asm("""ttyname_r") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(2)));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(2))) __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int ttyname_r(int __fd, char *__buf, size_t __buflen)
{
  if (__builtin_object_size(__buf, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__buflen))
        {
          return __ttyname_r_chk(__fd, __buf, __buflen, __builtin_object_size(__buf, 2 > 1));
        }
      if (__buflen > __builtin_object_size(__buf, 2 > 1))
        {
          return __ttyname_r_chk_warn(__fd, __buf, __buflen, __builtin_object_size(__buf, 2 > 1));
        }
    }
  return __ttyname_r_alias(__fd, __buf, __buflen);
}
extern int __getlogin_r_chk(char *__buf, size_t __buflen, size_t __nreal) __attribute__((__nonnull__(1)));
extern int __getlogin_r_chk_warn(char *__buf, size_t __buflen, size_t __nreal) __asm("""__getlogin_r_chk") __attribute__((__nonnull__(1))) __attribute__((__warning__("getlogin_r called with bigger buflen than ""size of destination buffer")));
extern int __getlogin_r_alias(char *__buf, size_t __buflen) __asm("""getlogin_r") __attribute__((__nonnull__(1)));
extern __inline __attribute__((__nonnull__(1))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int getlogin_r(char *__buf, size_t __buflen)
{
  if (__builtin_object_size(__buf, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__buflen))
        {
          return __getlogin_r_chk(__buf, __buflen, __builtin_object_size(__buf, 2 > 1));
        }
      if (__buflen > __builtin_object_size(__buf, 2 > 1))
        {
          return __getlogin_r_chk_warn(__buf, __buflen, __builtin_object_size(__buf, 2 > 1));
        }
    }
  return __getlogin_r_alias(__buf, __buflen);
}
extern int __gethostname_chk(char *__buf, size_t __buflen, size_t __nreal) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
extern int __gethostname_chk_warn(char *__buf, size_t __buflen, size_t __nreal) __asm("""__gethostname_chk") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1))) __attribute__((__warning__("gethostname called with bigger buflen than ""size of destination buffer")));
extern int __gethostname_alias(char *__buf, size_t __buflen) __asm("""gethostname") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int gethostname(char *__buf, size_t __buflen)
{
  if (__builtin_object_size(__buf, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__buflen))
        {
          return __gethostname_chk(__buf, __buflen, __builtin_object_size(__buf, 2 > 1));
        }
      if (__buflen > __builtin_object_size(__buf, 2 > 1))
        {
          return __gethostname_chk_warn(__buf, __buflen, __builtin_object_size(__buf, 2 > 1));
        }
    }
  return __gethostname_alias(__buf, __buflen);
}
extern int __getdomainname_chk(char *__buf, size_t __buflen, size_t __nreal) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1))) __attribute__((__warn_unused_result__));
extern int __getdomainname_chk_warn(char *__buf, size_t __buflen, size_t __nreal) __asm("""__getdomainname_chk") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1))) __attribute__((__warn_unused_result__)) __attribute__((__warning__("getdomainname called with bigger ""buflen than size of destination ""buffer")));
extern int __getdomainname_alias(char *__buf, size_t __buflen) __asm("""getdomainname") __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1))) __attribute__((__warn_unused_result__));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1))) __attribute__((__warn_unused_result__)) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) int getdomainname(char *__buf, size_t __buflen)
{
  if (__builtin_object_size(__buf, 2 > 1) != (size_t) -1)
    {
      if (!__builtin_constant_p(__buflen))
        {
          return __getdomainname_chk(__buf, __buflen, __builtin_object_size(__buf, 2 > 1));
        }
      if (__buflen > __builtin_object_size(__buf, 2 > 1))
        {
          return __getdomainname_chk_warn(__buf, __buflen, __builtin_object_size(__buf, 2 > 1));
        }
    }
  return __getdomainname_alias(__buf, __buflen);
}
extern __inline __attribute__((__gnu_inline__)) size_t __strcspn_c1(const char *__s, int __reject)
{
  size_t __result = 0;
  while (__s[__result] != '\000' && __s[__result] != __reject)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strcspn_c2(const char *__s, int __reject1, int __reject2)
{
  size_t __result = 0;
  while ((__s[__result] != '\000' && __s[__result] != __reject1) && __s[__result] != __reject2)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strcspn_c3(const char *__s, int __reject1, int __reject2, int __reject3)
{
  size_t __result = 0;
  while (((__s[__result] != '\000' && __s[__result] != __reject1) && __s[__result] != __reject2) && __s[__result] != __reject3)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strspn_c1(const char *__s, int __accept)
{
  size_t __result = 0;
  while (__s[__result] == __accept)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strspn_c2(const char *__s, int __accept1, int __accept2)
{
  size_t __result = 0;
  while (__s[__result] == __accept1 || __s[__result] == __accept2)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) size_t __strspn_c3(const char *__s, int __accept1, int __accept2, int __accept3)
{
  size_t __result = 0;
  while ((__s[__result] == __accept1 || __s[__result] == __accept2) || __s[__result] == __accept3)
    {
       ++__result;
    }
  return __result;
}
extern __inline __attribute__((__gnu_inline__)) char *__strpbrk_c2(const char *__s, int __accept1, int __accept2)
{
  while ((*__s != '\000' && *__s != __accept1) && *__s != __accept2)
    {
       ++__s;
    }
  return *__s == '\000' ? (void *)0 : (char *)(size_t)__s;
}
extern __inline __attribute__((__gnu_inline__)) char *__strpbrk_c3(const char *__s, int __accept1, int __accept2, int __accept3)
{
  while (((*__s != '\000' && *__s != __accept1) && *__s != __accept2) && *__s != __accept3)
    {
       ++__s;
    }
  return *__s == '\000' ? (void *)0 : (char *)(size_t)__s;
}
extern __inline __attribute__((__gnu_inline__)) char *__strtok_r_1c(char *__s, char __sep, char **__nextp)
{
  char *__result;
  if (__s == (void *)0)
    {
      __s = *__nextp;
    }
  while (*__s == __sep)
    {
       ++__s;
    }
  __result = (void *)0;
  if (*__s != '\000')
    {
      __result = __s++;
      while (*__s != '\000')
        {
          if (*__s++ == __sep)
            {
              __s[ -1] = '\000';
              break;
            }
        }
    }
  *__nextp = __s;
  return __result;
}
extern void *__rawmemchr(const void *__s, int __c);
extern __inline __attribute__((__gnu_inline__)) char *__strsep_1c(char **__s, char __reject)
{
  char *__retval = *__s;
  if (__retval != (void *)0 && (*__s = (__builtin_constant_p(__reject) && !__builtin_constant_p(__retval)) && __reject == '\000' ? (char *)__rawmemchr(__retval, __reject) : __builtin_strchr(__retval, __reject)) != (void *)0)
    {
      *(*__s)++ = '\000';
    }
  return __retval;
}
extern __inline __attribute__((__gnu_inline__)) char *__strsep_2c(char **__s, char __reject1, char __reject2)
{
  char *__retval = *__s;
  if (__retval != (void *)0)
    {
      char *__cp = __retval;
      while (1)
        {
          if (*__cp == '\000')
            {
              __cp = (void *)0;
              break;
            }
          if (*__cp == __reject1 || *__cp == __reject2)
            {
              *__cp++ = '\000';
              break;
            }
           ++__cp;
        }
      *__s = __cp;
    }
  return __retval;
}
extern __inline __attribute__((__gnu_inline__)) char *__strsep_3c(char **__s, char __reject1, char __reject2, char __reject3)
{
  char *__retval = *__s;
  if (__retval != (void *)0)
    {
      char *__cp = __retval;
      while (1)
        {
          if (*__cp == '\000')
            {
              __cp = (void *)0;
              break;
            }
          if ((*__cp == __reject1 || *__cp == __reject2) || *__cp == __reject3)
            {
              *__cp++ = '\000';
              break;
            }
           ++__cp;
        }
      *__s = __cp;
    }
  return __retval;
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) void *__attribute__((__leaf__)) memcpy(void *__restrict __dest, const void *__restrict __src, size_t __len)
{
  return __builtin___memcpy_chk(__dest, __src, __len, __builtin_object_size(__dest, 0));
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) void *__attribute__((__leaf__)) memmove(void *__dest, const void *__src, size_t __len)
{
  return __builtin___memmove_chk(__dest, __src, __len, __builtin_object_size(__dest, 0));
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) void *__attribute__((__leaf__)) memset(void *__dest, int __ch, size_t __len)
{
  return __builtin___memset_chk(__dest, __ch, __len, __builtin_object_size(__dest, 0));
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) void bcopy(const void *__src, void *__dest, size_t __len)
{
  (void)__builtin___memmove_chk(__dest, __src, __len, __builtin_object_size(__dest, 0));
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) void bzero(void *__dest, size_t __len)
{
  (void)__builtin___memset_chk(__dest, '\000', __len, __builtin_object_size(__dest, 0));
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) char *__attribute__((__leaf__)) strcpy(char *__restrict __dest, const char *__restrict __src)
{
  return __builtin___strcpy_chk(__dest, __src, __builtin_object_size(__dest, 2 > 1));
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) char *__attribute__((__leaf__)) strncpy(char *__restrict __dest, const char *__restrict __src, size_t __len)
{
  return __builtin___strncpy_chk(__dest, __src, __len, __builtin_object_size(__dest, 2 > 1));
}
extern char *__stpncpy_chk(char *__dest, const char *__src, size_t __n, size_t __destlen) __attribute__((__nothrow__)) __attribute__((__leaf__));
extern char *__stpncpy_alias(char *__dest, const char *__src, size_t __n) __asm("""stpncpy") __attribute__((__nothrow__)) __attribute__((__leaf__));
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) char *__attribute__((__leaf__)) stpncpy(char *__dest, const char *__src, size_t __n)
{
  if (__builtin_object_size(__dest, 2 > 1) != (size_t) -1 && (!__builtin_constant_p(__n) || __n > __builtin_object_size(__dest, 2 > 1)))
    {
      return __stpncpy_chk(__dest, __src, __n, __builtin_object_size(__dest, 2 > 1));
    }
  return __stpncpy_alias(__dest, __src, __n);
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) char *__attribute__((__leaf__)) strcat(char *__restrict __dest, const char *__restrict __src)
{
  return __builtin___strcat_chk(__dest, __src, __builtin_object_size(__dest, 2 > 1));
}
extern __inline __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1, 2))) __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__artificial__)) char *__attribute__((__leaf__)) strncat(char *__restrict __dest, const char *__restrict __src, size_t __len)
{
  return __builtin___strncat_chk(__dest, __src, __len, __builtin_object_size(__dest, 2 > 1));
}
extern char *__tzname[2L];
extern int __daylight;
extern long int __timezone;
extern char *tzname[2L];
extern int daylight;
extern long int timezone;
struct timespec;
typedef long int __time_t;
typedef long int __syscall_slong_t;
struct  timespec
{
  __time_t tv_sec;
  __syscall_slong_t tv_nsec;
};
static __inline long int time_diff_sec(struct timespec *new, struct timespec *old)
{
  return (*new).tv_sec - (*old).tv_sec + ((*new).tv_nsec - (*old).tv_nsec) / 1000000000;
}
static __inline long int time_diff_msec(struct timespec *new, struct timespec *old)
{
  return ((*new).tv_sec - (*old).tv_sec) * 1000 + ((*new).tv_nsec - (*old).tv_nsec) / 1000000;
}
static __inline long int time_sec(struct timespec *t)
{
  return (*t).tv_sec + (*t).tv_nsec / 1000000000;
}
static __inline long int time_msec(struct timespec *t)
{
  return (*t).tv_sec * 1000 + (*t).tv_nsec / 1000000;
}
typedef int __clockid_t;
typedef __clockid_t clockid_t;
extern int clock_gettime(clockid_t __clock_id, struct timespec *__tp) __attribute__((__nothrow__)) __attribute__((__leaf__));
static __inline void time_gettime(struct timespec *t)
{
  clock_gettime(5, t);
}
static int BLOCKSIZE = 25;
static int N = 300;
typedef unsigned long int uint64_t;
typedef uint64_t elem_t;
void seqinit(int size, int n, elem_t *A, elem_t *B)
{
  int i;
  int j;
  for (i = 0; i < size; i++)
    {
      for (j = 0; j < n; j++)
        {
          A[i * n + j] = j;
          B[i * n + j] = j;
        }
    }
}
typedef __time_t time_t;
extern struct _IO_FILE *stderr;
void matmul(int size, int n, elem_t *A, elem_t *B, elem_t *C, long int *elapsed)
{
  struct timespec time_start;
  int i;
  int j;
  elem_t tmp;
  elem_t k;
  struct timespec time_end;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Start matmul %p %p %p\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), A, B, C);
  }
  ;
  time_gettime(&time_start);
  for (i = 0; i < size; i++)
    {
      for (j = 0; j < n; j++)
        {
          tmp = 0;
          for (k = 0; k < n; k++)
            {
              tmp += A[i * n + k] * B[k * n + j];
            }
          C[i * n + j] = tmp;
        }
    }
  time_gettime(&time_end);
  *elapsed = time_diff_msec(&time_end, &time_start);
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: End matmul   %p %p %p elapse=%ld\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), A, B, C, *elapsed);
  }
  ;
}
static elem_t *a;
static elem_t *b;
static elem_t *c;
static long int *matmul_time;
struct  nanos_args_0_t
{
  int size;
  int n;
  elem_t *A;
  elem_t *B;
  elem_t *C;
  long int *elapsed;
};
typedef void *nanos_wd_t;
enum mcc_enum_anon_5
{
  NANOS_OK = 0,
  NANOS_UNKNOWN_ERR = 1,
  NANOS_UNIMPLEMENTED = 2,
  NANOS_ENOMEM = 3,
  NANOS_INVALID_PARAM = 4,
  NANOS_INVALID_REQUEST = 5
};
typedef enum mcc_enum_anon_5 nanos_err_t;
typedef unsigned int nanos_copy_id_t;
extern nanos_err_t nanos_get_addr(nanos_copy_id_t copy_id, void **addr, nanos_wd_t cwd);
extern void nanos_handle_error(nanos_err_t err);
static void nanos_xlate_fun_ompssevimmc_0(struct nanos_args_0_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).A = (elem_t *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(1, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).B = (elem_t *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(2, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).C = (elem_t *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(3, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).elapsed = (long int *)device_base_address;
  }
}
struct  nanos_args_1_t
{
  int size;
  int n;
  elem_t *A;
  elem_t *B;
  elem_t *C;
  long int *elapsed;
};
static void nanos_xlate_fun_ompssevimmc_1(struct nanos_args_1_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).A = (elem_t *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(1, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).B = (elem_t *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(2, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).C = (elem_t *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(3, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).elapsed = (long int *)device_base_address;
  }
}
extern void ompss_nanox_main_begin(void *addr, const char *filename, int line);
extern void exit(int __status) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__noreturn__));
extern void *malloc(size_t __size) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__malloc__)) __attribute__((__warn_unused_result__));
extern nanos_err_t nanos_in_final(_Bool *result);
struct  mcc_struct_anon_15
{
  void (*outline)(void *);
};
typedef struct mcc_struct_anon_15 nanos_smp_args_t;
static void smp_ol_matmul_1(struct nanos_args_0_t *const args);
struct  mcc_struct_anon_11
{
  _Bool mandatory_creation:1;
  _Bool tied:1;
  _Bool clear_chunk:1;
  _Bool reserved0:1;
  _Bool reserved1:1;
  _Bool reserved2:1;
  _Bool reserved3:1;
  _Bool reserved4:1;
};
typedef struct mcc_struct_anon_11 nanos_wd_props_t;
struct  nanos_const_wd_definition_tag
{
  nanos_wd_props_t props;
  size_t data_alignment;
  size_t num_copies;
  size_t num_devices;
  size_t num_dimensions;
  const char *description;
};
typedef struct nanos_const_wd_definition_tag nanos_const_wd_definition_t;
struct  mcc_struct_anon_14
{
  void *(*factory)(void *);
  void *arg;
};
typedef struct mcc_struct_anon_14 nanos_device_t;
struct  nanos_const_wd_definition_1
{
  nanos_const_wd_definition_t base;
  nanos_device_t devices[1L];
};
extern void *nanos_smp_factory(void *args);
struct  mcc_struct_anon_12
{
  _Bool is_final:1;
  _Bool is_recover:1;
  _Bool is_implicit:1;
  _Bool reserved3:1;
  _Bool reserved4:1;
  _Bool reserved5:1;
  _Bool reserved6:1;
  _Bool reserved7:1;
};
typedef struct mcc_struct_anon_12 nanos_wd_dyn_flags_t;
typedef void *nanos_thread_t;
struct  mcc_struct_anon_13
{
  nanos_wd_dyn_flags_t flags;
  nanos_thread_t tie_to;
  int priority;
  void *callback;
  void *arguments;
};
typedef struct mcc_struct_anon_13 nanos_wd_dyn_props_t;
struct mcc_struct_anon_4;
typedef struct mcc_struct_anon_4 nanos_copy_data_internal_t;
typedef nanos_copy_data_internal_t nanos_copy_data_t;
struct mcc_struct_anon_0;
typedef struct mcc_struct_anon_0 nanos_region_dimension_internal_t;
typedef void *nanos_wg_t;
extern nanos_err_t nanos_create_wd_compact(nanos_wd_t *wd, nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void **data, nanos_wg_t wg, nanos_copy_data_t **copies, nanos_region_dimension_internal_t **dimensions);
extern nanos_wd_t nanos_current_wd(void);
struct  mcc_struct_anon_0
{
  size_t size;
  size_t lower_bound;
  size_t accessed_length;
};
typedef nanos_region_dimension_internal_t nanos_region_dimension_t;
struct  mcc_struct_anon_1
{
  _Bool input:1;
  _Bool output:1;
  _Bool can_rename:1;
  _Bool concurrent:1;
  _Bool commutative:1;
};
typedef struct mcc_struct_anon_1 nanos_access_type_internal_t;
typedef long int ptrdiff_t;
struct  mcc_struct_anon_2
{
  void *address;
  nanos_access_type_internal_t flags;
  short int dimension_count;
  const nanos_region_dimension_internal_t *dimensions;
  ptrdiff_t offset;
};
typedef struct mcc_struct_anon_2 nanos_data_access_internal_t;
typedef nanos_data_access_internal_t nanos_data_access_t;
enum mcc_enum_anon_0
{
  NANOS_PRIVATE = 0,
  NANOS_SHARED = 1
};
typedef enum mcc_enum_anon_0 nanos_sharing_t;
struct  mcc_struct_anon_5
{
  _Bool input:1;
  _Bool output:1;
};
typedef unsigned int reg_t;
struct  mcc_struct_anon_4
{
  void *address;
  nanos_sharing_t sharing;
  struct mcc_struct_anon_5 flags;
  short int dimension_count;
  const nanos_region_dimension_internal_t *dimensions;
  ptrdiff_t offset;
  uint64_t host_base_address;
  reg_t host_region_id;
  _Bool remote_host;
  void *deducted_cd;
};
typedef void (*nanos_translate_args_t)(void *, nanos_wd_t);
extern nanos_err_t nanos_set_translate_function(nanos_wd_t wd, nanos_translate_args_t translate_args);
typedef void *nanos_team_t;
extern nanos_err_t nanos_submit(nanos_wd_t wd, size_t num_data_accesses, nanos_data_access_t *data_accesses, nanos_team_t team);
extern nanos_err_t nanos_create_wd_and_run_compact(nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void *data, size_t num_data_accesses, nanos_data_access_t *data_accesses, nanos_copy_data_t *copies, nanos_region_dimension_internal_t *dimensions, nanos_translate_args_t translate_args);
static void smp_ol_matmul_3(struct nanos_args_1_t *const args);
extern nanos_err_t nanos_wg_wait_completion(nanos_wg_t wg, _Bool avoid_flush);
extern int fflush(FILE *__stream);
int main(int argc, char **argv)
{
  struct timespec time_start;
  int i;
  struct timespec time_end;
  elem_t k;
  int j;
  ompss_nanox_main_begin((void *)main, "src/ompss_evimm.c", 111);
  int errors = 0;
  long int total_matmul = 0;
  long int max_matmul = 0;
  long int min_matmul = 9223372036854775807L;
  long int mean_matmul = 0;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Initializing system ....................\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
  }
  ;
  if (argc > 1)
    {
      BLOCKSIZE = atoi(argv[1]);
      if (argc > 2)
        {
          N = atoi(argv[2]);
        }
    }
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: block size:%d matrix size:%d\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), BLOCKSIZE, N);
  }
  ;
  if (N < BLOCKSIZE)
    {
      {
        struct timespec _t1;
        time_t _secs;
        time_t _min;
        clock_gettime(5, &_t1);
        _secs = _t1.tv_sec % 60;
        _min = _t1.tv_sec / 60 % 60;
        fprintf(stderr, "[%02d:%02d.%06d]: bad sizes\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
      }
      ;
      exit( -1);
    }
  a = malloc(sizeof(elem_t) * N * N);
  b = malloc(sizeof(elem_t) * N * N);
  c = malloc(sizeof(elem_t) * N * N);
  matmul_time = malloc(sizeof(long int) * N);
  memset(matmul_time, 0, sizeof(long int) * N);
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Initializing system done!\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
  }
  ;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Initializing matrices ....................\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
  }
  ;
  seqinit(N, N, a, b);
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Initializing matrices done!\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
  }
  ;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Calculating matrices ....................\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
  }
  ;
  time_gettime(&time_start);
  for (i = 0; i < N - BLOCKSIZE; i += BLOCKSIZE)
    {
      {
        int mcc_arg_0 = BLOCKSIZE;
        int mcc_arg_1 = N;
        elem_t *mcc_arg_2 = a + i * N;
        elem_t *mcc_arg_3 = b;
        elem_t *mcc_arg_4 = c + i * N;
        long int *mcc_arg_5 = matmul_time + i;
        {
          _Bool mcc_is_in_final;
          nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
          if (mcc_err_in_final != NANOS_OK)
            {
              nanos_handle_error(mcc_err_in_final);
            }
          if (mcc_is_in_final)
            {
              matmul(BLOCKSIZE, N, a + i * N, b, c + i * N, matmul_time + i);
            }
          else
            {
              {
                nanos_wd_dyn_props_t nanos_wd_dyn_props;
                struct nanos_args_0_t *ol_args;
                nanos_err_t nanos_err;
                struct nanos_args_0_t imm_args;
                nanos_region_dimension_t dimensions_0[1L];
                nanos_data_access_t dependences[4L];
                nanos_region_dimension_t dimensions_1[1L];
                nanos_region_dimension_t dimensions_2[1L];
                nanos_region_dimension_t dimensions_3[1L];
                static nanos_smp_args_t smp_ol_matmul_1_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_0_t *))&smp_ol_matmul_1};
                static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_0_t), .num_copies = 4, .num_devices = 1, .num_dimensions = 4, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_matmul_1_args}}};
                nanos_wd_dyn_props.tie_to = 0;
                nanos_wd_dyn_props.priority = 0;
                nanos_wd_dyn_props.flags.is_final = 0;
                nanos_wd_dyn_props.flags.is_implicit = 0;
                nanos_wd_dyn_props.flags.is_recover = 0;
                ol_args = (struct nanos_args_0_t *)0;
                nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_0_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                if (nanos_err != NANOS_OK)
                  {
                    nanos_handle_error(nanos_err);
                  }
                dimensions_0[0].size = (((mcc_arg_0 * mcc_arg_1) - 1L - 0L) + 1L) * sizeof(elem_t);
                dimensions_0[0].lower_bound = (0L - 0L) * sizeof(elem_t);
                dimensions_0[0].accessed_length = ((mcc_arg_0 * mcc_arg_1) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                dependences[0].address = (void *)mcc_arg_2;
                dependences[0].offset = 0L;
                dependences[0].dimensions = dimensions_0;
                dependences[0].flags.input = 1;
                dependences[0].flags.output = 0;
                dependences[0].flags.can_rename = 0;
                dependences[0].flags.concurrent = 0;
                dependences[0].flags.commutative = 0;
                dependences[0].dimension_count = 1;
                dimensions_1[0].size = (((mcc_arg_1 * mcc_arg_1) - 1L - 0L) + 1L) * sizeof(elem_t);
                dimensions_1[0].lower_bound = (0L - 0L) * sizeof(elem_t);
                dimensions_1[0].accessed_length = ((mcc_arg_1 * mcc_arg_1) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                dependences[1].address = (void *)mcc_arg_3;
                dependences[1].offset = 0L;
                dependences[1].dimensions = dimensions_1;
                dependences[1].flags.input = 1;
                dependences[1].flags.output = 0;
                dependences[1].flags.can_rename = 0;
                dependences[1].flags.concurrent = 0;
                dependences[1].flags.commutative = 0;
                dependences[1].dimension_count = 1;
                dimensions_2[0].size = (((mcc_arg_0 * mcc_arg_1) - 1L - 0L) + 1L) * sizeof(elem_t);
                dimensions_2[0].lower_bound = (0L - 0L) * sizeof(elem_t);
                dimensions_2[0].accessed_length = ((mcc_arg_0 * mcc_arg_1) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                dependences[2].address = (void *)mcc_arg_4;
                dependences[2].offset = 0L;
                dependences[2].dimensions = dimensions_2;
                dependences[2].flags.input = 0;
                dependences[2].flags.output = 1;
                dependences[2].flags.can_rename = 0;
                dependences[2].flags.concurrent = 0;
                dependences[2].flags.commutative = 0;
                dependences[2].dimension_count = 1;
                dimensions_3[0].size = 1L * sizeof(long int);
                dimensions_3[0].lower_bound = (0L - 0L) * sizeof(long int);
                dimensions_3[0].accessed_length = (0L - 0L - (0L - 0L) + 1) * sizeof(long int);
                dependences[3].address = (void *)mcc_arg_5;
                dependences[3].offset = 0L;
                dependences[3].dimensions = dimensions_3;
                dependences[3].flags.input = 0;
                dependences[3].flags.output = 1;
                dependences[3].flags.can_rename = 0;
                dependences[3].flags.concurrent = 0;
                dependences[3].flags.commutative = 0;
                dependences[3].dimension_count = 1;
                if (nanos_wd_ != (nanos_wd_t)0)
                  {
                    (*ol_args).size = mcc_arg_0;
                    (*ol_args).n = mcc_arg_1;
                    (*ol_args).A = mcc_arg_2;
                    (*ol_args).B = mcc_arg_3;
                    (*ol_args).C = mcc_arg_4;
                    (*ol_args).elapsed = mcc_arg_5;
                    ol_copy_dimensions[0 + 0].size = (((mcc_arg_0 * mcc_arg_1) - 1L - 0L) + 1L) * sizeof(elem_t);
                    ol_copy_dimensions[0 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    ol_copy_dimensions[0 + 0].accessed_length = ((mcc_arg_0 * mcc_arg_1) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    ol_copy_data[0].sharing = NANOS_SHARED;
                    ol_copy_data[0].address = (void *)mcc_arg_2;
                    ol_copy_data[0].flags.input = 1;
                    ol_copy_data[0].flags.output = 0;
                    ol_copy_data[0].dimension_count = (short int)1;
                    ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                    ol_copy_data[0].offset = 0L;
                    ol_copy_dimensions[1 + 0].size = (((mcc_arg_1 * mcc_arg_1) - 1L - 0L) + 1L) * sizeof(elem_t);
                    ol_copy_dimensions[1 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    ol_copy_dimensions[1 + 0].accessed_length = ((mcc_arg_1 * mcc_arg_1) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    ol_copy_data[1].sharing = NANOS_SHARED;
                    ol_copy_data[1].address = (void *)mcc_arg_3;
                    ol_copy_data[1].flags.input = 1;
                    ol_copy_data[1].flags.output = 0;
                    ol_copy_data[1].dimension_count = (short int)1;
                    ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                    ol_copy_data[1].offset = 0L;
                    ol_copy_dimensions[2 + 0].size = (((mcc_arg_0 * mcc_arg_1) - 1L - 0L) + 1L) * sizeof(elem_t);
                    ol_copy_dimensions[2 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    ol_copy_dimensions[2 + 0].accessed_length = ((mcc_arg_0 * mcc_arg_1) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    ol_copy_data[2].sharing = NANOS_SHARED;
                    ol_copy_data[2].address = (void *)mcc_arg_4;
                    ol_copy_data[2].flags.input = 0;
                    ol_copy_data[2].flags.output = 1;
                    ol_copy_data[2].dimension_count = (short int)1;
                    ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                    ol_copy_data[2].offset = 0L;
                    ol_copy_dimensions[3 + 0].size = 1L * sizeof(long int);
                    ol_copy_dimensions[3 + 0].lower_bound = (0L - 0L) * sizeof(long int);
                    ol_copy_dimensions[3 + 0].accessed_length = (0L - 0L - (0L - 0L) + 1) * sizeof(long int);
                    ol_copy_data[3].sharing = NANOS_SHARED;
                    ol_copy_data[3].address = (void *)mcc_arg_5;
                    ol_copy_data[3].flags.input = 0;
                    ol_copy_data[3].flags.output = 1;
                    ol_copy_data[3].dimension_count = (short int)1;
                    ol_copy_data[3].dimensions = &ol_copy_dimensions[3];
                    ol_copy_data[3].offset = 0L;
                    nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_ompssevimmc_0);
                    if (nanos_err != NANOS_OK)
                      {
                        nanos_handle_error(nanos_err);
                      }
                    nanos_err = nanos_submit(nanos_wd_, 4, &dependences[0], (nanos_team_t)0);
                    if (nanos_err != NANOS_OK)
                      {
                        nanos_handle_error(nanos_err);
                      }
                  }
                else
                  {
                    nanos_region_dimension_internal_t imm_copy_dimensions[4L];
                    nanos_copy_data_t imm_copy_data[4L];
                    imm_args.size = mcc_arg_0;
                    imm_args.n = mcc_arg_1;
                    imm_args.A = mcc_arg_2;
                    imm_args.B = mcc_arg_3;
                    imm_args.C = mcc_arg_4;
                    imm_args.elapsed = mcc_arg_5;
                    imm_copy_dimensions[0 + 0].size = (((mcc_arg_0 * mcc_arg_1) - 1L - 0L) + 1L) * sizeof(elem_t);
                    imm_copy_dimensions[0 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    imm_copy_dimensions[0 + 0].accessed_length = ((mcc_arg_0 * mcc_arg_1) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    imm_copy_data[0].sharing = NANOS_SHARED;
                    imm_copy_data[0].address = (void *)mcc_arg_2;
                    imm_copy_data[0].flags.input = 1;
                    imm_copy_data[0].flags.output = 0;
                    imm_copy_data[0].dimension_count = (short int)1;
                    imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                    imm_copy_data[0].offset = 0L;
                    imm_copy_dimensions[1 + 0].size = (((mcc_arg_1 * mcc_arg_1) - 1L - 0L) + 1L) * sizeof(elem_t);
                    imm_copy_dimensions[1 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    imm_copy_dimensions[1 + 0].accessed_length = ((mcc_arg_1 * mcc_arg_1) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    imm_copy_data[1].sharing = NANOS_SHARED;
                    imm_copy_data[1].address = (void *)mcc_arg_3;
                    imm_copy_data[1].flags.input = 1;
                    imm_copy_data[1].flags.output = 0;
                    imm_copy_data[1].dimension_count = (short int)1;
                    imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                    imm_copy_data[1].offset = 0L;
                    imm_copy_dimensions[2 + 0].size = (((mcc_arg_0 * mcc_arg_1) - 1L - 0L) + 1L) * sizeof(elem_t);
                    imm_copy_dimensions[2 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    imm_copy_dimensions[2 + 0].accessed_length = ((mcc_arg_0 * mcc_arg_1) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    imm_copy_data[2].sharing = NANOS_SHARED;
                    imm_copy_data[2].address = (void *)mcc_arg_4;
                    imm_copy_data[2].flags.input = 0;
                    imm_copy_data[2].flags.output = 1;
                    imm_copy_data[2].dimension_count = (short int)1;
                    imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                    imm_copy_data[2].offset = 0L;
                    imm_copy_dimensions[3 + 0].size = 1L * sizeof(long int);
                    imm_copy_dimensions[3 + 0].lower_bound = (0L - 0L) * sizeof(long int);
                    imm_copy_dimensions[3 + 0].accessed_length = (0L - 0L - (0L - 0L) + 1) * sizeof(long int);
                    imm_copy_data[3].sharing = NANOS_SHARED;
                    imm_copy_data[3].address = (void *)mcc_arg_5;
                    imm_copy_data[3].flags.input = 0;
                    imm_copy_data[3].flags.output = 1;
                    imm_copy_data[3].dimension_count = (short int)1;
                    imm_copy_data[3].dimensions = &imm_copy_dimensions[3];
                    imm_copy_data[3].offset = 0L;
                    nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_0_t), &imm_args, 4, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_ompssevimmc_0);
                    if (nanos_err != NANOS_OK)
                      {
                        nanos_handle_error(nanos_err);
                      }
                  }
              }
            }
        }
      }
    }
  if (i < N)
    {
      {
        int mcc_arg_8 = N - i;
        int mcc_arg_9 = N;
        elem_t *mcc_arg_10 = a + i * N;
        elem_t *mcc_arg_11 = b;
        elem_t *mcc_arg_12 = c + i * N;
        long int *mcc_arg_13 = matmul_time + i;
        {
          _Bool mcc_is_in_final;
          nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
          if (mcc_err_in_final != NANOS_OK)
            {
              nanos_handle_error(mcc_err_in_final);
            }
          if (mcc_is_in_final)
            {
              matmul(N - i, N, a + i * N, b, c + i * N, matmul_time + i);
            }
          else
            {
              {
                nanos_wd_dyn_props_t nanos_wd_dyn_props;
                struct nanos_args_1_t *ol_args;
                nanos_err_t nanos_err;
                struct nanos_args_1_t imm_args;
                nanos_region_dimension_t dimensions_4[1L];
                nanos_data_access_t dependences[4L];
                nanos_region_dimension_t dimensions_5[1L];
                nanos_region_dimension_t dimensions_6[1L];
                nanos_region_dimension_t dimensions_7[1L];
                static nanos_smp_args_t smp_ol_matmul_3_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_1_t *))&smp_ol_matmul_3};
                static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_1_t), .num_copies = 4, .num_devices = 1, .num_dimensions = 4, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_matmul_3_args}}};
                nanos_wd_dyn_props.tie_to = 0;
                nanos_wd_dyn_props.priority = 0;
                nanos_wd_dyn_props.flags.is_final = 0;
                nanos_wd_dyn_props.flags.is_implicit = 0;
                nanos_wd_dyn_props.flags.is_recover = 0;
                ol_args = (struct nanos_args_1_t *)0;
                nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_1_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                if (nanos_err != NANOS_OK)
                  {
                    nanos_handle_error(nanos_err);
                  }
                dimensions_4[0].size = (((mcc_arg_8 * mcc_arg_9) - 1L - 0L) + 1L) * sizeof(elem_t);
                dimensions_4[0].lower_bound = (0L - 0L) * sizeof(elem_t);
                dimensions_4[0].accessed_length = ((mcc_arg_8 * mcc_arg_9) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                dependences[0].address = (void *)mcc_arg_10;
                dependences[0].offset = 0L;
                dependences[0].dimensions = dimensions_4;
                dependences[0].flags.input = 1;
                dependences[0].flags.output = 0;
                dependences[0].flags.can_rename = 0;
                dependences[0].flags.concurrent = 0;
                dependences[0].flags.commutative = 0;
                dependences[0].dimension_count = 1;
                dimensions_5[0].size = (((mcc_arg_9 * mcc_arg_9) - 1L - 0L) + 1L) * sizeof(elem_t);
                dimensions_5[0].lower_bound = (0L - 0L) * sizeof(elem_t);
                dimensions_5[0].accessed_length = ((mcc_arg_9 * mcc_arg_9) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                dependences[1].address = (void *)mcc_arg_11;
                dependences[1].offset = 0L;
                dependences[1].dimensions = dimensions_5;
                dependences[1].flags.input = 1;
                dependences[1].flags.output = 0;
                dependences[1].flags.can_rename = 0;
                dependences[1].flags.concurrent = 0;
                dependences[1].flags.commutative = 0;
                dependences[1].dimension_count = 1;
                dimensions_6[0].size = (((mcc_arg_8 * mcc_arg_9) - 1L - 0L) + 1L) * sizeof(elem_t);
                dimensions_6[0].lower_bound = (0L - 0L) * sizeof(elem_t);
                dimensions_6[0].accessed_length = ((mcc_arg_8 * mcc_arg_9) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                dependences[2].address = (void *)mcc_arg_12;
                dependences[2].offset = 0L;
                dependences[2].dimensions = dimensions_6;
                dependences[2].flags.input = 0;
                dependences[2].flags.output = 1;
                dependences[2].flags.can_rename = 0;
                dependences[2].flags.concurrent = 0;
                dependences[2].flags.commutative = 0;
                dependences[2].dimension_count = 1;
                dimensions_7[0].size = 1L * sizeof(long int);
                dimensions_7[0].lower_bound = (0L - 0L) * sizeof(long int);
                dimensions_7[0].accessed_length = (0L - 0L - (0L - 0L) + 1) * sizeof(long int);
                dependences[3].address = (void *)mcc_arg_13;
                dependences[3].offset = 0L;
                dependences[3].dimensions = dimensions_7;
                dependences[3].flags.input = 0;
                dependences[3].flags.output = 1;
                dependences[3].flags.can_rename = 0;
                dependences[3].flags.concurrent = 0;
                dependences[3].flags.commutative = 0;
                dependences[3].dimension_count = 1;
                if (nanos_wd_ != (nanos_wd_t)0)
                  {
                    (*ol_args).size = mcc_arg_8;
                    (*ol_args).n = mcc_arg_9;
                    (*ol_args).A = mcc_arg_10;
                    (*ol_args).B = mcc_arg_11;
                    (*ol_args).C = mcc_arg_12;
                    (*ol_args).elapsed = mcc_arg_13;
                    ol_copy_dimensions[0 + 0].size = (((mcc_arg_8 * mcc_arg_9) - 1L - 0L) + 1L) * sizeof(elem_t);
                    ol_copy_dimensions[0 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    ol_copy_dimensions[0 + 0].accessed_length = ((mcc_arg_8 * mcc_arg_9) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    ol_copy_data[0].sharing = NANOS_SHARED;
                    ol_copy_data[0].address = (void *)mcc_arg_10;
                    ol_copy_data[0].flags.input = 1;
                    ol_copy_data[0].flags.output = 0;
                    ol_copy_data[0].dimension_count = (short int)1;
                    ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                    ol_copy_data[0].offset = 0L;
                    ol_copy_dimensions[1 + 0].size = (((mcc_arg_9 * mcc_arg_9) - 1L - 0L) + 1L) * sizeof(elem_t);
                    ol_copy_dimensions[1 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    ol_copy_dimensions[1 + 0].accessed_length = ((mcc_arg_9 * mcc_arg_9) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    ol_copy_data[1].sharing = NANOS_SHARED;
                    ol_copy_data[1].address = (void *)mcc_arg_11;
                    ol_copy_data[1].flags.input = 1;
                    ol_copy_data[1].flags.output = 0;
                    ol_copy_data[1].dimension_count = (short int)1;
                    ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                    ol_copy_data[1].offset = 0L;
                    ol_copy_dimensions[2 + 0].size = (((mcc_arg_8 * mcc_arg_9) - 1L - 0L) + 1L) * sizeof(elem_t);
                    ol_copy_dimensions[2 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    ol_copy_dimensions[2 + 0].accessed_length = ((mcc_arg_8 * mcc_arg_9) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    ol_copy_data[2].sharing = NANOS_SHARED;
                    ol_copy_data[2].address = (void *)mcc_arg_12;
                    ol_copy_data[2].flags.input = 0;
                    ol_copy_data[2].flags.output = 1;
                    ol_copy_data[2].dimension_count = (short int)1;
                    ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                    ol_copy_data[2].offset = 0L;
                    ol_copy_dimensions[3 + 0].size = 1L * sizeof(long int);
                    ol_copy_dimensions[3 + 0].lower_bound = (0L - 0L) * sizeof(long int);
                    ol_copy_dimensions[3 + 0].accessed_length = (0L - 0L - (0L - 0L) + 1) * sizeof(long int);
                    ol_copy_data[3].sharing = NANOS_SHARED;
                    ol_copy_data[3].address = (void *)mcc_arg_13;
                    ol_copy_data[3].flags.input = 0;
                    ol_copy_data[3].flags.output = 1;
                    ol_copy_data[3].dimension_count = (short int)1;
                    ol_copy_data[3].dimensions = &ol_copy_dimensions[3];
                    ol_copy_data[3].offset = 0L;
                    nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_ompssevimmc_1);
                    if (nanos_err != NANOS_OK)
                      {
                        nanos_handle_error(nanos_err);
                      }
                    nanos_err = nanos_submit(nanos_wd_, 4, &dependences[0], (nanos_team_t)0);
                    if (nanos_err != NANOS_OK)
                      {
                        nanos_handle_error(nanos_err);
                      }
                  }
                else
                  {
                    nanos_region_dimension_internal_t imm_copy_dimensions[4L];
                    nanos_copy_data_t imm_copy_data[4L];
                    imm_args.size = mcc_arg_8;
                    imm_args.n = mcc_arg_9;
                    imm_args.A = mcc_arg_10;
                    imm_args.B = mcc_arg_11;
                    imm_args.C = mcc_arg_12;
                    imm_args.elapsed = mcc_arg_13;
                    imm_copy_dimensions[0 + 0].size = (((mcc_arg_8 * mcc_arg_9) - 1L - 0L) + 1L) * sizeof(elem_t);
                    imm_copy_dimensions[0 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    imm_copy_dimensions[0 + 0].accessed_length = ((mcc_arg_8 * mcc_arg_9) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    imm_copy_data[0].sharing = NANOS_SHARED;
                    imm_copy_data[0].address = (void *)mcc_arg_10;
                    imm_copy_data[0].flags.input = 1;
                    imm_copy_data[0].flags.output = 0;
                    imm_copy_data[0].dimension_count = (short int)1;
                    imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                    imm_copy_data[0].offset = 0L;
                    imm_copy_dimensions[1 + 0].size = (((mcc_arg_9 * mcc_arg_9) - 1L - 0L) + 1L) * sizeof(elem_t);
                    imm_copy_dimensions[1 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    imm_copy_dimensions[1 + 0].accessed_length = ((mcc_arg_9 * mcc_arg_9) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    imm_copy_data[1].sharing = NANOS_SHARED;
                    imm_copy_data[1].address = (void *)mcc_arg_11;
                    imm_copy_data[1].flags.input = 1;
                    imm_copy_data[1].flags.output = 0;
                    imm_copy_data[1].dimension_count = (short int)1;
                    imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                    imm_copy_data[1].offset = 0L;
                    imm_copy_dimensions[2 + 0].size = (((mcc_arg_8 * mcc_arg_9) - 1L - 0L) + 1L) * sizeof(elem_t);
                    imm_copy_dimensions[2 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    imm_copy_dimensions[2 + 0].accessed_length = ((mcc_arg_8 * mcc_arg_9) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    imm_copy_data[2].sharing = NANOS_SHARED;
                    imm_copy_data[2].address = (void *)mcc_arg_12;
                    imm_copy_data[2].flags.input = 0;
                    imm_copy_data[2].flags.output = 1;
                    imm_copy_data[2].dimension_count = (short int)1;
                    imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                    imm_copy_data[2].offset = 0L;
                    imm_copy_dimensions[3 + 0].size = 1L * sizeof(long int);
                    imm_copy_dimensions[3 + 0].lower_bound = (0L - 0L) * sizeof(long int);
                    imm_copy_dimensions[3 + 0].accessed_length = (0L - 0L - (0L - 0L) + 1) * sizeof(long int);
                    imm_copy_data[3].sharing = NANOS_SHARED;
                    imm_copy_data[3].address = (void *)mcc_arg_13;
                    imm_copy_data[3].flags.input = 0;
                    imm_copy_data[3].flags.output = 1;
                    imm_copy_data[3].dimension_count = (short int)1;
                    imm_copy_data[3].dimensions = &imm_copy_dimensions[3];
                    imm_copy_data[3].offset = 0L;
                    nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_1_t), &imm_args, 4, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_ompssevimmc_1);
                    if (nanos_err != NANOS_OK)
                      {
                        nanos_handle_error(nanos_err);
                      }
                  }
              }
            }
        }
      }
    }
  {
    nanos_err_t nanos_err;
    nanos_wd_t nanos_wd_ = nanos_current_wd();
    nanos_err = nanos_wg_wait_completion(nanos_wd_, 0);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
  }
  time_gettime(&time_end);
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Calculating matrices done!\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
  }
  ;
  k = N * (N - 1) / 2;
  for (i = 0; i < N; i++)
    {
      for (j = 0; j < N; j++)
        {
          if (c[i * N + j] != k * j)
            {
              errors++;
            }
        }
    }
  if (errors != 0)
    {
      struct timespec _t1;
      time_t _secs;
      time_t _min;
      clock_gettime(5, &_t1);
      _secs = _t1.tv_sec % 60;
      _min = _t1.tv_sec / 60 % 60;
      fprintf(stderr, "[%02d:%02d.%06d]: NOTE: Found %d ERRORS!\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), errors);
    }
  ;
  for (i = 0; i < N - BLOCKSIZE; i += BLOCKSIZE)
    {
      if (matmul_time[i] >= 0)
        {
          total_matmul += matmul_time[i];
          max_matmul = max_matmul > matmul_time[i] ? max_matmul : matmul_time[i];
          min_matmul = min_matmul < matmul_time[i] ? min_matmul : matmul_time[i];
        }
    }
  if (i < N)
    {
      if (matmul_time[i] >= 0)
        {
          total_matmul += matmul_time[i];
          max_matmul = max_matmul > matmul_time[i] ? max_matmul : matmul_time[i];
          min_matmul = min_matmul < matmul_time[i] ? min_matmul : matmul_time[i];
        }
    }
  mean_matmul = total_matmul / (N / BLOCKSIZE);
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Min matmul       %8ld msec\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), min_matmul);
  }
  ;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Mean matmul      %8ld msec\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), mean_matmul);
  }
  ;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Max matmul       %8ld msec\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), max_matmul);
  }
  ;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Total matmul     %8ld msec\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), total_matmul);
  }
  ;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Total elab time  %8ld msec\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), time_diff_msec(&time_end, &time_start));
  }
  ;
  fflush(stderr);
  {
    nanos_err_t nanos_err;
    nanos_wd_t nanos_wd_ = nanos_current_wd();
    nanos_err = nanos_wg_wait_completion(nanos_wd_, 0);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
  }
  return errors;
}
static void smp_ol_matmul_1_unpacked(int size, int n, elem_t *A, elem_t *B, elem_t *C, long int *elapsed)
{
  {
    matmul(size, n, A, B, C, elapsed);
  }
}
static void smp_ol_matmul_1(struct nanos_args_0_t *const args)
{
  {
    smp_ol_matmul_1_unpacked((*args).size, (*args).n, (*args).A, (*args).B, (*args).C, (*args).elapsed);
  }
}
static void smp_ol_matmul_3_unpacked(int size, int n, elem_t *A, elem_t *B, elem_t *C, long int *elapsed)
{
  {
    matmul(size, n, A, B, C, elapsed);
  }
}
static void smp_ol_matmul_3(struct nanos_args_1_t *const args)
{
  {
    smp_ol_matmul_3_unpacked((*args).size, (*args).n, (*args).A, (*args).B, (*args).C, (*args).elapsed);
  }
}
