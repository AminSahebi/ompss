struct _IO_FILE_plus;
extern struct _IO_FILE_plus _IO_2_1_stdin_;
extern struct _IO_FILE_plus _IO_2_1_stdout_;
extern struct _IO_FILE_plus _IO_2_1_stderr_;
struct _IO_FILE;
extern struct _IO_FILE *stdin;
extern struct _IO_FILE *stdout;
extern struct _IO_FILE *stderr;
extern int sys_nerr;
extern const char *const sys_errlist[];
static __inline unsigned int __bswap_32(unsigned int __bsx)
{
  return __builtin_bswap32(__bsx);
}
typedef unsigned long int __uint64_t;
static __inline __uint64_t __bswap_64(__uint64_t __bsx)
{
  return __builtin_bswap64(__bsx);
}
typedef unsigned short int __uint16_t;
static __inline __uint16_t __uint16_identity(__uint16_t __x)
{
  return __x;
}
typedef unsigned int __uint32_t;
static __inline __uint32_t __uint32_identity(__uint32_t __x)
{
  return __x;
}
static __inline __uint64_t __uint64_identity(__uint64_t __x)
{
  return __x;
}
extern char **__environ;
extern char *optarg;
extern int optind;
extern int opterr;
extern int optopt;
extern char *__tzname[2L];
extern int __daylight;
extern long int __timezone;
extern char *tzname[2L];
extern int daylight;
extern long int timezone;
struct timespec;
typedef long int __time_t;
typedef long int __syscall_slong_t;
struct  timespec
{
  __time_t tv_sec;
  __syscall_slong_t tv_nsec;
};
static __inline long int time_diff_sec(struct timespec *new, struct timespec *old)
{
  return (*new).tv_sec - (*old).tv_sec + ((*new).tv_nsec - (*old).tv_nsec) / 1000000000;
}
static __inline long int time_diff_msec(struct timespec *new, struct timespec *old)
{
  return ((*new).tv_sec - (*old).tv_sec) * 1000 + ((*new).tv_nsec - (*old).tv_nsec) / 1000000;
}
static __inline long int time_sec(struct timespec *t)
{
  return (*t).tv_sec + (*t).tv_nsec / 1000000000;
}
static __inline long int time_msec(struct timespec *t)
{
  return (*t).tv_sec * 1000 + (*t).tv_nsec / 1000000;
}
typedef int __clockid_t;
typedef __clockid_t clockid_t;
extern int clock_gettime(clockid_t __clock_id, struct timespec *__tp) __attribute__((__nothrow__)) __attribute__((__leaf__));
static __inline void time_gettime(struct timespec *t)
{
  clock_gettime(5, t);
}
static int BLOCKSIZE = 25;
static int N = 300;
typedef __uint64_t uint64_t;
typedef uint64_t elem_t;
void seqinit(int size, int n, elem_t *A, elem_t *B)
{
  int i;
  int j;
  for (i = 0; i < size; i++)
    {
      for (j = 0; j < n; j++)
        {
          A[i * n + j] = j;
          B[i * n + j] = j;
        }
    }
}
typedef __time_t time_t;
typedef struct _IO_FILE FILE;
extern int fprintf(FILE *__restrict __stream, const char *__restrict __format, ...);
extern struct _IO_FILE *stderr;
void matmul(int size, int n, elem_t *A, elem_t *B, elem_t *C, long int *elapsed)
{
  struct timespec time_start;
  int i;
  int j;
  elem_t tmp;
  elem_t k;
  struct timespec time_end;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Start matmul %p %p %p\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), A, B, C);
  }
  ;
  time_gettime(&time_start);
  for (i = 0; i < size; i++)
    {
      for (j = 0; j < n; j++)
        {
          tmp = 0;
          for (k = 0; k < n; k++)
            {
              tmp += A[i * n + k] * B[k * n + j];
            }
          C[i * n + j] = tmp;
        }
    }
  time_gettime(&time_end);
  *elapsed = time_diff_msec(&time_end, &time_start);
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: End matmul   %p %p %p elapse=%ld\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), A, B, C, *elapsed);
  }
  ;
}
static elem_t *a;
static elem_t *b;
static elem_t *c;
static long int *matmul_time;
struct  nanos_args_2_t
{
  int size;
  int n;
  elem_t *A;
  elem_t *B;
  elem_t *C;
  long int *elapsed;
};
typedef void *nanos_wd_t;
enum mcc_enum_anon_15
{
  NANOS_OK = 0,
  NANOS_UNKNOWN_ERR = 1,
  NANOS_UNIMPLEMENTED = 2,
  NANOS_ENOMEM = 3,
  NANOS_INVALID_PARAM = 4,
  NANOS_INVALID_REQUEST = 5
};
typedef enum mcc_enum_anon_15 nanos_err_t;
typedef unsigned int nanos_copy_id_t;
extern nanos_err_t nanos_get_addr(nanos_copy_id_t copy_id, void **addr, nanos_wd_t cwd);
extern void nanos_handle_error(nanos_err_t err);
static void nanos_xlate_fun_mmc_2(struct nanos_args_2_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).A = (elem_t *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(1, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).B = (elem_t *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(2, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).C = (elem_t *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(3, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).elapsed = (long int *)device_base_address;
  }
}
struct  nanos_args_3_t
{
  int size;
  int n;
  elem_t *A;
  elem_t *B;
  elem_t *C;
  long int *elapsed;
};
static void nanos_xlate_fun_mmc_3(struct nanos_args_3_t *const arg, void *wd)
{
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(0, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).A = (elem_t *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(1, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).B = (elem_t *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(2, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).C = (elem_t *)device_base_address;
  }
  {
    void *device_base_address;
    nanos_err_t nanos_err;
    device_base_address = 0;
    nanos_err = nanos_get_addr(3, &device_base_address, wd);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
    (*arg).elapsed = (long int *)device_base_address;
  }
}
extern void ompss_nanox_main_begin(void *addr, const char *filename, int line);
extern int atoi(const char *__nptr) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__pure__)) __attribute__((__nonnull__(1)));
extern void exit(int __status) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__noreturn__));
typedef unsigned long int size_t;
extern void *malloc(size_t __size) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__malloc__));
extern void *memset(void *__s, int __c, size_t __n) __attribute__((__nothrow__)) __attribute__((__leaf__)) __attribute__((__nonnull__(1)));
extern nanos_err_t nanos_in_final(_Bool *result);
struct  mcc_struct_anon_57
{
  void (*outline)(void *);
};
typedef struct mcc_struct_anon_57 nanos_smp_args_t;
static void smp_ol_matmul_5(struct nanos_args_2_t *const args);
struct  mcc_struct_anon_53
{
  _Bool mandatory_creation:1;
  _Bool tied:1;
  _Bool clear_chunk:1;
  _Bool reserved0:1;
  _Bool reserved1:1;
  _Bool reserved2:1;
  _Bool reserved3:1;
  _Bool reserved4:1;
};
typedef struct mcc_struct_anon_53 nanos_wd_props_t;
struct  nanos_const_wd_definition_tag
{
  nanos_wd_props_t props;
  size_t data_alignment;
  size_t num_copies;
  size_t num_devices;
  size_t num_dimensions;
  const char *description;
};
typedef struct nanos_const_wd_definition_tag nanos_const_wd_definition_t;
struct  mcc_struct_anon_56
{
  void *(*factory)(void *);
  void *arg;
};
typedef struct mcc_struct_anon_56 nanos_device_t;
struct  nanos_const_wd_definition_1
{
  nanos_const_wd_definition_t base;
  nanos_device_t devices[1L];
};
extern void *nanos_smp_factory(void *args);
struct  mcc_struct_anon_54
{
  _Bool is_final:1;
  _Bool is_recover:1;
  _Bool is_implicit:1;
  _Bool reserved3:1;
  _Bool reserved4:1;
  _Bool reserved5:1;
  _Bool reserved6:1;
  _Bool reserved7:1;
};
typedef struct mcc_struct_anon_54 nanos_wd_dyn_flags_t;
typedef void *nanos_thread_t;
struct  mcc_struct_anon_55
{
  nanos_wd_dyn_flags_t flags;
  nanos_thread_t tie_to;
  int priority;
  void *callback;
  void *arguments;
};
typedef struct mcc_struct_anon_55 nanos_wd_dyn_props_t;
struct mcc_struct_anon_46;
typedef struct mcc_struct_anon_46 nanos_copy_data_internal_t;
typedef nanos_copy_data_internal_t nanos_copy_data_t;
struct mcc_struct_anon_42;
typedef struct mcc_struct_anon_42 nanos_region_dimension_internal_t;
typedef void *nanos_wg_t;
extern nanos_err_t nanos_create_wd_compact(nanos_wd_t *wd, nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void **data, nanos_wg_t wg, nanos_copy_data_t **copies, nanos_region_dimension_internal_t **dimensions);
extern nanos_wd_t nanos_current_wd(void);
struct  mcc_struct_anon_42
{
  size_t size;
  size_t lower_bound;
  size_t accessed_length;
};
typedef nanos_region_dimension_internal_t nanos_region_dimension_t;
struct  mcc_struct_anon_43
{
  _Bool input:1;
  _Bool output:1;
  _Bool can_rename:1;
  _Bool concurrent:1;
  _Bool commutative:1;
};
typedef struct mcc_struct_anon_43 nanos_access_type_internal_t;
typedef long int ptrdiff_t;
struct  mcc_struct_anon_44
{
  void *address;
  nanos_access_type_internal_t flags;
  short int dimension_count;
  const nanos_region_dimension_internal_t *dimensions;
  ptrdiff_t offset;
};
typedef struct mcc_struct_anon_44 nanos_data_access_internal_t;
typedef nanos_data_access_internal_t nanos_data_access_t;
enum mcc_enum_anon_10
{
  NANOS_PRIVATE = 0,
  NANOS_SHARED = 1
};
typedef enum mcc_enum_anon_10 nanos_sharing_t;
struct  mcc_struct_anon_47
{
  _Bool input:1;
  _Bool output:1;
};
typedef unsigned int reg_t;
struct  mcc_struct_anon_46
{
  void *address;
  nanos_sharing_t sharing;
  struct mcc_struct_anon_47 flags;
  short int dimension_count;
  const nanos_region_dimension_internal_t *dimensions;
  ptrdiff_t offset;
  uint64_t host_base_address;
  reg_t host_region_id;
  _Bool remote_host;
  void *deducted_cd;
};
typedef void (*nanos_translate_args_t)(void *, nanos_wd_t);
extern nanos_err_t nanos_set_translate_function(nanos_wd_t wd, nanos_translate_args_t translate_args);
typedef void *nanos_team_t;
extern nanos_err_t nanos_submit(nanos_wd_t wd, size_t num_data_accesses, nanos_data_access_t *data_accesses, nanos_team_t team);
extern nanos_err_t nanos_create_wd_and_run_compact(nanos_const_wd_definition_t *const_data, nanos_wd_dyn_props_t *dyn_props, size_t data_size, void *data, size_t num_data_accesses, nanos_data_access_t *data_accesses, nanos_copy_data_t *copies, nanos_region_dimension_internal_t *dimensions, nanos_translate_args_t translate_args);
static void smp_ol_matmul_7(struct nanos_args_3_t *const args);
extern nanos_err_t nanos_wg_wait_completion(nanos_wg_t wg, _Bool avoid_flush);
extern int fflush(FILE *__stream);
int main(int argc, char **argv)
{
  struct timespec time_start;
  int i;
  struct timespec time_end;
  elem_t k;
  int j;
  ompss_nanox_main_begin((void *)main, "src/mm.c", 111);
  int errors = 0;
  long int total_matmul = 0;
  long int max_matmul = 0;
  long int min_matmul = 9223372036854775807L;
  long int mean_matmul = 0;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Initializing system ....................\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
  }
  ;
  if (argc > 1)
    {
      BLOCKSIZE = atoi(argv[1]);
      if (argc > 2)
        {
          N = atoi(argv[2]);
        }
    }
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: block size:%d matrix size:%d\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), BLOCKSIZE, N);
  }
  ;
  if (N < BLOCKSIZE)
    {
      {
        struct timespec _t1;
        time_t _secs;
        time_t _min;
        clock_gettime(5, &_t1);
        _secs = _t1.tv_sec % 60;
        _min = _t1.tv_sec / 60 % 60;
        fprintf(stderr, "[%02d:%02d.%06d]: bad sizes\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
      }
      ;
      exit( -1);
    }
  a = malloc(sizeof(elem_t) * N * N);
  b = malloc(sizeof(elem_t) * N * N);
  c = malloc(sizeof(elem_t) * N * N);
  matmul_time = malloc(sizeof(long int) * N);
  memset(matmul_time, 0, sizeof(long int) * N);
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Initializing system done!\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
  }
  ;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Initializing matrices ....................\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
  }
  ;
  seqinit(N, N, a, b);
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Initializing matrices done!\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
  }
  ;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Calculating matrices ....................\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
  }
  ;
  time_gettime(&time_start);
  for (i = 0; i < N - BLOCKSIZE; i += BLOCKSIZE)
    {
      {
        int mcc_arg_16 = BLOCKSIZE;
        int mcc_arg_17 = N;
        elem_t *mcc_arg_18 = a + i * N;
        elem_t *mcc_arg_19 = b;
        elem_t *mcc_arg_20 = c + i * N;
        long int *mcc_arg_21 = matmul_time + i;
        {
          _Bool mcc_is_in_final;
          nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
          if (mcc_err_in_final != NANOS_OK)
            {
              nanos_handle_error(mcc_err_in_final);
            }
          if (mcc_is_in_final)
            {
              matmul(BLOCKSIZE, N, a + i * N, b, c + i * N, matmul_time + i);
            }
          else
            {
              {
                nanos_wd_dyn_props_t nanos_wd_dyn_props;
                struct nanos_args_2_t *ol_args;
                nanos_err_t nanos_err;
                struct nanos_args_2_t imm_args;
                nanos_region_dimension_t dimensions_8[1L];
                nanos_data_access_t dependences[4L];
                nanos_region_dimension_t dimensions_9[1L];
                nanos_region_dimension_t dimensions_10[1L];
                nanos_region_dimension_t dimensions_11[1L];
                static nanos_smp_args_t smp_ol_matmul_5_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_2_t *))&smp_ol_matmul_5};
                static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_2_t), .num_copies = 4, .num_devices = 1, .num_dimensions = 4, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_matmul_5_args}}};
                nanos_wd_dyn_props.tie_to = 0;
                nanos_wd_dyn_props.priority = 0;
                nanos_wd_dyn_props.flags.is_final = 0;
                nanos_wd_dyn_props.flags.is_implicit = 0;
                nanos_wd_dyn_props.flags.is_recover = 0;
                ol_args = (struct nanos_args_2_t *)0;
                nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_2_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                if (nanos_err != NANOS_OK)
                  {
                    nanos_handle_error(nanos_err);
                  }
                dimensions_8[0].size = (((mcc_arg_16 * mcc_arg_17) - 1L - 0L) + 1L) * sizeof(elem_t);
                dimensions_8[0].lower_bound = (0L - 0L) * sizeof(elem_t);
                dimensions_8[0].accessed_length = ((mcc_arg_16 * mcc_arg_17) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                dependences[0].address = (void *)mcc_arg_18;
                dependences[0].offset = 0L;
                dependences[0].dimensions = dimensions_8;
                dependences[0].flags.input = 1;
                dependences[0].flags.output = 0;
                dependences[0].flags.can_rename = 0;
                dependences[0].flags.concurrent = 0;
                dependences[0].flags.commutative = 0;
                dependences[0].dimension_count = 1;
                dimensions_9[0].size = (((mcc_arg_17 * mcc_arg_17) - 1L - 0L) + 1L) * sizeof(elem_t);
                dimensions_9[0].lower_bound = (0L - 0L) * sizeof(elem_t);
                dimensions_9[0].accessed_length = ((mcc_arg_17 * mcc_arg_17) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                dependences[1].address = (void *)mcc_arg_19;
                dependences[1].offset = 0L;
                dependences[1].dimensions = dimensions_9;
                dependences[1].flags.input = 1;
                dependences[1].flags.output = 0;
                dependences[1].flags.can_rename = 0;
                dependences[1].flags.concurrent = 0;
                dependences[1].flags.commutative = 0;
                dependences[1].dimension_count = 1;
                dimensions_10[0].size = (((mcc_arg_16 * mcc_arg_17) - 1L - 0L) + 1L) * sizeof(elem_t);
                dimensions_10[0].lower_bound = (0L - 0L) * sizeof(elem_t);
                dimensions_10[0].accessed_length = ((mcc_arg_16 * mcc_arg_17) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                dependences[2].address = (void *)mcc_arg_20;
                dependences[2].offset = 0L;
                dependences[2].dimensions = dimensions_10;
                dependences[2].flags.input = 0;
                dependences[2].flags.output = 1;
                dependences[2].flags.can_rename = 0;
                dependences[2].flags.concurrent = 0;
                dependences[2].flags.commutative = 0;
                dependences[2].dimension_count = 1;
                dimensions_11[0].size = 1L * sizeof(long int);
                dimensions_11[0].lower_bound = (0L - 0L) * sizeof(long int);
                dimensions_11[0].accessed_length = (0L - 0L - (0L - 0L) + 1) * sizeof(long int);
                dependences[3].address = (void *)mcc_arg_21;
                dependences[3].offset = 0L;
                dependences[3].dimensions = dimensions_11;
                dependences[3].flags.input = 0;
                dependences[3].flags.output = 1;
                dependences[3].flags.can_rename = 0;
                dependences[3].flags.concurrent = 0;
                dependences[3].flags.commutative = 0;
                dependences[3].dimension_count = 1;
                if (nanos_wd_ != (nanos_wd_t)0)
                  {
                    (*ol_args).size = mcc_arg_16;
                    (*ol_args).n = mcc_arg_17;
                    (*ol_args).A = mcc_arg_18;
                    (*ol_args).B = mcc_arg_19;
                    (*ol_args).C = mcc_arg_20;
                    (*ol_args).elapsed = mcc_arg_21;
                    ol_copy_dimensions[0 + 0].size = (((mcc_arg_16 * mcc_arg_17) - 1L - 0L) + 1L) * sizeof(elem_t);
                    ol_copy_dimensions[0 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    ol_copy_dimensions[0 + 0].accessed_length = ((mcc_arg_16 * mcc_arg_17) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    ol_copy_data[0].sharing = NANOS_SHARED;
                    ol_copy_data[0].address = (void *)mcc_arg_18;
                    ol_copy_data[0].flags.input = 1;
                    ol_copy_data[0].flags.output = 0;
                    ol_copy_data[0].dimension_count = (short int)1;
                    ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                    ol_copy_data[0].offset = 0L;
                    ol_copy_dimensions[1 + 0].size = (((mcc_arg_17 * mcc_arg_17) - 1L - 0L) + 1L) * sizeof(elem_t);
                    ol_copy_dimensions[1 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    ol_copy_dimensions[1 + 0].accessed_length = ((mcc_arg_17 * mcc_arg_17) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    ol_copy_data[1].sharing = NANOS_SHARED;
                    ol_copy_data[1].address = (void *)mcc_arg_19;
                    ol_copy_data[1].flags.input = 1;
                    ol_copy_data[1].flags.output = 0;
                    ol_copy_data[1].dimension_count = (short int)1;
                    ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                    ol_copy_data[1].offset = 0L;
                    ol_copy_dimensions[2 + 0].size = (((mcc_arg_16 * mcc_arg_17) - 1L - 0L) + 1L) * sizeof(elem_t);
                    ol_copy_dimensions[2 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    ol_copy_dimensions[2 + 0].accessed_length = ((mcc_arg_16 * mcc_arg_17) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    ol_copy_data[2].sharing = NANOS_SHARED;
                    ol_copy_data[2].address = (void *)mcc_arg_20;
                    ol_copy_data[2].flags.input = 0;
                    ol_copy_data[2].flags.output = 1;
                    ol_copy_data[2].dimension_count = (short int)1;
                    ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                    ol_copy_data[2].offset = 0L;
                    ol_copy_dimensions[3 + 0].size = 1L * sizeof(long int);
                    ol_copy_dimensions[3 + 0].lower_bound = (0L - 0L) * sizeof(long int);
                    ol_copy_dimensions[3 + 0].accessed_length = (0L - 0L - (0L - 0L) + 1) * sizeof(long int);
                    ol_copy_data[3].sharing = NANOS_SHARED;
                    ol_copy_data[3].address = (void *)mcc_arg_21;
                    ol_copy_data[3].flags.input = 0;
                    ol_copy_data[3].flags.output = 1;
                    ol_copy_data[3].dimension_count = (short int)1;
                    ol_copy_data[3].dimensions = &ol_copy_dimensions[3];
                    ol_copy_data[3].offset = 0L;
                    nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_mmc_2);
                    if (nanos_err != NANOS_OK)
                      {
                        nanos_handle_error(nanos_err);
                      }
                    nanos_err = nanos_submit(nanos_wd_, 4, &dependences[0], (nanos_team_t)0);
                    if (nanos_err != NANOS_OK)
                      {
                        nanos_handle_error(nanos_err);
                      }
                  }
                else
                  {
                    nanos_region_dimension_internal_t imm_copy_dimensions[4L];
                    nanos_copy_data_t imm_copy_data[4L];
                    imm_args.size = mcc_arg_16;
                    imm_args.n = mcc_arg_17;
                    imm_args.A = mcc_arg_18;
                    imm_args.B = mcc_arg_19;
                    imm_args.C = mcc_arg_20;
                    imm_args.elapsed = mcc_arg_21;
                    imm_copy_dimensions[0 + 0].size = (((mcc_arg_16 * mcc_arg_17) - 1L - 0L) + 1L) * sizeof(elem_t);
                    imm_copy_dimensions[0 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    imm_copy_dimensions[0 + 0].accessed_length = ((mcc_arg_16 * mcc_arg_17) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    imm_copy_data[0].sharing = NANOS_SHARED;
                    imm_copy_data[0].address = (void *)mcc_arg_18;
                    imm_copy_data[0].flags.input = 1;
                    imm_copy_data[0].flags.output = 0;
                    imm_copy_data[0].dimension_count = (short int)1;
                    imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                    imm_copy_data[0].offset = 0L;
                    imm_copy_dimensions[1 + 0].size = (((mcc_arg_17 * mcc_arg_17) - 1L - 0L) + 1L) * sizeof(elem_t);
                    imm_copy_dimensions[1 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    imm_copy_dimensions[1 + 0].accessed_length = ((mcc_arg_17 * mcc_arg_17) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    imm_copy_data[1].sharing = NANOS_SHARED;
                    imm_copy_data[1].address = (void *)mcc_arg_19;
                    imm_copy_data[1].flags.input = 1;
                    imm_copy_data[1].flags.output = 0;
                    imm_copy_data[1].dimension_count = (short int)1;
                    imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                    imm_copy_data[1].offset = 0L;
                    imm_copy_dimensions[2 + 0].size = (((mcc_arg_16 * mcc_arg_17) - 1L - 0L) + 1L) * sizeof(elem_t);
                    imm_copy_dimensions[2 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    imm_copy_dimensions[2 + 0].accessed_length = ((mcc_arg_16 * mcc_arg_17) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    imm_copy_data[2].sharing = NANOS_SHARED;
                    imm_copy_data[2].address = (void *)mcc_arg_20;
                    imm_copy_data[2].flags.input = 0;
                    imm_copy_data[2].flags.output = 1;
                    imm_copy_data[2].dimension_count = (short int)1;
                    imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                    imm_copy_data[2].offset = 0L;
                    imm_copy_dimensions[3 + 0].size = 1L * sizeof(long int);
                    imm_copy_dimensions[3 + 0].lower_bound = (0L - 0L) * sizeof(long int);
                    imm_copy_dimensions[3 + 0].accessed_length = (0L - 0L - (0L - 0L) + 1) * sizeof(long int);
                    imm_copy_data[3].sharing = NANOS_SHARED;
                    imm_copy_data[3].address = (void *)mcc_arg_21;
                    imm_copy_data[3].flags.input = 0;
                    imm_copy_data[3].flags.output = 1;
                    imm_copy_data[3].dimension_count = (short int)1;
                    imm_copy_data[3].dimensions = &imm_copy_dimensions[3];
                    imm_copy_data[3].offset = 0L;
                    nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_2_t), &imm_args, 4, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_mmc_2);
                    if (nanos_err != NANOS_OK)
                      {
                        nanos_handle_error(nanos_err);
                      }
                  }
              }
            }
        }
      }
    }
  if (i < N)
    {
      {
        int mcc_arg_24 = N - i;
        int mcc_arg_25 = N;
        elem_t *mcc_arg_26 = a + i * N;
        elem_t *mcc_arg_27 = b;
        elem_t *mcc_arg_28 = c + i * N;
        long int *mcc_arg_29 = matmul_time + i;
        {
          _Bool mcc_is_in_final;
          nanos_err_t mcc_err_in_final = nanos_in_final(&mcc_is_in_final);
          if (mcc_err_in_final != NANOS_OK)
            {
              nanos_handle_error(mcc_err_in_final);
            }
          if (mcc_is_in_final)
            {
              matmul(N - i, N, a + i * N, b, c + i * N, matmul_time + i);
            }
          else
            {
              {
                nanos_wd_dyn_props_t nanos_wd_dyn_props;
                struct nanos_args_3_t *ol_args;
                nanos_err_t nanos_err;
                struct nanos_args_3_t imm_args;
                nanos_region_dimension_t dimensions_12[1L];
                nanos_data_access_t dependences[4L];
                nanos_region_dimension_t dimensions_13[1L];
                nanos_region_dimension_t dimensions_14[1L];
                nanos_region_dimension_t dimensions_15[1L];
                static nanos_smp_args_t smp_ol_matmul_7_args = {.outline = (void (*)(void *))(void (*)(struct nanos_args_3_t *))&smp_ol_matmul_7};
                static struct nanos_const_wd_definition_1 nanos_wd_const_data = {.base = {.props = {.mandatory_creation = 0, .tied = 0, .clear_chunk = 0, .reserved0 = 0, .reserved1 = 0, .reserved2 = 0, .reserved3 = 0, .reserved4 = 0}, .data_alignment = __alignof__(struct nanos_args_3_t), .num_copies = 4, .num_devices = 1, .num_dimensions = 4, .description = 0}, .devices = {[0] = {.factory = &nanos_smp_factory, .arg = &smp_ol_matmul_7_args}}};
                nanos_wd_dyn_props.tie_to = 0;
                nanos_wd_dyn_props.priority = 0;
                nanos_wd_dyn_props.flags.is_final = 0;
                nanos_wd_dyn_props.flags.is_implicit = 0;
                nanos_wd_dyn_props.flags.is_recover = 0;
                ol_args = (struct nanos_args_3_t *)0;
                nanos_wd_t nanos_wd_ = (nanos_wd_t)0;
                nanos_copy_data_t *ol_copy_data = (nanos_copy_data_t *)0;
                nanos_region_dimension_internal_t *ol_copy_dimensions = (nanos_region_dimension_internal_t *)0;
                nanos_err = nanos_create_wd_compact(&nanos_wd_, &nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_3_t), (void **)&ol_args, nanos_current_wd(), &ol_copy_data, &ol_copy_dimensions);
                if (nanos_err != NANOS_OK)
                  {
                    nanos_handle_error(nanos_err);
                  }
                dimensions_12[0].size = (((mcc_arg_24 * mcc_arg_25) - 1L - 0L) + 1L) * sizeof(elem_t);
                dimensions_12[0].lower_bound = (0L - 0L) * sizeof(elem_t);
                dimensions_12[0].accessed_length = ((mcc_arg_24 * mcc_arg_25) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                dependences[0].address = (void *)mcc_arg_26;
                dependences[0].offset = 0L;
                dependences[0].dimensions = dimensions_12;
                dependences[0].flags.input = 1;
                dependences[0].flags.output = 0;
                dependences[0].flags.can_rename = 0;
                dependences[0].flags.concurrent = 0;
                dependences[0].flags.commutative = 0;
                dependences[0].dimension_count = 1;
                dimensions_13[0].size = (((mcc_arg_25 * mcc_arg_25) - 1L - 0L) + 1L) * sizeof(elem_t);
                dimensions_13[0].lower_bound = (0L - 0L) * sizeof(elem_t);
                dimensions_13[0].accessed_length = ((mcc_arg_25 * mcc_arg_25) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                dependences[1].address = (void *)mcc_arg_27;
                dependences[1].offset = 0L;
                dependences[1].dimensions = dimensions_13;
                dependences[1].flags.input = 1;
                dependences[1].flags.output = 0;
                dependences[1].flags.can_rename = 0;
                dependences[1].flags.concurrent = 0;
                dependences[1].flags.commutative = 0;
                dependences[1].dimension_count = 1;
                dimensions_14[0].size = (((mcc_arg_24 * mcc_arg_25) - 1L - 0L) + 1L) * sizeof(elem_t);
                dimensions_14[0].lower_bound = (0L - 0L) * sizeof(elem_t);
                dimensions_14[0].accessed_length = ((mcc_arg_24 * mcc_arg_25) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                dependences[2].address = (void *)mcc_arg_28;
                dependences[2].offset = 0L;
                dependences[2].dimensions = dimensions_14;
                dependences[2].flags.input = 0;
                dependences[2].flags.output = 1;
                dependences[2].flags.can_rename = 0;
                dependences[2].flags.concurrent = 0;
                dependences[2].flags.commutative = 0;
                dependences[2].dimension_count = 1;
                dimensions_15[0].size = 1L * sizeof(long int);
                dimensions_15[0].lower_bound = (0L - 0L) * sizeof(long int);
                dimensions_15[0].accessed_length = (0L - 0L - (0L - 0L) + 1) * sizeof(long int);
                dependences[3].address = (void *)mcc_arg_29;
                dependences[3].offset = 0L;
                dependences[3].dimensions = dimensions_15;
                dependences[3].flags.input = 0;
                dependences[3].flags.output = 1;
                dependences[3].flags.can_rename = 0;
                dependences[3].flags.concurrent = 0;
                dependences[3].flags.commutative = 0;
                dependences[3].dimension_count = 1;
                if (nanos_wd_ != (nanos_wd_t)0)
                  {
                    (*ol_args).size = mcc_arg_24;
                    (*ol_args).n = mcc_arg_25;
                    (*ol_args).A = mcc_arg_26;
                    (*ol_args).B = mcc_arg_27;
                    (*ol_args).C = mcc_arg_28;
                    (*ol_args).elapsed = mcc_arg_29;
                    ol_copy_dimensions[0 + 0].size = (((mcc_arg_24 * mcc_arg_25) - 1L - 0L) + 1L) * sizeof(elem_t);
                    ol_copy_dimensions[0 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    ol_copy_dimensions[0 + 0].accessed_length = ((mcc_arg_24 * mcc_arg_25) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    ol_copy_data[0].sharing = NANOS_SHARED;
                    ol_copy_data[0].address = (void *)mcc_arg_26;
                    ol_copy_data[0].flags.input = 1;
                    ol_copy_data[0].flags.output = 0;
                    ol_copy_data[0].dimension_count = (short int)1;
                    ol_copy_data[0].dimensions = &ol_copy_dimensions[0];
                    ol_copy_data[0].offset = 0L;
                    ol_copy_dimensions[1 + 0].size = (((mcc_arg_25 * mcc_arg_25) - 1L - 0L) + 1L) * sizeof(elem_t);
                    ol_copy_dimensions[1 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    ol_copy_dimensions[1 + 0].accessed_length = ((mcc_arg_25 * mcc_arg_25) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    ol_copy_data[1].sharing = NANOS_SHARED;
                    ol_copy_data[1].address = (void *)mcc_arg_27;
                    ol_copy_data[1].flags.input = 1;
                    ol_copy_data[1].flags.output = 0;
                    ol_copy_data[1].dimension_count = (short int)1;
                    ol_copy_data[1].dimensions = &ol_copy_dimensions[1];
                    ol_copy_data[1].offset = 0L;
                    ol_copy_dimensions[2 + 0].size = (((mcc_arg_24 * mcc_arg_25) - 1L - 0L) + 1L) * sizeof(elem_t);
                    ol_copy_dimensions[2 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    ol_copy_dimensions[2 + 0].accessed_length = ((mcc_arg_24 * mcc_arg_25) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    ol_copy_data[2].sharing = NANOS_SHARED;
                    ol_copy_data[2].address = (void *)mcc_arg_28;
                    ol_copy_data[2].flags.input = 0;
                    ol_copy_data[2].flags.output = 1;
                    ol_copy_data[2].dimension_count = (short int)1;
                    ol_copy_data[2].dimensions = &ol_copy_dimensions[2];
                    ol_copy_data[2].offset = 0L;
                    ol_copy_dimensions[3 + 0].size = 1L * sizeof(long int);
                    ol_copy_dimensions[3 + 0].lower_bound = (0L - 0L) * sizeof(long int);
                    ol_copy_dimensions[3 + 0].accessed_length = (0L - 0L - (0L - 0L) + 1) * sizeof(long int);
                    ol_copy_data[3].sharing = NANOS_SHARED;
                    ol_copy_data[3].address = (void *)mcc_arg_29;
                    ol_copy_data[3].flags.input = 0;
                    ol_copy_data[3].flags.output = 1;
                    ol_copy_data[3].dimension_count = (short int)1;
                    ol_copy_data[3].dimensions = &ol_copy_dimensions[3];
                    ol_copy_data[3].offset = 0L;
                    nanos_err = nanos_set_translate_function(nanos_wd_, (nanos_translate_args_t)nanos_xlate_fun_mmc_3);
                    if (nanos_err != NANOS_OK)
                      {
                        nanos_handle_error(nanos_err);
                      }
                    nanos_err = nanos_submit(nanos_wd_, 4, &dependences[0], (nanos_team_t)0);
                    if (nanos_err != NANOS_OK)
                      {
                        nanos_handle_error(nanos_err);
                      }
                  }
                else
                  {
                    nanos_region_dimension_internal_t imm_copy_dimensions[4L];
                    nanos_copy_data_t imm_copy_data[4L];
                    imm_args.size = mcc_arg_24;
                    imm_args.n = mcc_arg_25;
                    imm_args.A = mcc_arg_26;
                    imm_args.B = mcc_arg_27;
                    imm_args.C = mcc_arg_28;
                    imm_args.elapsed = mcc_arg_29;
                    imm_copy_dimensions[0 + 0].size = (((mcc_arg_24 * mcc_arg_25) - 1L - 0L) + 1L) * sizeof(elem_t);
                    imm_copy_dimensions[0 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    imm_copy_dimensions[0 + 0].accessed_length = ((mcc_arg_24 * mcc_arg_25) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    imm_copy_data[0].sharing = NANOS_SHARED;
                    imm_copy_data[0].address = (void *)mcc_arg_26;
                    imm_copy_data[0].flags.input = 1;
                    imm_copy_data[0].flags.output = 0;
                    imm_copy_data[0].dimension_count = (short int)1;
                    imm_copy_data[0].dimensions = &imm_copy_dimensions[0];
                    imm_copy_data[0].offset = 0L;
                    imm_copy_dimensions[1 + 0].size = (((mcc_arg_25 * mcc_arg_25) - 1L - 0L) + 1L) * sizeof(elem_t);
                    imm_copy_dimensions[1 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    imm_copy_dimensions[1 + 0].accessed_length = ((mcc_arg_25 * mcc_arg_25) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    imm_copy_data[1].sharing = NANOS_SHARED;
                    imm_copy_data[1].address = (void *)mcc_arg_27;
                    imm_copy_data[1].flags.input = 1;
                    imm_copy_data[1].flags.output = 0;
                    imm_copy_data[1].dimension_count = (short int)1;
                    imm_copy_data[1].dimensions = &imm_copy_dimensions[1];
                    imm_copy_data[1].offset = 0L;
                    imm_copy_dimensions[2 + 0].size = (((mcc_arg_24 * mcc_arg_25) - 1L - 0L) + 1L) * sizeof(elem_t);
                    imm_copy_dimensions[2 + 0].lower_bound = (0L - 0L) * sizeof(elem_t);
                    imm_copy_dimensions[2 + 0].accessed_length = ((mcc_arg_24 * mcc_arg_25) - 1L - 0L - (0L - 0L) + 1) * sizeof(elem_t);
                    imm_copy_data[2].sharing = NANOS_SHARED;
                    imm_copy_data[2].address = (void *)mcc_arg_28;
                    imm_copy_data[2].flags.input = 0;
                    imm_copy_data[2].flags.output = 1;
                    imm_copy_data[2].dimension_count = (short int)1;
                    imm_copy_data[2].dimensions = &imm_copy_dimensions[2];
                    imm_copy_data[2].offset = 0L;
                    imm_copy_dimensions[3 + 0].size = 1L * sizeof(long int);
                    imm_copy_dimensions[3 + 0].lower_bound = (0L - 0L) * sizeof(long int);
                    imm_copy_dimensions[3 + 0].accessed_length = (0L - 0L - (0L - 0L) + 1) * sizeof(long int);
                    imm_copy_data[3].sharing = NANOS_SHARED;
                    imm_copy_data[3].address = (void *)mcc_arg_29;
                    imm_copy_data[3].flags.input = 0;
                    imm_copy_data[3].flags.output = 1;
                    imm_copy_data[3].dimension_count = (short int)1;
                    imm_copy_data[3].dimensions = &imm_copy_dimensions[3];
                    imm_copy_data[3].offset = 0L;
                    nanos_err = nanos_create_wd_and_run_compact(&nanos_wd_const_data.base, &nanos_wd_dyn_props, sizeof(struct nanos_args_3_t), &imm_args, 4, &dependences[0], imm_copy_data, imm_copy_dimensions, (nanos_translate_args_t)nanos_xlate_fun_mmc_3);
                    if (nanos_err != NANOS_OK)
                      {
                        nanos_handle_error(nanos_err);
                      }
                  }
              }
            }
        }
      }
    }
  {
    nanos_err_t nanos_err;
    nanos_wd_t nanos_wd_ = nanos_current_wd();
    nanos_err = nanos_wg_wait_completion(nanos_wd_, 0);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
  }
  time_gettime(&time_end);
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Calculating matrices done!\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000));
  }
  ;
  k = N * (N - 1) / 2;
  for (i = 0; i < N; i++)
    {
      for (j = 0; j < N; j++)
        {
          if (c[i * N + j] != k * j)
            {
              errors++;
            }
        }
    }
  if (errors != 0)
    {
      struct timespec _t1;
      time_t _secs;
      time_t _min;
      clock_gettime(5, &_t1);
      _secs = _t1.tv_sec % 60;
      _min = _t1.tv_sec / 60 % 60;
      fprintf(stderr, "[%02d:%02d.%06d]: NOTE: Found %d ERRORS!\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), errors);
    }
  ;
  for (i = 0; i < N - BLOCKSIZE; i += BLOCKSIZE)
    {
      if (matmul_time[i] >= 0)
        {
          total_matmul += matmul_time[i];
          max_matmul = max_matmul > matmul_time[i] ? max_matmul : matmul_time[i];
          min_matmul = min_matmul < matmul_time[i] ? min_matmul : matmul_time[i];
        }
    }
  if (i < N)
    {
      if (matmul_time[i] >= 0)
        {
          total_matmul += matmul_time[i];
          max_matmul = max_matmul > matmul_time[i] ? max_matmul : matmul_time[i];
          min_matmul = min_matmul < matmul_time[i] ? min_matmul : matmul_time[i];
        }
    }
  mean_matmul = total_matmul / (N / BLOCKSIZE);
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Min matmul       %8ld msec\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), min_matmul);
  }
  ;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Mean matmul      %8ld msec\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), mean_matmul);
  }
  ;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Max matmul       %8ld msec\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), max_matmul);
  }
  ;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Total matmul     %8ld msec\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), total_matmul);
  }
  ;
  {
    struct timespec _t1;
    time_t _secs;
    time_t _min;
    clock_gettime(5, &_t1);
    _secs = _t1.tv_sec % 60;
    _min = _t1.tv_sec / 60 % 60;
    fprintf(stderr, "[%02d:%02d.%06d]: Total elab time  %8ld msec\n", (int)_min, (int)_secs, (int)(_t1.tv_nsec / 1000), time_diff_msec(&time_end, &time_start));
  }
  ;
  fflush(stderr);
  {
    nanos_err_t nanos_err;
    nanos_wd_t nanos_wd_ = nanos_current_wd();
    nanos_err = nanos_wg_wait_completion(nanos_wd_, 0);
    if (nanos_err != NANOS_OK)
      {
        nanos_handle_error(nanos_err);
      }
  }
  return errors;
}
static void smp_ol_matmul_5_unpacked(int size, int n, elem_t *A, elem_t *B, elem_t *C, long int *elapsed)
{
  {
    matmul(size, n, A, B, C, elapsed);
  }
}
static void smp_ol_matmul_5(struct nanos_args_2_t *const args)
{
  {
    smp_ol_matmul_5_unpacked((*args).size, (*args).n, (*args).A, (*args).B, (*args).C, (*args).elapsed);
  }
}
static void smp_ol_matmul_7_unpacked(int size, int n, elem_t *A, elem_t *B, elem_t *C, long int *elapsed)
{
  {
    matmul(size, n, A, B, C, elapsed);
  }
}
static void smp_ol_matmul_7(struct nanos_args_3_t *const args)
{
  {
    smp_ol_matmul_7_unpacked((*args).size, (*args).n, (*args).A, (*args).B, (*args).C, (*args).elapsed);
  }
}
