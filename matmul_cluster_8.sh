#./matmul/matmul-smp.sh 

export NX_ARGS="--cluster --cluster-network=mpi --smp-workers 4 --cluster-smp-presend 1020 --deps regions --cluster-node-memory $((2048*2048*(256))) --cluster-unaligned-node-memory --schedule affinity"

#export NX_ARGS="--cluster --cluster-network=mpi --cluster-smp-presend=3" \
#	NX_CLUSTER_NODE_MEMORY=268435456 \
#	NX_GASNET_SEGMENT_SIZE=268435456 \

echo "Executing matrix multiplication on 8 boards..."
mpirun -x NX_ARGS -np 1 -hostfile hosts matmul/matmul.sh 1
